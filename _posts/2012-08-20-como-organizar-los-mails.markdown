---
title: Como organizar los mails
layout: post
categories: []
---

Las listas de correos cuando son muy activas pueden molestarnos y
hacernos perder mails importantes, por eso conviene organizarlos en
alguna carpeta para que no nos molesten.

Gmail
-----

En Gmail abrimos un mail de la lista que queremos organizar.

Al abrirlo hacemos click en \"mas\" y luego en \"filtrar mensajes
similares\". Esto automáticamente identifica los mails como provenientes
de una lista de correos.

Hacemos click en \"crear un filtro con estos criterios de búsqueda\"

Marcamos la opción \"Omitir recibidos (Archivarlo)\". Esto quiere decir
que los mails que cumplan este criterio no aparecerán en la carpeta de
recibidos.

Marcamos la opción \"Aplicar la etiqueta:\" y creamos una nueva etiqueta
para identificar estos mails.

Marcamos la opcion que dice \"Aplicar tambien a las x\...\" esto aplica
el filtro que creamos a los mails anteriores que cumplan con este
criterio.

Hacemos click en \"Crear Filtro\"

Ahora a la izquierda nos aparecera una carpeta con el nombre que
elegimos y los mails que cumplan los criterios. Y los nuevos mails iran
directamente a esa carpeta sin pasar por la carpeta recibidos.

![](Organizar-mails.png "Organizar-mails.png")

Otros webmails/clientes
-----------------------

Si sabes como hacer lo mismo en otros servicios por favor comparte el
conocimiento con los demas piratas :)
