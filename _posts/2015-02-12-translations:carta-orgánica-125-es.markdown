---
title: Translations:Carta Orgánica/125/es
layout: post
categories: []
---

-   Los aportes y franquicias otorgados por el Fondo Partidario
    Permanente a cargo del Estado, que será percibido y administrado por
    las Asambleas;
-   Las contribuciones de sus afiliados y simpatizantes, en los montos y
    condiciones que decida la Asamblea;
-   Las contribuciones de los empleados, funcionarios o candidatos
    designados o electos que reciban como haberes de sus cargos;
-   Aportes y donaciones sin cargo alguno, siempre y cuando su
    percepción no esté prohibida por las leyes o por los principios
    ideológicos y/o éticos del Partido.
-   Los bienes inmuebles adquiridos con fondos partidarios o con
    donaciones con tal objetivo, que se inscribirán a nombre del Partido
    en el registro respectivo.
-   Demás contribuciones que la legislación permita y que no estén en
    contradicción con los principios ideológicos y/o éticos del Partido.
