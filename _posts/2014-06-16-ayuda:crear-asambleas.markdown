---
title: Ayuda:Crear asambleas
layout: post
categories:
- Barco_de_infraestructura|{{PAGENAME}}
---

Las [asambleas piratas](http://asambleas.partidopirata.com.ar/) son
listas de correo usando GNU Mailman.

Para crear una nueva, correr en el servidor:

`   newlist`\
`   # Completar los datos que pide....`\
`   newaliases /var/lib/mailman/data/aliases`

Configuración
-------------

### Aceptar mails de piratas no suscritos

-   Pero suscritos en la [asamblea
    general](http://asambleas.partidopirata.com.ar/listinfo/general).
    Poner en la opción **Opciones de privacidad \> Filtrado del
    remitente \> Lista de direcciones no suscritas cuyos envíos deben
    ser aceptados automaticamente**\

`   @general`

-   Direcciones \@partidopirata.com.ar, en la misma sección que el ítem
    anterior:\

`   ^.*@partidopirata.com.ar$`

[](Categoría:Barco_de_infraestructura "wikilink")
