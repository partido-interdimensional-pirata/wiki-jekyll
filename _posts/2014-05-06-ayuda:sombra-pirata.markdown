---
title: Ayuda:Sombra pirata
layout: post
categories:
- Propuestas aprobadas
- Abril 2014
- Organización
---

[Categoría:Propuestas
aprobadas](Categoría:Propuestas_aprobadas "wikilink") [Categoría:Abril
2014](Categoría:Abril_2014 "wikilink")
[Categoría:Organización](Categoría:Organización "wikilink")

Propuesta hecha en la [Asamblea
Permanente](http://asambleas.partidopirata.com.ar/archivos/general/2014-April/007598.html)
el 22 de Abril de 2014.

> Hace un tiempo estabamos hablando que estaría bueno que aparte de la
> bienvenida, las personas que se suscriben a la lista tengan dos
> piratas amigas que las guíe en cómo viene la mano, puedan resolver
> dudas sobre cómo funcionan las cosas, etc.
>
> [MaxPower](Pirata:MaxPower "wikilink") las llamó piratas sombra :P

Adoptá unx pirata!
==================

Implementación
--------------

-   [Armar un listado de piratas disponibles](Piratas_Sombra "wikilink")

<!-- -->

-   Cuando haya una nueva suscripción, las administradoras de la lista
    eligen dos al azar y las ponen en contacto con la nueva suscripción

Preguntas
---------

-   ¿Por qué dos? Porque si la sombra resulta ser creepy y resta más de
    lo que suma, la otra puede desempatar :P\
    ¡Además siempre hablamos de barcos de al menos tres piratas!
