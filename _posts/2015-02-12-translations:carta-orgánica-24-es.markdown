---
title: Translations:Carta Orgánica/24/es
layout: post
categories: []
---

Los Piratas afiliados se otorgan un poder recíproco e indistinto para
defenderse mutuamente y a otros piratas, ante los estrados judiciales en
caso de que cualquiera de sus miembros sea demandado civilmente o
querellado penalmente por propiciar el derecho constitucional a informar
y ser informado libremente, compartiendo bienes culturales, sean
materiales o digitales.
