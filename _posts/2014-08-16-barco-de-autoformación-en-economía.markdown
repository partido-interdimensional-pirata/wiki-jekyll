---
title: Barco de Autoformación en Economía
layout: post
categories:
- Autoformación
- Autoformación
---

Pad:
<http://pad.partidopirata.com.ar/p/Barco_Autoformaci%C3%B3n_en_Econom%C3%ADa>

**Fecha de reunión**: lunes 25/8, 17 o 18hrs. Bar La Poesía, San Telmo,
CABA

A. Objetivo General:
--------------------

-   Lograr un grupo que permita una sistema de autoformación en teorías
    economicas, para postular una \"economía pirata\" e indagar sobre
    esa posibilidad.
-   Mejorar nuestro nivel de argumentos y capacidad de análisis de
    discurso económico.

B. Metodología:
---------------

-   Reuniones

Quincenales a determinar mediante opcion I : consenso, acordar el día
con mayor convocatoria opcion II: día fijo

Tiempo estimado: hora y cuarto/media

-   Fuentes de estudio

Selección de texto inicial disparador, considerando \... la propuesta
del expositor voluntario \... el interés de los participantes que
asistirán en la temática propuesta

-   Enfoque

opción 1: en un tema específico por tanda de 3/4 reuniones (por ej.
deuda externa) opción 2: un tema diferente por reunión

-   Exposiciónes

<!-- -->

-   De voluntario que haya leído uno o dos capítulos de determinado
    libro y los proponga y comparta. Debería presentar un breve contexto
    y contar de que va. Informal pero para formarnos.

<!-- -->

-   El texto a leer se acuerda previamente, los asistentes deberían
    leerlo dentro de lo posible.

C. Temas y textos propuestos //No hacer una lista de 300 libros
---------------------------------------------------------------

Arrancamos por El Manifiesto Telecomunista. Fijate en el
[pad](http://pad.partidopirata.com.ar/p/Barco_Autoformaci%C3%B3n_en_Econom%C3%ADa)
los siguientes.

D. Calendario de primera reunión experimental
---------------------------------------------

**Fecha de reunión**: lunes 25/8, 17 o 18hrs. Bar La Poesía, San Telmo,
CABA

Voluntario: Laion

Texto: El Manifiesto Telecomunista. Nueve puntos. //vendría luego
segunda parte para otra reunión, de punto 10 a 16, que son otras 22
carillas. <http://endefensadelsl.org/manifiesto_telecomunista.html>

El Manifiesto Telecomunista

1.  Prefacio
2.  Introducción
3.  El comunismo de pares contra el estado capitalista cliente-servidor
4.  Las condiciones de la clase trabajadora en la Internet
5.  Atrapados en la telaraña mundial
6.  La Producción de Pares y la Pobreza de las Redes
7.  Comunismo de riesgo
8.  El manifiesto de la red telecomunista
9.  Contribución a la Crítica de la Cultura Libre

Estos complementan muy bien y extienden el manifiesto:

-   Dmytri Kleiner y la Financiación del Procomún Material
    <http://guerrillatranslation.com/2014/03/11/dmytri-kleiner-y-la-financiacion-del-procomun-material/>

<!-- -->

-   Cooperativismo, producción P2P, y financiación comunitaria en el
    contexto del procomún. KMO del C-Realm Podcast, entrevista a Michel
    Bauwens, Dmytri Kleiner y John Restakis
    <http://guerrillatranslation.com/2014/01/28/hacia-un-procomun-material/>

[Categoría:Autoformación](Categoría:Autoformación "wikilink")

[Categoría:Autoformación](Categoría:Autoformación "wikilink")
