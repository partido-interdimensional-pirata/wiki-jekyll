---
title: Cultura Libre
layout: post
categories: []
---

\'\'\'

Libertad: "creando y defendiendo la libertad"
---------------------------------------------

\'\'\'

**Licencias Libres -\> Software libre -\> Cultura Libre -\> Hombre
Libre**

== *\' Copyleft*\'

`==`

*\' De Wikipedia, la enciclopedia libre*\'

Una letra C invertida ("reversed c"), sin reconocimiento legal, es el
símbolo más común, como contrapartida del símbolo copyright.

Copyleft (por oposición a copyright)\[1\] comprende a un grupo de
derechos de autor caracterizados por eliminar las restricciones de
distribución o modificación impuestas por el copyright, con la condición
de que el trabajo derivado se mantenga con el mismo régimen de derechos
de autor que el original.

Bajo tales licencias pueden protegerse una gran diversidad de obras,
tales como programas informáticos, arte, cultura y ciencia, es decir
prácticamente casi cualquier tipo de producción creativa.

Sus partidarios la proponen como alternativa a las restricciones que
imponen las normas planteadas en los derechos de autor, a la hora de
hacer, modificar y distribuir copias de una obra determinada. Se
pretende garantizar así una mayor libertad para que cada receptor de una
copia, o una versión derivada de un trabajo, pueda, a su vez, usar,
modificar y redistribuir tanto el propio trabajo como las versiones
derivadas del mismo. Así, y en un entorno no legal, puede considerarse
como opuesto al copyright o derechos de autor tradicionales. Contenido

-   1 Historia
-   2 Métodos de aplicación
-   3 Etimología
-   4 Tipos y relación con otras licencias

o 4.1 Software de código abierto o 4.2 Fuerte y débil o 4.3 Completo y
parcial o 4.4 Compartir-Igual

-   5 Ideología
-   6 El efecto vírico
-   7 Fuera del contexto del software

o 7.1 Arte y documentación o 7.2 Patentes

-   8 Explotación comercial

o 8.1 Nuevos productos o 8.2 Comercialización industrial o 8.3
Comercialización artística

-   9 Véase también
-   10 Referencias
-   11 Enlaces externos

\'\'\' Historia \'\'\' Pese a que hoy en día el concepto se aplica a una
amplia variedad de campos como la producción literaria o la
cinematográfica, su origen se encuentra en la década de los años setenta
en el incipiente desarrollo de software para la todavía embrionaria
industria informática.

Por aquel entonces Richard Stallman estaba elaborando un intérprete de
Lisp que interesó a la compañía Symbolics, éste accedió a
proporcionarles una versión del intérprete bajo dominio público, sin
restricciones iniciales. Más tarde, la empresa amplió y mejoró el
software original, pero cuando Stallman quiso acceder a dichas
modificaciones, la compañía se negó.

Fue entonces, en 1984, cuando Stallman decidió ponerse a trabajar para
erradicar este tipo de comportamiento, al que bautizó con el nombre de
acaparamiento del software (software hoarding).

Como a Stallman le pareció poco viable, a corto plazo, eliminar las
leyes del copyright así como las injusticias que consideraba provocadas
por su perpetuamiento, decidió trabajar dentro del marco legal existente
y creó así su propia licencia de derechos de autor, la Licencia Pública
General de GNU (GPL). Según el proyecto GNU:\[2\] La forma más simple de
hacer que un programa sea libre es ponerlo en el dominio público, sin
derechos reservados. Esto le permite compartir el programa y sus mejoras
a la gente, si así lo desean. Pero le permite a gente no cooperativa
convertir el programa en software privativo. Ellos pueden hacer cambios,
muchos o pocos, y distribuir el resultado como un producto privativo.
Las personas que reciben el programa con esas modificaciones no tienen
la libertad que el autor original les dio; el intermediario se las ha
quitado. En el proyecto GNU, nuestro objetivo es el dar a todo usuario
la libertad de redistribuir y cambiar software GNU. Si los
intermediarios pudieran quitar esa libertad, nosotros tendríamos muchos
usuarios, pero esos usuarios no tendrían libertad. Así en vez de poner
software GNU en el dominio público, nosotros lo protegemos con Copyleft.
Copyleft dice que cualquiera que redistribuye el software, con o sin
cambios, debe dar la libertad de copiarlo y modificarlo más. Copyleft
garantiza que cada usuario tiene libertad.

Por primera vez se recogía el derecho a que el titular de los derechos
de autor pudiera transferir de forma permanente y a obras derivadas
surgidas, el máximo número de derechos posible a aquellos que reciban
una copia del programa. Es decir, impedir jurídicamente al material
ofrecido en estos términos que en un futuro se pueda apropiar parte de
él a derechos de autor. Aunque es la primera licencia copyleft, será
posteriormente, con nuevas licencias inspiradas en esta y con la
popularización del software libre cuando se empezaría a hacer frecuente
este término. \'\'\' Métodos de aplicación \'\'\' La práctica habitual
para conseguir este objetivo de explotación sin trabas, copia y
distribución de una creación o de un trabajo (y sus derivados) es la de
ofrecerlo junto con una licencia o contrato. Esta debería estipular que
cada propietario de una copia del trabajo pudiera:

1\. usarla sin ninguna limitación. 2. (re)distribuir cuantas copias
desee, y 3. modificarla de la manera que crea conveniente.

Estas tres libertades básicas, sin embargo, no son suficientes aún para
asegurar que una obra derivada sea distribuida bajo las mismas
condiciones no restrictivas: con este fin, la licencia debe asegurar que
el propietario del trabajo derivado lo distribuirá bajo el mismo tipo de
licencia.

Otras condiciones de licencia adicionales que podrían evitar posibles
impedimentos a las tres libertades básicas anteriores son:

-   las condiciones de la licencia copyleft no pueden ser revocadas;
-   el trabajo y sus derivados son siempre puestos a disposición de
    manera que se facilite su modificación. Por ejemplo, en el software,
    esta facilidad suele asociarse a la disponibilidad del código
    fuente, donde incluso la compilación de dicho código debería
    permitirse sin ninguna clase de impedimento.
-   idear un sistema más o menos obligatorio para documentar
    adecuadamente la creación y sus modificaciones, por medio de
    manuales de usuario, descripciones, etc.

En la práctica, para que estas licencias copyleft tuvieran algún tipo de
efecto, necesitarían hacer un uso creativo de las reglas y leyes que
rigen los derechos de autor, p.e., cuando nos referimos a las leyes del
copyright (que es el caso más común), todas las personas que de alguna
manera han contribuido al trabajo con copyleft se convertirían en (co)
titulares de los derechos de autor, pero, al mismo tiempo, si nos
atenemos a la licencia, también renunciarían deliberadamente a algunos
de los derechos que normalmente se derivan de los derechos de autor, por
ejemplo, el derecho a ser el único distribuidor de las copias del
trabajo.

Aunque depende de las leyes que rigen los derechos de autor, que pueden
ser diferentes de un país a otro, la licencia final, que no es más que
un método para alcanzar los objetivos del copyleft, también puede
diferir de un país a otro. Por ejemplo, en algunos países puede ser
aceptable vender un producto software sin ninguna garantía, al estilo de
la licencia \[GPL (véanse los artículos 11 y 12 de la versión 2 de la
licencia GPL\[3\] ), mientras que en la mayoría de países europeos no es
posible que un distribuidor de software se desentienda de todas las
garantías vinculadas a un producto vendido, razón por la cual el alcance
de dichas garantías es descrito explícitamente en la mayoría de
licencias copyleft europeas (la licencia CeCILL, permite usar la GPL --
artículo 5.3.4 de CeCILL -- en combinación con una garantía limitada --
artículo 9). *\' Etimología*\'

Según algunas fuentes, el término copyleft proviene de un mensaje
contenido en el programa Tiny BASIC, una versión de BASIC distribuida
libremente y escrita por el Doctor Li-Chen Wang a finales de los años
setenta. El listado del programa contenía las frases "\@COPYLEFT" y
"TODOS LOS PERJUICIOS RESERVADOS", en contraposición a "copyright" y
"todos los derechos reservados", términos éstos usados habitualmente en
los textos de derechos de autor.

Richard Stallman asegura que la palabra proviene de Don Hopkins, al que
considera un compañero muy imaginativo, el cual le envió en 1984 ó 1985
una carta en la que podía leerse: Copyleft-revocados todos los derechos
(Copyleft-all rights reversed). El término copyleft con la anotación
revocados todos los derechos fue utilizado también a principios de los
años setenta dentro del Principia Discordia, texto que tal vez inspirara
a Hopkins.

Hay algunos problemas con la definición del término copyleft que
contribuyen a crear controversia a su alrededor. El término, en su
origen un nombre, se creó como una transformación chistosa o parodía del
término copyright, queriendo abarcar los términos de la GPL creada por
Stallman como parte de su trabajo para la Free Software Foundation. Los
vocablos en inglés right y left, derecha e izquierda respectivamente,
acentúan la diferencia entre copyleft y copyright. La traducción
propuesta, izquierdo de autor o izquierdos de autor, intenta mantener
este sentido en contraste con los derechos de autor o el derecho de
autor. Curiosamente, left también se puede traducir como dejado o
abandonado, mientras que right es derecho o recto.

Así, el hecho de que un programa esté cubierto por el copyleft se
considera prácticamente lo mismo que poner dicho programa bajo GPL
(siendo el estándar de facto de todos los tipos de licencias surgidas
posteriormente y difundidas como copyleft). Cuando se utiliza como
verbo, su significado es menos preciso y puede referirse a cualquiera de
entre una variedad similar de licencias, o incluso a una teórica
licencia imaginaria para propósitos de discusión. Véase también la
siguiente sección, que entra en detalle sobre algunos aspectos de la
definición.

**Tipos y relación con otras licencias** *\' Software de código
abierto*\'

Algunos han querido ver el copyleft como una piedra de toque en el
conflicto ideológico entre el movimiento del código abierto y el
movimiento del software libre. Por un lado, Eric Raymond, fundador de la
Open Source Initiative, manifestó en 2005 que "ya no necesitamos la
GPL"\[1\]. Por otro lado, las licencias publicadas por la Free Software
Foundation son únicamente licencias copyleft\[2\].

Sin embargo, es difícil caracterizar al copyleft como punto de discordia
entre las dos posturas filosóficas, ya que en la práctica ambas aceptan
las licencias copyleft y no copyleft por igual:

-   Ambos tipos de licencias aparecen en las respectivas listas de
    licencias admitidas por la Free Software Foundation\[3\] y por la
    Open Source Initiative\[4\].
-   El primer Consejero Legal de la OSI, Larry Rosen, es el autor de una
    licencia copyleft, la Open Software License\[5\].
-   El Licensing Howto\[6\] de la OSI reconoce la GPL como una best
    practice (práctica recomendable).
-   La propia Free Software Foundation publica parte de los programas de
    su Proyecto GNU bajo licencias no-copyleft\[7\].
-   El propio Richard Stallman ha apoyado el uso de licencias no
    copyleft para determinados proyectos, como en el caso del reciente
    cambio de licencia del proyecto Ogg Vorbis\[8\].

**Fuerte y débil**

El copyleft que rige un trabajo se considera "más fuerte" cuanto mayor
es la eficiencia con la que hace cumplir las condiciones de la licencia
a todos los tipos de trabajos derivados. El "copyleft débil" hace
referencia a las licencias que no se heredan a todos los trabajos
derivados, dependiendo a menudo de la manera en que éstos se hayan
derivado.

Este último tipo de licencias es el que se utiliza generalmente para la
creación de bibliotecas de software, con el fin de permitir que otros
programas puedan enlazar con ellas y ser redistribuidos, sin el
requerimiento legal de tener que hacerlo bajo la nueva licencia
copyleft. Solamente se requiere distribuir los cambios sobre el software
con "copyleft débil", no los cambios sobre el software que enlaza con
él. Esto permite a programas con cualquier licencia ser compilados y
enlazados con bibliotecas con copyleft tales como glibc (una biblioteca
estándar requerida por muchos programas) y ser redistribuidos después
sin necesidad de cambiar la licencia.

Ejemplos de licencias de software libre que utilizan copyleft "fuerte"
son la Licencia Pública General de GNU y la Licencia Pública Q. Por otra
parte, entre las licencias de software libre que usan copyleft "débil"
tenemos la Licencia Pública General Reducida de GNU (LGPL) y la Licencia
Pública de Mozilla. Ejemplos de licencias de software libre que no son
copyleft son la licencia X11 y las licencias BSD.

**Completo y parcial**

El copyleft "completo" y "parcial" tiene que ver con otra cuestión: El
copyleft completo es aquel que permite que todas las partes de un
trabajo (excepto la licencia) sean modificadas por sus sucesivos
autores. El copyleft parcial implica que algunas partes de la propia
creación no están expuestas a su modificación ilimitada, o visto de otro
modo, que no están completamente sujetas a todos los principios del
copyleft, p.e., en la creación artística el copyleft completo es en
ocasiones imposible o indeseable. *\' Compartir-Igual*\'

Muchas licencias del tipo compartir-por-igual (share-alike) son
licencias copyleft parciales (o no completas). El concepto de
compartir-igual implica, sin embargo, que cualquier libertad otorgada
sobre el trabajo original (o sus copias) se mantiene intacta en
cualquier trabajo derivado: esto implica además que cualquier licencia
copyleft completa es automáticamente una licencia de tipo
compartir-por-igual (¡pero no al contrario!). En lugar de usar la
consigna "todos los derechos reservados" propia del copyright, o la de
"todos los derechos invertidos", del copyleft completo, las licencias
compartir-igual suelen usar el lema de "algunos derechos reservados".
Ciertas combinaciones de la licencia Creative Commons, o en el caso de
la música, las Licencias de Música Libre (LML) son un ejemplo de
licencia del tipo compartir-por-igual.

**Ideología**

Para mucha gente, es una técnica que utiliza los derechos de autor como
medio para subvertir las restricciones impuestas tradicionalmente por el
copyright sobre la diseminación y el desarrollo del conocimiento. Con
este enfoque, el copyleft es principalmente una herramienta en una
operación de mayor envergadura: la intención es invertir permanentemente
dichas restricciones.

Aunque el copyleft no es un término reconocido por la ley, sus
defensores lo ven como una herramienta legal en un debate político e
ideológico sobre las obras intelectuales. Algunos ven en el copyleft un
primer paso para suprimir cualquier tipo de ley relacionada con el
copyright. En el dominio público, la ausencia de una protección como la
que ofrece el copyleft deja al software en un estado desprotegido. Los
desarrolladores no tendrían ningún problema pues en difundir y vender
binarios sin documentación y sin proporcionar el código fuente. Si se
abolieran las leyes del copyright, y a falta de otros medios, no habría
manera de hacer cumplir una licencia copyleft, aunque también sería
menos necesario.

Muchas licencias de software libre, como las que utilizan los sistemas
operativos BSD, el Sistema de Ventanas X y el servidor web Apache, no
son licencias copyleft puesto que no exigen al titular de la licencia la
distribución de los trabajos derivados bajo la misma licencia. En la
actualidad, se debate sobre qué licencia proporciona mayor grado de
libertad. En este debate se consideran cuestiones complejas como la
propia definición de libertad y qué libertades son más importantes. A
veces se dice que las licencias copyleft tratan de maximizar la libertad
de todos aquellos destinatarios potenciales en el futuro (inmunidad
contra la creación de software privativo), mientras que las licencias de
software libre sin copyleft maximizan la libertad del destinatario
inicial (libertad para crear software privativo). Con un enfoque
similar, la libertad del destinatario (que es limitada por el copyleft)
puede distinguirse de la libertad del propio software (la cual es
garantizada por el copyleft).

**El efecto vírico**

A las licencias copyleft se les suele atribuir un efecto "vírico",
debido a que cualquier trabajo derivado de un trabajo con copyleft debe
a su vez atenerse a los principios del copyleft. En particular, los
trabajos con copyleft no pueden ser incorporados legalmente en trabajos
que sean distribuidos sin el código fuente, como pasa con la mayoría de
productos comerciales, sin el permiso específico de sus autores. Como
resultado, su uso en la industria está mayoritariamente limitado a uso
interno.

El término "vírico" implica propagación como la de un virus biológico
por un organismo vivo. En el contexto de las licencias y los contratos
legalmente vinculantes, "vírico" hace referencia a cualquier cosa que se
propaga adhiriéndose a cualquier otra cosa, sin tener en cuenta si las
aportaciones víricas añaden algún valor al trabajo particular.

Los partidarios del copyleft sostienen que hacer extensiva la analogía
entre las licencias copyleft y los virus informáticos resulta
inapropiado, puesto que los virus informáticos generalmente infectan los
ordenadores sin que el usuario se percate de ello e intentan causar
daño, mientras que los autores de trabajos derivados son conscientes de
la licencia copyleft del trabajo original y sus usuarios pueden obtener
beneficio de ella. Muchos evitan usar el término "vírico" dadas sus
connotaciones negativas.

Microsoft, entre otros, al describir la GPL como una "licencia vírica",
también puede estar refiriéndose a la idea de que cualquier lanzamiento
de un trabajo nuevo bajo GPL pudiera crear un efecto red con
realimentación positiva, en el que con el paso del tiempo habría una
ingente cantidad de código copyleft que continuara expandiéndose. La
reutilización de código es una meta importante en ingeniería del
software por la cual se ahorran esfuerzos usando componentes genéricos
que ya existen para tener un producto listo en poco tiempo. Aquellos que
no aplican copyleft en sus programas, a menudo tienen que "reinventar la
rueda" en muchas partes de éstos, debido principalmente a que la
cantidad de software libre que carece de copyleft es relativamente
pequeña. Esto es considerado frecuentemente una desventaja del
desarrollo de software sin copyleft.

Algunos oponentes del copyleft sin formación legal, afirman que el
simple hecho de usar una sola línea de código copyleft en un proyecto de
millones de líneas de código sin copyleft, convierte automáticamente
este último en código copyleft. Se puede ver entonces que este
comportamiento es similar al de un virus informático o biológico, el
cual infecta una entidad mucho mayor a pesar de que sus dimensiones son
pequeñas en comparación.

Sin embargo, dicha reclamación es incorrecta por dos motivos. En primer
lugar, en la mayoría de jurisdicciones es improbable que una sola línea
de código sea considerada suficiente para justificar la protección del
copyright. En segundo lugar, incluso cuando el código que se incluye en
un proyecto es lo suficientemente importante para justificar la
protección del copyright, el resto del código nunca pasará a tener
automáticamente una licencia copyleft. Lo que ocurrirá es que no será
legal distribuir el trabajo derivado, a menos que el propietario del
trabajo que recibe el código copyleft lo ponga bajo una licencia
compatible (que no tiene por qué ser necesariamente una licencia
copyleft). Si el trabajo es distribuido de todos modos, esto será
considerado una simple violación del copyright, y no afectará para nada
a la licencia del trabajo.

De manera adicional, algunas licencias copyleft populares como la GPL
incluyen una cláusula que especifica que los componentes con copyleft
pueden interactuar con componentes sin copyleft siempre que la
comunicación sea relativamente simple, como por ejemplo ejecutar una
herramienta de línea de órdenes mediante una sencilla estructura de
control. Como consecuencia, incluso si un módulo de un producto con
intenciones de no aplicar copyleft está puesto bajo GPL, todavía existe
una posibilidad legal para que otros componentes se comuniquen con él de
una forma restringida.

**Fuera del contexto del software**

**Arte y documentación**

El copyleft también ha inspirado a las artes, con movimientos emergentes
como la Libre Society y los sellos discográficos open-source. Por
ejemplo, la Licencia Arte Libre es una licencia copyleft que puede ser
aplicada a cualquier obra de arte.

Entre las licencias copyleft para materiales ajenos al software tenemos
las licencias Creative Commons compartir-igual y la Licencia de
Documentación Libre de GNU (abreviada como GNU FDL, GFDL, o FDL). La
licencia GFDL puede utilizarse para proteger con copyleft aquellos
trabajos que no tienen código fuente distinguible (aunque el requisito
que establece la GPL de liberar el código fuente no tiene mucho sentido
cuando se trata de trabajos donde no se puede distinguir entre código
compilado, código objeto, código ejecutable o código binario). La
licencia GFDL sí que distingue entre una "copia transparente" y una
"copia opaca", usando una definición diferente a la dada por la GPL para
"código fuente" y "código objeto".

Merece la pena destacar que para que el copyleft tenga sentido, requiere
de alguna manera que exista un espacio donde sea algo común el poder
hacer copias baratas de forma sencilla (ficheros de ordenador o
fotocopias, etc.), o, poniéndolo de otro modo, donde uno pueda ofrecer
algo sin "perder" ese algo (como el conocimiento): p.e., el copyleft es
más difícil de poner en práctica en aquellas artes que se caracterizan
por la producción de objetos únicos, que no pueden ser copiados tal como
son (a menos que no se tema por la integridad del trabajo original). Se
puede ilustrar esta idea con el siguiente ejemplo: suponga que hay una
exposición pública de algunos cuadros mundialmente famosos, p.e.,
algunas de las muchas copias y trabajos derivados que Andy Warhol hizo
de sus propias obras de arte, y suponga que alguien que tiene acceso a
esos cuadros (sin tener plena propiedad de los derechos de éstos),
decide "mejorarlos" con algunos efectos pictóricos de su gusto (sin
olvidar la correspondiente firma con pintura en aerosol). Dada esta
situación, no habría manera (legal) de detener a este tipo si le puede
considerar el titular bajo copyleft completo de dichas obras.

Éste y otros ejemplos parecen señalar que el copyleft no es la piedra
filosofal definitiva capaz de resolver todos los asuntos relacionados
con los derechos de autor de una vez por todas: especialmente en el
arte, que posee también una tradición de creación como un proceso
solitario (junto con, pero de manera separada, una tradición de
creatividad cooperativa), las obras "dirigidas a la comunidad" no es lo
que se desea en todos los casos.

Las licencias copyleft para el arte son conscientes generalmente de
tales limitaciones, por lo que difieren de las licencias copyleft para
el software. Algunas diferencias consisten, p.e., en distinguir entre la
obra original y las copias (donde algunas condiciones imprescindibles
del copyleft son aplicables solamente a las copias) o en descansar sobre
ideas que son menos objetivas a la hora de poner en práctica (más como
declaraciones de intenciones), por ejemplo, estipulando que el copyleft
sea sujeto de consideración -- en el mundo de los programadores la
implementación del copyleft constituye en sí misma la máxima
consideración que uno puede obtener. En otras palabras: en el arte el
copyleft tiene que depender de nociones más generales referentes a los
derechos de autor, las cuales son incluso más complejas (y más
cambiantes entre países) que las leyes del copyright, como por ejemplo,
los derechos morales, intelectuales, etc.

Al igual que el sistema de licencias Creative Commons compartir-igual,
la GFDL permite a los autores establecer limitaciones en ciertas
secciones de su trabajo, dejando exentas a algunas partes de su creación
del mecanismo del copyleft completo. En el caso de la licencia GFDL,
estas limitaciones incluyen el uso de secciones invariantes, que no
pueden ser alteradas por futuros editores. Estos tipos de licencias
copyleft parciales pueden ser utilizadas también fuera del contexto del
arte: de hecho, ese era el propósito inicial para la GFDL, puesto que
fue originalmente concebida como un mecanismo para apoyar la
documentación de software (con copyleft). Sin embargo, puede ser
utilizada para cualquier tipo de documento.

Muchos artistas aplican copyleft en su trabajo teniendo en mente que
aquellos que lo copien y lo modifiquen de algún modo reconocerán el
trabajo al artista inicial. Sin embargo, esto puede traer problemas: el
trabajo del artista podría utilizarse de manera contraria a su voluntad,
p.e., poniendo una fotografía estándar en un cartel racista. Si el
artista es reconocido, será entonces asociado aparentemente con un grupo
y una ideología que tal vez no comparta. Asimismo, tampoco hay garantía
de que se le atribuya el mérito de su trabajo cuando le gustaría.

**Patentes**

Las ideas del copyleft están siendo también sugeridas cada vez más para
su aplicación en patentes (y por tanto, dependiendo de un marco legal de
patentes en lugar de un marco legal de derechos de autor). Ejemplos de
estas iniciativas son los fondos de patentes abiertas que permiten el
uso libre de royalties de patentes contribuidas al fondo bajo ciertas
condiciones (como renunciar al derecho de solicitar nuevas patentes que
no han sido contribuidas a dicho fondo). Sin embargo, esta iniciativa
parece no haber despegado, tal vez porque las patentes son relativamente
caras de obtener, mientras que los derechos de autor se obtienen de
manera gratuita.

No obstante, puesto que la mayoría de creaciones con copyleft adquieren
la característica de copyleft exclusivamente de las leyes del copyright,
los mecanismos de patentes pueden amenazar las libertades que otorga el
copyleft a dichas creaciones cuando se permite a las leyes de patentes
anular a las de copyright, que podría ser el caso de las nuevas reglas
referentes a patentes desarrolladas por la Unión Europea a principios
del siglo XXI. No parece haber una respuesta fácil a tales amenazas,
aunque generalmente se considera que las comunidades que desarrollan
productos con copyleft no tienen ni los recursos ni la organización
necesaria para los complejos trámites de patentes.

**Explotación comercial**

La explotación comercial de trabajos con copyleft difiere de la
explotación comercial tradicional que se obtiene de los derechos de
autor. La explotación de trabajos con copyleft puede conseguirse, por
ejemplo, construyendo un modelo de servicios -incluyendo asesoría y
soporte- alrededor del trabajo con copyleft. Generalmente, se espera que
un negocio "copyleft" genere unos beneficios económicos más bajos que un
negocio que utilice trabajos privativos. Las empresas que trabajan con
productos privativos pueden generar ingresos exclusivamente con las
ventas, las licencias individuales y transferibles, y los lucrativos
litigios sobre derechos del trabajo.

**Nuevos productos**

La competitividad de los trabajos con copyleft en los negocios puede
parecer excesivamente débil, siendo incapaz de generar inversiones para
investigación y desarrollo, ni de acaparar exclusivamente los beneficios
obtenidos del resultado. Económicamente, el copyleft se considera el
único mecanismo capaz de competir con las empresas monopolísticas que
dependen de la explotación económica del copyright, marcas registradas y
leyes de patentes. El copyleft permite a programadores voluntarios
contribuir y sentirse involucrados en el desarrollo de software,
formando parte de un proyecto mucho más grande, como el desarrollo del
núcleo de un sistema operativo. Además, se aseguran de que cualquier
derivado que surja de su esfuerzo en el futuro permanecerá accesible a
ellos gracias al copyleft. Por consiguiente, el desarrollo de software
con copyleft deja clara su intención de nunca ocultar o abusar de
cualquier conocimiento que se aporte. A su vez, el copyleft también
garantiza que las compañías y programadores que se deciden a colaborar
no puedan crear sus propias versiones privativas del trabajo para tomar
ventaja sobre otros. En su lugar, la competitividad se basa en otros
aspectos del suministro de productos comerciales con copyleft.

**Comercialización industrial**

Los distribuidores comerciales de sistemas basados en GNU/Linux (como
Red Hat y Mandrake) pueden haber tenido sus más y sus menos a la hora de
encontrar una estrategia exitosa (o modelo de negocio) para sacar
adelante sus negocios, pero con el tiempo ha quedado demostrado que es
posible basar un negocio en un servicio comercial en torno a una
creación con copyleft. Un ejemplo bien conocido es el de Mandrake, que
fue una de las primeras compañías en tener éxito en la bolsa de valores
tras la implosión de grandes partes del mercado de la Tecnología de la
Información (TI) a principios del siglo XXI. La compañía también logró
convencer a los organismos gubernamentales para cambiar a su
distribución de GNU/Linux.

Sin embargo, y dejando a un lado excepciones como Debian, la mayoría de
distribuidores de GNU/Linux no limitan su negocio al software con
copyleft. Parece no haber una verdadera razón por la cual, la
explotación de servicios comerciales en torno a creaciones con copyleft,
no pudiera ser posible en negocios a pequeña escala. Dicha estrategia,
como concepto de negocio, no sería más compleja que la de hacer dinero
con una receta para preparar café de "dominio público", y que tan
satisfactoriamente han sabido explotar los propietarios de muchas
cafeterías. UserLinux, un proyecto de Bruce Perens, apoya la aparición
de tales negocios a pequeña escala basados en software libre, es decir,
programas informáticos con copyleft o con algún otro tipo de licencia
libre. El sitio web de UserLinux\[4\] expone algunos casos de estudio
así como historias que han tenido éxito en tales negocios.

**Comercialización artística**

En arte, el concepto de "servicio comercial en torno a una creación con
copyleft" puede ser (incluso) más difícil de poner en práctica que en el
desarrollo de software. Las representaciones públicas podrían ser
consideradas como una de las pocas posibilidades de proporcionar dichos
"servicios".

La industria de la música, por ejemplo, parece haber encontrado un
obstáculo a su desarrollo en los programas de intercambio de ficheros en
redes P2P. La Fundación de Fronteras Electrónicas (Electronic Frontier
Foundation, EFF) propone algunas sugerencias para resolver este
problema:

Licenciamiento voluntario colectivo: Suena obvio: las principales casas
discográficas podrían reunirse y ofrecer unas licencias justas y no
discriminatorias para su música. A esto se le llama "licenciamiento
voluntario colectivo", y es lo que se lleva practicando 70 años para
mantener legal la radio y al mismo tiempo remunerar a los compositores.
Protege a las estaciones de posibles pleitos mientras reúne el dinero
por las canciones que éstas reproducen.

Licencias individuales obligatorias: Si a los artistas, compositores, y
titulares de derechos de autor se les exigiera permitir la copia on-line
a cambio de unos honorarios fijados por el gobierno, las compañías
podrían arreglárselas sin problemas para reunir dichos honorarios, hacer
la contabilidad, y remitirlos a los artistas. El pago a cada artista no
tiene que reflejar directamente lo que paga cada consumidor, siempre que
el total entre todos los artistas y consumidores quede equilibrado.
(...)

Compartir los ingresos por publicidad: Sitios como Internet Underground
Music Archive, EMusic.com, Soundclick, y Artistdirect.com ponen a
disposición de los fans un espacio donde escuchar flujos de música
(streaming), descargar ficheros, y ponerse en contacto con los artistas.
Mientras tanto, estos fans ven publicidad cuyos beneficios se reparten
entre el propio sitio y los titulares de los derechos de autor.

Suscripciones P2P: Algunos vendedores de software P2P podrían empezar a
cobrar por su servicio. Los amantes de la música podrían pagar una
cantidad fija por el software o por cada canción descargada. Los fondos
podrían ser distribuidos a los artistas y titulares de derechos de autor
mediante acuerdos de licencia con los estudios o firmas discográficas o
a través de licencias obligatorias. En 2001, Napster se planteó este
servicio de suscripción. Aunque las batallas legales de Napster contra
la industria del disco lo dejaron fuera de juego, (junto con muchos
otros sitios y sistemas P2P), los servicios de suscripción (como el
iTunes Music Store de Apple) muestran que los consumidores están
dispuestos a pagar por la música que descargan.

Patrocinio digital y propinas on-line: La contribución directa de los
amantes de la música es una manera muy antigua de compensar a los
artistas. Puesto que el contenido se ha transformado en digital, así
debe hacerlo también la forma de pago. Con un bote de propinas on-line
como el Amazon Honor System, los artistas pueden pedir donaciones
directamente desde sus páginas web, en cantidades tan pequeñas como un
dólar. Los sitios que ofrecen patrocinio como MusicLink y QuidMusic
emergen con este propósito permitiendo a los consumidores seleccionar a
los músicos y compositores que les gustaría apoyar. De cualquier forma,
se proporciona a los consumidores un método fácil y seguro para dar
dinero directamente a los artistas que admiran.

Impuestos por ancho de banda: Algunas personas han propuesto a los ISPs
como puntos de recaudación por el intercambio P2P. Todo usuario de
Internet obtiene acceso web a través de un ISP. La mayoría mantiene
también un acuerdo financiero regular con alguno de ellos. A cambio de
protección contra posibles pleitos o juicios, los ISPs podrían vender
cuentas "autorizadas" (con un cargo extra) a los usuarios de P2P.

Tarifas sobre los medios: Otro lugar donde generar ingresos es en los
medios físicos que se utilizan para almacenar la música. Canadá y
Alemania gravan todos los discos grabables y distribuyen luego los
fondos entre los artistas. En Estados Unidos tienen discos grabables con
derechos pagados y discos de datos. Es difícil pagar a los artistas de
forma precisa con este sistema por sí solo, pero otros datos (como
estadísticas de las redes P2P, por ejemplo) podrían ayudar a la hora de
hacer más justo el desembolso de los fondos.

Conciertos: Está comprobado, los conciertos son una enorme fuente de
ingresos para los artistas. Algunos, como las bandas Grateful Dead\[5\]
y Phish,\[6\] han construido sus carreras alrededor de sus giras, al
mismo tiempo que animaban a sus fans a grabar e intercambiar su música.
La distribución por paridad encaja a la perfección con este modelo,
constituyendo un sistema de distribución y promoción para aquellas
bandas que eligen ganarse la vida en la carretera.

Algunos se muestran más firmes sobre el comercio de ideas y dicen: Las
ideas no funcionan igual que los objetos. Si yo te doy un objeto físico
dejo de poder usar y controlar dicho objeto, y puedo pedir algo a
cambio, algún tipo de pago o compensación. En cambio, cuando te doy una
idea, no pierdo nada. Aún puedo utilizar esa idea como desee. No
necesito pedir nada a cambio.

Algunos artistas usan licencias, como Creative Commons Reconocimiento No
comercial Compartir Igual,\[7\] que no permiten un uso comercial. De
esta manera, pueden escoger vender sus creaciones sin tener que competir
con otras copias en venta del mismo trabajo.

**Referencias**

1\. ↑ Traducción propuesta, GNU: La definición de Software Libre o Cabos
sueltos: Copyleft, boletín de la traducción en las instituciones de la
Unión Europea, número 98 (mayo-junio 2006) 2. ↑ Extraído de GNU: The GNU
Project (en inglés) 3. ↑ GPL versión 2, (en inglés) 4. ↑ UserLinux es
una distribución GNU/linux, página oficial (en inglés) 5. ↑ Grateful
Dead, página oficial, necesita Flash (en inglés) 6. ↑ Phish, página
oficial, necesita Flash (en inglés) 7. ↑ Licencia CC Reconocimiento No
comercial Compartir Igual 2.5, para España.

Artículo extraído de: <http://es.wikipedia.org/wiki/Copyleft>

==

**== ¿Qué es el Software Libre? ==** ==

Software Libre se refiere a la libertad de los usuarios para ejecutar,
copiar, distribuir, estudiar, cambiar y mejorar el software. De modo más
preciso, se refiere a cuatro libertades de los usuarios del software:

-   La libertad de usar el programa, con cualquier propósito (libertad
    0).
-   La libertad de estudiar cómo funciona el programa, y adaptarlo a tus
    necesidades (libertad 1). El acceso al código fuente es una
    condición previa para esto.
-   La libertad de distribuir copias, con lo que puedes ayudar a tu
    vecino (libertad 2).
-   La libertad de mejorar el programa y hacer públicas las mejoras a
    los demás, de modo que toda la comunidad se beneficie. (libertad 3).
    El acceso al código fuente es un requisito previo para esto.

Un programa es software libre si los usuarios tienen todas estas
libertades. Así pues, deberías tener la libertad de distribuir copias,
sea con o sin modificaciones, sea gratis o cobrando una cantidad por la
distribución, a cualquiera y a cualquier lugar. El ser libre de hacer
esto significa (entre otras cosas) que no tienes que pedir o pagar
permisos.

También deberías tener la libertad de hacer modificaciones y utilizarlas
de manera privada en tu trabajo u ocio, sin ni siquiera tener que
anunciar que dichas modificaciones existen. Si publicas tus cambios, no
tienes por qué avisar a nadie en particular, ni de ninguna manera en
particular.

La libertad para usar un programa significa la libertad para cualquier
persona u organización de usarlo en cualquier tipo de sistema
informático, para cualquier clase de trabajo, y sin tener obligación de
comunicárselo al desarrollador o a alguna otra entidad específica.

La libertad de distribuir copias debe incluir tanto las formas binarias
o ejecutables del programa como su código fuente, sean versiones
modificadas o sin modificar (distribuir programas de modo ejecutable es
necesario para que los sistemas operativos libres sean fáciles de
instalar). Está bien si no hay manera de producir un binario o
ejecutable de un programa concreto (ya que algunos lenguajes no tienen
esta capacidad), pero debes tener la libertad de distribuir estos
formatos si encontraras o desarrollaras la manera de crearlos.

Para que las libertades de hacer modificaciones y de publicar versiones
mejoradas tengan sentido, debes tener acceso al código fuente del
programa. Por lo tanto, la posibilidad de acceder al código fuente es
una condición necesaria para el software libre.

Para que estas libertades sean reales, deben ser irrevocables mientras
no hagas nada incorrecto; si el desarrollador del software tiene el
poder de revocar la licencia aunque no le hayas dado motivos, el
software no es libre.

Son aceptables, sin embargo, ciertos tipos de reglas sobre la manera de
distribuir software libre, mientras no entren en conflicto con las
libertades centrales. Por ejemplo, copyleft es la regla que implica que,
cuando se redistribuya el programa, no se pueden agregar restricciones
para denegar a otras personas las libertades centrales. Esta regla no
entra en conflicto con las libertades centrales, sino que más bien las
protege.

'Software libre' no significa 'no comercial'. Un programa libre debe
estar disponible para uso comercial, desarrollo comercial y distribución
comercial. El desarrollo comercial del software libre ha dejado de ser
inusual; el software comercial libre es muy importante.

Pero el software libre sin \`copyleft' también existe. Creemos que hay
razones importantes por las que es mejor usar 'copyleft', pero si tus
programas son software libre sin ser 'copyleft', los podemos utilizar de
todos modos.

Cuando se habla de software libre, es mejor evitar términos como:
\`regalar' o \`gratis', porque esos téminos implican que lo importante
es el precio, y no la libertad. \'\'\' ¿Qué es GNU/Linux? \'\'\'
GNU/LINUX (más conocido como Linux, simplemente) es un sistema
operativo, compatible Unix.

Dos características muy peculiares lo diferencian del resto de los
sistemas que podemos encontrar en el mercado: la primera, es que es
libre, esto significa que no tenemos que pagar ningún tipo de licencia a
ninguna casa desarrolladora de software por el uso del mismo, la
segunda, es que el sistema viene acompañado del código fuente.

El sistema lo forman el núcleo del sistema (kernel) más un gran número
de programas y librerias que hacen posible su utilización.

Linux se distribuye bajo la Licencia Pública General GNU (GPL), por lo
tanto, el código fuente tiene que estar siempre accesible.

El sistema ha sido diseñado y programado por multitud de programadores
alrededor del mundo. El núcleo del sistema sigue en continuo desarrollo
bajo la coordinación de Linus Torvalds, la persona de la que partió la
idea de este proyecto, en 1991.

Linus, por aquel entonces un estudiante de informática de la Universidad
de Helsinki, empezó (como proyecto de fin de carrera y sin poder
imaginar en lo que se llegaría convertir) a programar las primeras
líneas de código de este sistema operativo llamado LINUX.

Aquí tenéis el primer mensaje que Linus Torvalds mandó al grupo de
noticias comp.os.minix:

From:torvalds\@klaava.Helsinki.FI (Linus Benedict Torvalds) Newsgroup:
comp.os.minix Subject: GCC-1.40 and a posix question Message-ID:
1991Jul13, 100050.9886\@klaava.Helsinki.FI Date: 3 Jul 91 10:00:50 GMT

Hello netlanders, Due a project I'm working on (in minix), I'm
interested in the posix standard definition. Could somebody please point
me to a (preferably) machine-readable format of the latest posix rules?
Ftp-sites would be nice.

Linux Torvalds torvalds\@kruuna.helsinki.fi

Aquí el que le siguió; este mensaje es considerado por muchos como el
comienzo de Linux:

From:torvalds\@klaava.Helsinki.FI (Linus Benedict Torvalds) Newsgroup:
comp.os.minix Subject: What would you like to see most in minix?
Summary: small poll for my new operating system Message-ID: 1991Aug25,
20578.9541\@klaava.Helsinki.FI Date: 25 Aug 91 20:57:08 GMT
Organization: University of Helsinki.

Hello everybody out there using minix-

I'm doing a (free) operating system (just a hobby, won't be big and
professional like gnu) for 386(486) AT clones. This has been brewing
since april, and is starting to get ready. I'd like any feedback on
things people like/dislike in minix; as my OS resembles it somewhat
(same physical layout of the file-sytem due to practical reasons) among
other things. I've currently ported bash (1.08) an gcc (1.40), and
things seem to work. This implies that i'll get something practical
within a few months, and I'd like to know what features most people
want. Any suggestions are welcome, but I won't promise I'll implement
them :-)

Linux Torvalds torvalds\@kruuna.helsinki.fi

Y aquí el que podíamos definir como el anuncio "oficial":

From: Linus Benedict Torvalds (torvalds\@klaava.Helsinki.FI) Subject:
Free minix-like kernel sources for 386-AT Newsgroups: comp.os.minix
Date: 1991-10-05 08:53:28 PST

Do you pine for the nice days of minix-1.1, when men were men and wrote
their own device drivers? Are you without a nice project and just dying
to cut your teeth on a OS you can try to modify for your needs? Are you
finding it frustrating when everything works on minix? No more all-
nighters to get a nifty program working? Then this post might be just
for you :-)

As I mentioned a month(?) ago, I'm working on a free version of a
minix-lookalike for AT-386 computers. It has finally reached the stage
where it's even usable (though may not be depending on what you want),
and I am willing to put out the sources for wider distribution. It is
just version 0.02 (+1 (very small) patch already), but I've successfully
run bash/gcc/gnu-make/gnu-sed/compress etc under it.

Sources for this pet project of mine can be found at nic.funet.fi
(128.214.6.100) in the directory /pub/OS/Linux. The directory also
contains some README-file and a couple of binaries to work under linux
(bash, update and gcc, what more can you ask for :-) . Full kernel
source is provided, as no minix code has been used. Library sources are
only partially free, so that cannot be distributed currently. The system
is able to compile "as-is" and has been known to work. Heh. Sources to
the binaries (bash and gcc) can be found at the same place in /pub/gnu.

ALERT! WARNING! NOTE! These sources still need minix-386 to be compiled
(and gcc-1.40, possibly 1.37.1, haven't tested), and you need minix to
set it up if you want to run it, so it is not yet a standalone system
for those of you without minix. I'm working on it. You also need to be
something of a hacker to set it up (?), so for those hoping for an
alternative to minix-386, please ignore me. It is currently meant for
hackers interested in operating systems and 386's with access to minix.

The system needs an AT-compatible harddisk (IDE is fine) and EGA/VGA. If
you are still interested, please ftp the README/RELNOTES, and/or mail me
for additional info.

I can (well, almost) hear you asking yourselves "why?". Hurd will be out
in a year (or two, or next month, who knows), and I've already got
minix. This is a program for hackers by a hacker. I've enjouyed doing
it, and somebody might enjoy looking at it and even modifying it for
their own needs. It is still small enough to understand, use and modify,
and I'm looking forward to any comments you might have.

I'm also interested in hearing from anybody who has written any of the
utilities/library functions for minix. If your efforts are freely
distributable (under copyright or even public domain), I'd like to hear
from you, so I can add them to the system. I'm using Earl Chews estdio
right now (thanks for a nice and working system Earl), and similar works
will be very wellcome. Your (C)'s will of course be left intact. Drop me
a line if you are willing to let me use your code.

Linus

El origen de Linux estuvo inspirado en MINIX, un pequeño sistema Unix
desarrollado por Andy Tanenbaum. Las primeras discusiones sobre Linux
fueron en el grupo de noticias comp.os.minix, en estas discusiones se
hablaba sobre todo del desarrollo de un pequeño sistema Unix para
usuarios de Minix que querían más.

Linus nunca anunció la versión 0.01 de Linux (agosto 1991), esta versión
no era ni siquiera ejecutable, solamente incluía los principios del
núcleo del sistema, estaba escrita en lenguaje ensamblador y asumía que
uno tenía acceso a un sistema Minix para su compilación.

El 5 de octubre de 1991, Linus anunció la primera versión "oficial" de
Linux, (version 0.02). Con esta versión Linus pudo ejecutar Bash (GNU
Bourne Again Shell) y gcc (El compilador GNU de C) pero no mucho más. En
este estado de desarrollo ni se pensaba en los terminos soporte,
documentación, distribución .....

Despues de la versión 0.03, Linus saltó en la numeración hasta la 0.10.
Más y más programadores a lo largo y ancho de Internet empezaron a
trabajar en el proyecto y después de sucesivas revisiones, Linus
incrementó el número de versión hasta la 0.95 (Marzo 1992). Más de un
año después (diciembre 1993) el núcleo del sistema estaba en la versión
0.99 y la versión 1.0 llego el 14 de marzo de 1994.

La serie actual del núcleo es la 2.6.x y sigue avanzando día a día con
la meta de perfeccionar y mejorar el sistema. ¿Donde se puede obtener
Linux?

Aunque se podrían hacer un sistema Linux desde el principio, lo más
normal es obtener una distribución ya empaquetada y que suele contener
el propio sistema operativo más centenares de programas, ya listos para
su uso.

Existen cientos de distribuciones Linux en el mundo; la mayoría se
pueden obtener a través de Internet, aunque también se pueden comprar
algunas de ellas.

Webs como Distrowatch y LinuxIso son buenos lugares para comenzar,
además de las propias webs de creadores de distribuciones.

Las distribuciones más conocidas son:

-   Ubuntu
-   Debian Gnu/Linux
-   Open SuSE
-   Mandriva
-   Fedora
-   Linux Mint
-   PCLinuxOS
-   Slackware Linux
-   Gentoo Linux
-   CentOS

Más información: <http://distrowatch.com/dwres.php?resource=major>
[1](http://www.zegeniestudios.net/ldc/index.php?lang=es)

*\' Licencias Libres*\'

En el mundo del software libre existen diversos tipos de licencias bajo
las cuales se amparan las producciones realizadas por los
desarrolladores y/o usuarios:

-   GPL: GNU General Public License. Es la más conocida, cubre la mayor
    parte del software de la Free Software Foundation, y otros muchos
    programas. Puedes leer la licencia traducida \[1\] al español en
    formato \[html\] o en formato \[txt\] (traducción de Jesús González
    Barahona y Pedro de las Heras Quirós)
-   FDL: GNU Free Documentation License. Cubre manuales y documentación
    para el software de la Free Software Foundation, con posibilidades
    en otros campos. Puede leer la licencia traducida \[1\] en formato
    \[html\] o descargarla en formato \[pdf\]. (Traducida por Igor
    Támara y Pablo Reyes)
-   LGPL: GNU Lesser General Publication License. Se aplica a algunos
    paquetes de software diseñados específicamente "típicamente
    librerías" de la Free Software Foundation y de otros autores que
    deciden usarla. Puede leer la licencia traducida \[1\] al español en
    formato \[html\] o descargarla en formato \[pdf\].

(Traducida por Rafael Palomino)

-   ColorIURIS: ColorIuris es un sistema mixto de autogestión y cesión
    de derechos de autor en línea a partir del modelo continental; a
    partir del artículo 27 de la Declaración Universal de Derechos
    Humanos, y con respeto al Convenio de Berna, los Tratados Internet
    de la O.M.P.I. de 1996, la normativa de U.E. y la legislación
    nacional de los respectivos Estados con efectos legales para
    creadores de los países soportados.
-   Creative Commons: está inspirada en la licencia GPL de la Free
    Software Foundation. La idea principal es posibilitar un modelo
    legal y ayudado de herramientas informáticas para así facilitar la
    distribución y el uso de contenidos para el dominio público. Ofrece
    una serie de licencias, cada una con diferentes configuraciones o
    principios como el derecho del autor original a dar libertad para
    citar su obra, reproducirla, crear obras derivadas, ofrecerlo
    públicamente y con diferentes restricciones como no permitir el uso
    comercial o respetar la autoría original. Puede leer la licencia
    traducida en formato \[html\].

\[1\] NOTA IMPORTANTE: Esta es una traducción no oficial al español de
la GNU General Public License. No ha sido publicada por la Free Software
Foundation, y no establece legalmente las condiciones de distribución
para el software que usa la GNU GPL. Estas condiciones se establecen
solamente por el texto original, en inglés, de la GNU GPL. Sin embargo,
esperamos que esta traducción ayude a los hispanohablantes a entender
mejor la GNU GPL.

Documentación Libre

Existe mucha documentación sobre GNU/Linux y el Software Libre; y la
mayor parte de ella está amparada bajo licencias GNU/GPL o GNU/FDL.
Sería muy difícil resumir aquí todos los lugares donde puedes encontrar
esa documentación, pero sin lugar a dudas uno de los mejores
repositorios de Documentación libre en español es la página del proyecto
L.U.C.A.S (Linux en Castellano) que te recomendamos.

Puedes visitar la página de LUCAS en: <http://es.tldp.org/> \'\'\'

== Cultura libre

`==`

\'**\'\' De Wikipedia, la enciclopedia libre**

Este artículo trata sobre el concepto de Cultura libre. Si desea saber
sobre el libro homónimo de Lawrence Lessig consulte Cultura libre
(libro).

Logotipo oficial de la Definición de las obras culturales libres

La cultura libre es la visión de la cultura promovida por un heterogéneo
movimiento social basada en la libertad de distribuir y modificar
trabajos y obras creativas. Contenido

-   1 Orígenes de la cultura libre
-   2 Polémica por la delimitación de las fronteras de la cultura libre
-   3 Definición de las obras culturales libres
-   4 Notas

Orígenes de la cultura libre

El surgimiento y popularización de las licencias de software libre,
donde los programas informáticos tienen cuatro libertades básicas, así
como del copyleft, que utiliza como medio el copyright para conseguir
como fin que las obras derivadas de un programa que tenga las cuatro
libertades anteriores las posean también\[1\] (lo que ha sido llamado a
veces cláusula vírica), inspiró la plasmación de dicha filosofía en
otros ámbitos. De esta manera surgieron las primeras licencias libres no
orientadas específicamente a software como la Open Communication License
v. 1.0,\[2\] para publicaciones, y la GNU Free Documentation License v.
1.2,\[3\] para documentación de software.

Estas manifestaciones de Cultura Libre han permitido un mayor control de
los creadores sobre sus obras y un mejor acceso de todos nosotros a
estos bienes intelectuales bajo estándares no restrictivos y para ello,
iniciativas encaminadas a la promoción de esta filosofía han adelantado
proyectos específicos encaminados al desarrollo y conocimiento de
actividades bajo estos permisos libres.

En el año 2000 nace ArtLibre, una licencia que surge del encuentro de
Copyleft Attitude en París a principios de dicho año, con el fin de dar
acceso abierto a una obra para autorizar su uso sin ignorar los derechos
morales de autor.

En el año 2001 nace Creative Commons, una organización sin ánimo de
lucro cuya misión consistió en la creación de una serie de licencias
estandarizadas para las obras artísticas y culturales.

En el año 2004 desde el marco legal español aparece la licencia Aire
Incondicional, llevada a cabo en el Centro de Arte Shedhalle, la cual ha
sido aplicada a una serie de contenidos y que la hace fácilmente
entendible y modificable por sus usuarios.

En el año 2005 nace ColorIURIS un sistema internacional de gestión y
cesión de derechos de autor creado a partir del modelo jurídico
continental, cuya principal característica es la puesta a disposición de
contenidos a través de contratos de cesión de derechos, lo que hace de
este sistema una alternativa diferente a las demás.

En el año 2007 se crea el primer registro de obras libres por internet,
conocido como Espacio de utilidad pública, el cual ha sido puesto a
disposición por el sistema ColorIURIS, su finalidad, albergar aquellas
obras que han pasado al dominio público.

Polémica por la delimitación de las fronteras de la cultura libre

Hay quienes sostienen que sin obras derivadas y sin uso comercial no hay
arte libre mientras otros afirman lo contrario. La libertad de crear y
compartir la producción artística no siempre es compatible con la
libertad de comprar o vender obras de arte.

Definición de las obras culturales libres

Benjamin Mako Hill escribió en 2005 un artículo\[4\] en el que criticaba
a Creative Commons por hablar de cultura libre no teniendo definidos una
serie de criterios concretos bajo los cuales una obra pudiera
considerarse libre.\[5\] En 2006 se lanzó el proyecto
Freedomdefined.org, para lograr una definición para las obras culturales
libres,\[6\] para la cual, se contó con la opinión de especialistas del
software libre, artistas, científicos y abogados, presentándose su
primera versión en el 2007.\[7\]

Según la Definición de las obras culturales libres,\[8\] son trabajos
libres aquellos que permiten las siguientes libertades:

-   usar el trabajo y disfrutar de los beneficios de su uso
-   estudiar el trabajo y aplicar el conocimiento adquirido de él
-   hacer y redistribuir copias, totales o parciales, de la información
    o expresión
-   hacer cambios y mejoras, y distribuir los trabajos derivados

Notas

1\. ↑ Richard Stallman (traductor Carlos Maldonado). Copyleft: Idealismo
Pragmático \[en línea\]. Proyecto GNU -- Free Software Foundation.
Disponible en . 2. ↑ Open Content. Open Communication License \[en
línea\]. 1999. Disponible en (en inglés). 3. ↑ Free Software Foundation.
GNU Free Documentation License \[en línea\]. 2002. Disponible en (en
inglés). 4. ↑ Bejamin "Mako" Hill. Towards a Standard of Freedom:
Creative Commons and the Free Software Movement \[en línea\]. 2005.
Disponible en (en inglés). 5. ↑ Barrapunto. A vueltas con Creative
Commons y la necesidad de una definición de libertad \[en línea\]. 2005.
Disponible en . 6. ↑ Barrapunto. Hacia una definición de la cultura
libre \[en línea\]. 2006. Disponible en . 7. ↑ Barrapunto. Definición de
obra cultural libre \[en línea\]. 2007. Disponible en . 8. ↑
Freedomdefined.org. Definición de las obras culturales libres \[en
línea\]. 2007. Disponible en .

\'\'\'==

Lo que algunas personas nunca entenderán "La Filosofia". == \'\'\'

La filosofía que existe tras GNU/Linux es el ideal de la humanidad. Es
el futuro de la especie al que tarde o temprano deberemos llegar si no
queremos extinguirnos. Si esta filosofía la aplicáramos en todos los
aspectos de la vida, ¿se imaginan que formidable sociedad tendríamos?.
Evolucionaríamos de forma vertiginosa. Me entristezco al pensar en todo
el tiempo que desperdiciamos y en donde estaríamos ahora si hubiéramos
aplicado esta filosofía desde el comienzo de nuestra historia
(seguramente desperdigados por toda la galaxia y no anclados en la
tierra).

Muchas veces se critica que Linux no funciona bien, que no detecta todos
los dispositivos del equipo, que es complicado de utilizar, que los
juegos del pingüino son una porquería, que OpenOffice.org nunca estará a
la altura del conjunto de oficina de Microsoft, que GIMP nunca será la
herramienta de trabajo que es Photoshop, y así podría continuar con
varias quejas más.

Sin embargo, lo que ellos no han entendido va mucho más allá de la
simple funcionalidad de los programas de Software Libre, incluso
muchisimo más allá de la gratuidad de los programas, o de la posibilidad
de obtener y revisar el código fuente de los programas. Si señores, va
mucho más allá.

Se trata de una filosofia que pretende mejorar nuestra sociedad,
hacernos mejores personas, trabajar por el bien común, mejorarnos a
nosotros mismos y al resto de la humanidad. Es la filosofia que se
encuentra detrás del Proyecto GNU. Y sus herramientas son el Software
Libre como Linux, OpenOffice.org, Mozilla Firefox, GIMP, KDE, GNOME,
Apache, WINE, etc.

Muchos podrán tildar de loco extremista a Richard Stallman, por su firme
posición de no aceptar el software privativo en ninguna de sus formas;
pero si lograrán entender su punto de vista, las aspiraciones por una
sociedad más libre, seguramente valorarían mejor las acciones y metas
que ha realizado y logrado.

Nuestra finalidad es aportar para lograr de alguna forma mejorar nuestra
sociedad, algunos escribiendo programas y luego ofreciendolos en forma
libre y gratuita para que cualquiera los puedas utilizar; otros
redactando guías, tutoriales, artículos y documentos para promover y
enseñar el uso del Software Libre; también hay algunos que diseñan arte,
logos, iconos e imágenes para que las puedas utilizar de forma
completamente libre; en definitiva, hacemos cosas para nuestro beneficio
y el de los demás, cooperamos aportando nuestro pequeño granito de arena
dentro de la Comunidad para lograr grandes cosas.

Soy parte de las personas que creen que todo se puede mejor, que podemos
tener una sociedad más justa, más libre, más equitativa, más
responsable, más generosa, más honesta, más comprometida, más correcta,
en fin, una sociedad más moderna en donde todas las personas sean
tratadas de la forma que se lo merecen, en donde todos tengamos las
mismas posibilidades, derechos y deberes.

Si depués de leer esto, no lográs ver la luz al final del tunel, es que
estás realmente ciego. Sigue encerrado en tu pequeño mundo, yo seguiré
aportando para intentar hacer una mejor sociedad para las futuras
generaciones.

Los programas libres no necesitan volverse comerciales, en estos
momentos los programas libres compiten con los privativos, y cada vez
mas personas los utilizan, promueven. Las empresas que indicas no ganan
dinero vendiendo software sino que venden SERVICIOS. Y eso lo tenemos
claro, no son beneficencias, tienen que pagar sueldos, arriendos, etc.

Segundo, No es un ataque a los windozers, sino una forma de explicar el
verdadero espiritu del software libre, que entiendan la filosofia que
plasma a quienes queremos promover y difundir los beneficios y ventajas
del software libre, no solamente en el ambito informatico, sino que en
todo quehacer humano. Te imaginas un mundo en el cual los cientificos
compartieran conocimientos para desarrollar y mejorar los medicamentos
actuales, en vez de ocultar los conocimientos y aprovecharse de esa
necesidad para cobrar altos precios?

El código libre, le interesa a mucha más gente de la que crees, no solo
a los informáticos, te has puesto a pensar que los grandes estudios
cinematograficos desarrollan estupendas aplicaciones para hacer mejores
efectos especiales en sus producciones, tomando el código de
aplicaciones libres y desarrollando, mejorandolo, etc.

Windows XP triunfo no por ser el mejor sistema operativo, sino por venir
incomporado en cada Pc que se vendia, no fue por elección.
Afortunadamente, esa tendencia esta cambiando, grandes empresas
fabricantes de computadores, estan dando la oportunidad de elegir que
sistema operativo quieres cuando compras tu PC, y eso no es gracias a
Microsoft.

Antes de internet, muchos admirabamos a microsoft, pero gracias a la era
de las comunicaciones, se han descubierto las maquiavelicas tácticas que
utilizo esta empresa para llegar al lugar donde esta. Repito lo mismo
que he dicho en oportunidades anteriores, no es odio lo que siento hacia
microsoft, sino una gran decepción.

Este movimiento no es una secta, es una comunidad que desea lo mejor
para la humanidad, y no solo en el ambito informatico, debemos trabajar
para hacernos cada vez mejor, y de esta forma mejorar nuestra sociedad.
Es cierto que somos pocos, pero cada día somos más, de a gotita un día
llegaremos a ser un oceano. Solo hay que darle tiempo al tiempo.

Saludos.
