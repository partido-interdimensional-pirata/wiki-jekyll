---
title: Proyectos sociales colaborativos
layout: post
categories: []
---

Con los hechos ocurridos en Soldati esta claro que hay problemas
sociales que necesitan ser atacados antes de concentrarnos en políticas
digitales y para atacar esos problemas lo mejor seria una estrategia
colaborativa, distribuida y autónoma como vemos funcionar a muchos
proyectos online.

Para esto es que tenemos que formar una infraestructura útil tanto para
la colaboración online como en la vida real.
