---
title: Translations:Carta Orgánica/18/en
layout: post
categories: []
---

The Affiliated Pirates have the right to attend the afore mentioned
activities with voice, vote and active participation, following their
individual faculties, voluntarism and/or Assembly delegation.
