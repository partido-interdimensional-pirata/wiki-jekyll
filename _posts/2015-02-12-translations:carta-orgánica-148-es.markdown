---
title: Translations:Carta Orgánica/148/es
layout: post
categories: []
---

Lo que no pudiese gastarse y demas bienes serán donados a una
organización sin fines de lucro elegida por la última Asamblea General.
