---
title: F-Droid (Software Libre en Android)
layout: post
categories:
- Software Libre
- Guías
- Privacidad
---

[Categoría:Software Libre](Categoría:Software_Libre "wikilink")
[Categoría:Guías](Categoría:Guías "wikilink")
[Categoría:Privacidad](Categoría:Privacidad "wikilink")

F-Droid es un instalador de aplicaciones libres para Android, que
reemplaza a GooglePlay. Si te preocupa que Google obtenga y utilice
datos privados tuyos y/o querés asegurarte que todo el software que usás
en tu Android sea libre, F-Droid es lo que necesitás P)

Se instala descargando la aplicación desde el sitio
[1](https://f-droid.org)

En la configuración es necesario permitir la instalación de aplicaciones
fuera de GooglePlay.

` Configuración > Aplicaciones > Orígenes desconocidos`

Una vez instalado vamos a tener acceso a \~900 aplicaciones libres.

Repositorio Pirata
------------------

En F-Droid es posible agregar repositorios alternativos. El nuestro es
[2](https://droid.partidopirata.com.ar) y se agrega así:

` F-Droid > Menú > Gestionar repositorios > Menú > Repositorio nuevo > `[`https://droid.partidopirata.com.ar`](https://droid.partidopirata.com.ar)
