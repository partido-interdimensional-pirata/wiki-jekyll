---
title: Translations:Carta Orgánica/10/en
layout: post
categories: []
---

A sympathetic adherent to the Pirate Party is any person without
distinction who under their own faculties manifest agreement and
comformity with the Party\'s Founding Principles.
