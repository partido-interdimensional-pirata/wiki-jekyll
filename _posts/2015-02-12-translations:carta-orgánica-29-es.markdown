---
title: Translations:Carta Orgánica/29/es
layout: post
categories: []
---

En concordancia con la concepción de los medios de comunicación masiva
como espacio público, el funcionar pirata implica, acorde al principio
de acceso libre y universal a la red, ayudar, sea con capacitación, o
con recursos técnicos, a quienes tengan dificultad de cualquier índole
para acceder a esta forma organizativa.
