---
title: Adopta un político
layout: post
categories: []
---

La mayoría de los políticos no sabe de la existencia de los movimientos
piratas ni conoce sus necesidades, intereses y problemáticas. Es por
esto que necesitamos establecer un dialogo con la clase política para
educarlos en las problemáticas y necesidades que trae esta nueva era
digital.

Proceso
-------

### Elije un político

Puede ser quien quieras, tu intendente, gobernador, un senador o
diputado que te caiga particularmente bien o alguno al azar que crees
que puede estar interesado una vez que conozca las problemáticas.

### Busca información de contacto

mail, twitter, blog, etc. Sus publicaciones ya te van a dar una idea de
si puede estar interesado o no

### Ponete en contacto

escribile, llamalo, lo que quieras. explicale de que se trata el
movimiento pirata, cuales son nuestros intereses y que lo vas a mantener
actualizado sobre nuestras luchas.

### Informalo y educalo

Hacele saber que esta pasando en el mundo de la lucha pirata, las
noticias, los avances, los retrocesos, por que es un problema tal o cual
cosa.

Links
-----
