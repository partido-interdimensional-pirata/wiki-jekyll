---
title: Translations:Carta Orgánica/7/en
layout: post
categories: []
---

This structure aspires to give birth to the most diverse humanist
movements, not only giving them support, but by continously rethinking
itself in coexistence with every one of the movements that include and
define it.
