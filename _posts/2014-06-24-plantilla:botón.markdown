---
title: Plantilla:Botón
layout: post
categories: []
---

<includeonly><span style="background-color: {{{1}}} display: inline;padding: 0.2em 0.6em 0.3em;font-size: 75%;font-weight: bold;line-height: 1;color: #FFF;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: 0.25em;"></span></includeonly>
<noinclude>Plantilla para hacer un
<span style="background-color: #5CB85C;display: inline;padding: 0.2em 0.6em 0.3em;font-size: 75%;font-weight: bold;line-height: 1;color: #FFF;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: 0.25em;">botón</span></noinclude>
