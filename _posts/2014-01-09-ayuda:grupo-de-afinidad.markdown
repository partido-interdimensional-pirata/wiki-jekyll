---
title: Ayuda:Grupo de afinidad
layout: post
categories:
- Traducción
- Autoformación
---

[Categoría:Traducción](Categoría:Traducción "wikilink")
[Categoría:Autoformación](Categoría:Autoformación "wikilink")

Traducido de [Starhawk](http://www.starhawk.org): [Grupos de
Afinidad](http://www.starhawk.org/activism/trainer-resources/affinitygroups.html)

Esta fue una [traducción
colaborativa](https://pad.partidopirata.com.ar/p/grupos-de-afinidad) en
enero del 2014, a partir de una discusión sobre si los barcos piratas
son comisiones o grupos de afinidad.

Grupos de Afinidad
==================

**¡Organizate en nodos! ¡Hacé un grupo con tus amigxs! ¡Sean ruidosos!
¡Luzcan apasionantes! ¡Diviértanse!**

¿Qué es un grupo de afinidad?
-----------------------------

Un grupo de afinidad es un grupo de personas con afinidades entre ellas,
que conocen debilidades y fortalezas de lxs otrxs, se apoyan y realizan
(o lo intentan) trabajo político juntas. La mayoría de nosotrxs
probablemente tengamos alguna experiencia infantil/formativa en ser
parte de un grupo ya sea informalmente, como en un grupo de chicxs de la
misma edad que viven en la misma calle, barrio o ciudad, o formalmente,
como ser parte de un equipo deportivo. Sin embargo, los grupos de
afinidad se diferencian de estos por muchas razones, que se explican a
continuación (en jerarquía, confianza, responsabilidad para con el
resto, etc.)

El concepto de grupos de afinidad tiene una larga historia. Se
desarrollaron como una estructura organizativa durante la Guerra Civil
española y han sido utilizados con éxito sorprendente durante los
últimos 30 años por movimientos feministas, anti nucleares,
ambientalistas y de justicia social de todo el mundo. Fueron utilizados
por primera vez como una estructura a gran escala para un bloqueo no
violento durante la fuerte ocupación de 30.000 personas en la estación
nuclear alemana de Ruhr en 1969, y luego en las ocupaciones/bloqueos
estadounidenses de la estación nuclear Seabrook en 1977 donde 1.400
personas fueron arrestadas y de nuevo muchas veces en el altamente
exitoso movimiento antinuclear estadounidense de los \'70 y \'80. Su uso
como sostén de lxs activistas en situaciones de altos niveles de
represión policial ha sido confirmada una y otra vez. Recientemente, han
sido utilizados constructivamente en las protestas masivas de Seattle y
Washington.

No es necesario utilizar la expresión \"grupo de afinidad\" - equipos de
bloqueo, grupos de acción, células, colectivos de acción, etc; se han
utilizado para describir el mismo concepto. Lo mejor sería encontrar el
nombre más relevante dependiendo de cuándo y dónde la estructura sea
utilizada. También, cada grupo de afinidad puede elegir su propio
nombre. Por ejemplo, en la protesta AIDEX, había un \"Grupo de Afinidad
Perseverancia\" que llevaba el nombre del bar en Fitzroy donde sus
miembros tuvieron su primera reunión. Otros nombres se extienden a
través de toda una gama de sensibilidades políticas (o la falta de
ellas) desde los \"Arboles Gritones\", los \"Alcohólicos contra la
Bomba\", hasta la \"Tribu de acción de Buckrabendinni\".

¿Con quién puedo formar un grupo de afinidad?
---------------------------------------------

La respuesta simple a esto es la gente que conoces, y que se sienten de
la misma manera sobre el(los) tema(s) en cuestión. Podrían ser las
personas que ves en un taller (Hacklab), trabajas, salís con, o vivís
con. El punto a destacar,sin embargo, es que tengan algo en común aparte
de la actividad que los está juntando, y que confíes en ellxs y que
ellxs confíen en vos.

Un aspecto importante de ser parte de un grupo de afinidad es llegar a
conocer la posición de cada unx en relación con la campaña o tema. Esto
puede implicar tener una comida juntxs, y discutirlo después de haber
comido, o hacer juntxs algún tipo de entrenamiento relacionado con el
activismo, como asistir a un taller de resolución o facilitación no
violenta de conflictos, desarrollo de estrategias de des-arresto si es
necesario, trabajar en cómo hacer frente a ciertas tácticas de la
policía como escuadrones de asalto, caballos de la policía.

Todxs deben tener una idea compartida de lo que se quiere de forma
individual y colectiva de la acción/campaña, cómo va a ir posiblemente,
qué tipo de apoyo se necesita de los demás, y lo que podés ofrecer a los
demás. Sirve de ayuda si se tiene un acuerdo sobre algunas cosas
básicas: qué tan activo, cuán espiritual, qué tan no-violento, qué tan
suave, qué tan molesto, cuán dispuestos a correr el riesgo de detención,
cuándo se abandona, su perspectiva política global, etc. Pero entonces
de nuevo, es posible que todos sólo compartan un mismo trabajo, escuchen
la misma música o hagan alpinismo, etc.

Organización
------------

Dentro de un grupo de afinidad existe un amplio rango de roles distintos
que lxs miembrxs pueden asumir. Muchos de estos roles estarán
determinados por el objetivo o razón de ser del grupo, pero podría
incluir unx vocerx, para hablar o tratar con los medios, unx
facilitadorx para las decisiones rápidas, primeros auxilios para hacerse
cargo de la gente lastimada, unx vocerx que lleve las ideas y decisiones
a otros grupos, unx observadorx legal, apoyo en arrestos, etc. Así como
estos roles pueden ser asumidos dentro de un grupo de afinidad, el grupo
puede tomar un rol especializado en su interacción con otros grupos de
afinidad o dentro de la campaña o protesta. Puede haber grupos de
afinidad especializados en la vigilancia hacia la policía, contra las
infiltraciones, observación legal, comida, comunicación y vinculación,
medicina, performance artística, o cualquiera otra actividad común. Con
este enfoque en los roles, cada grupo de afinidad puede hacer su trabajo
y apoyar el trabajo de los otros. De esta forma, muchos grupos de
afinidad forman una red interdependiente que logra mucho más que un
grupo grande de activistas individuales.

En el contexto de una manifestación, es tan importante el grupo que está
en la calle, como lo es el equipo de apoyo. Ellxs hacen todas las cosas
cotidianas, y lamentablemente no reciben el reconocimiento que merecen.
Pueden pasear/alimentar los animales domésticos, regar las plantas,
cuidar niñxs, llamar a los empleadores y enloquecidxs padres/hijos,
pagar el alquiler, etc. Como consecuencia, más gente puede participar(y
arriesgar mas) porque tienen ayuda con estas cosas. El apoyo emocional
no debe ser subestimado; y fuera de lo que ofrecen los abrazos, besos y
las llamadas telefónicas, la gente se siente lo suficientemente segura
para arriesgarse cuando saben que tienen apoyo emocional. El equipo de
apoyo también puede apoyar indirectamente la acción directa, mediante el
suministro de información a los medios de comunicación y los grupos
comunitarios interesados, la recaudación de fondos y apoyo logístico,
como el suministro de alimentos, agua y alojamiento. El aspecto en la
calle de un grupo, y su equipo de apoyo puede (y debe )intercambiarse,
así hay una clara comprensión dentro de él, en cuanto a la importancia
de todos los roles en la efectividad del grupo.

El objetivo al final del día es cuidar de uno mismo y de sus pares,
divertirse y trabajar hacia un grado maximizado del cambio social
constructivo.

Palabras clave
--------------

-   Confianza
-   Solidaridad
-   Autosuficiencia
-   Seguridad emocional
-   Ayuda mutua
-   Abierto o cerrado
