---
title: Translations:Carta Orgánica/30/en
layout: post
categories: []
---

The Pirate Party agrees to maintain, develop and implement means of
communication and participation following the Principles and Spirit of
this document. The Party\'s responsible as a whole of fostering access,
knowledge and training for the appropiation, utilization and
re-utilization of these infrastructures, digital, social and political
as well.
