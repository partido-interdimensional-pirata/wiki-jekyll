---
title: Propuesta:Utopía de Software Libre
layout: post
categories: []
---

Ya lo tengo listo en el
[repositorio](https://github.com/piratas-ar/utopia.partidopirata.com.ar/tree/feature/software-libre)
y permiso de RMS para editarlos. Son todos artículos de gnu.org.

Faltaría dividir en dos porque son muchos artículos (sería 200 páginas
en A6 sino!) y elegir un color para las tapas. Luego imprimir y
distribuir :P
