---
title: Enmiendas a Argentina Digital
layout: post
categories:
- Noviembre 2014
- Argentina Digital
---

[Categoría:Noviembre 2014](Categoría:Noviembre_2014 "wikilink")
[Categoría:Argentina Digital](Categoría:Argentina_Digital "wikilink")

Articulado de las Recomendaciones para el Proyecto de Ley Argentina Digital
===========================================================================

Título 1, Capítulo 1
--------------------

### ARTICULO 1.- Objeto

> \"\...estableciendo y garantizando la completa neutralidad de las
> redes.\"

**Sugerencia:** agregar el dictamen de mayoría de Neutralidad de las
Redes del 22 de octubre como anexo al proyecto de Argentina Digital,
quedando:

> \"Apruébase el Dictamen de Neutralidad de las Redes, que como ANEXO I
> forma parte del presente.\"

### ARTICULO 5.- Inviolabilidad de las comunicaciones.

> \"La correspondencia, entendida como toda comunicación que se efectúe
> por medio de Tecnologías de la Información y las Comunicaciones (TIC)
> autorizadas, entre las que se incluyen los tradicionales correos
> postales, el correo electrónico o cualquier otro mecanismo que induzca
> al usuario a presumir la privacidad del mismo, es inviolable. Su
> interceptación sólo procederá a requerimiento de juez competente.\"

**Sugerencias:**

1.  Eliminar la condición de autorizadas.
2.  Cambiar \"Su interceptación sólo procederá a requerimiento de juez
    competente\" por \"Su interceptación, registro y análisis sólo
    procederá a requerimiento de juez competente\"

> \"La correspondencia, entendida como toda comunicación que se efectúe
> por medio de Tecnologías de la Información y las Comunicaciones (TIC),
> entre las que se incluyen los tradicionales correos postales, el
> correo electrónico o cualquier otro mecanismo que induzca al usuario a
> presumir la privacidad del mismo, es inviolable. Su interceptación,
> registro y análisis sólo procederá a requerimiento de juez
> competente.\"

### ARTÍCULO 6: Definiciones generales

**Sugerencia:** agregar la definición de Red Comunitaria

> En lo que respecta al régimen de las Tecnologías de la Información y
> las Comunicaciones en general y de las Telecomunicaciones en
> particular, se aplicarán las siguientes definiciones:
>
> f\) Red Comunitaria: Es un servicio de conectividad de acceso abierto,
> libre, sin fines de lucro, organizado y gestionado por sus mismos
> usuarios.

### ARTÍCULO 7: Definiciones particulares

**Sugerencia:** agregar la definición de Punto de Intercambio.

> i\) Punto de Intercambio (NAP, IXP): es la infraestructura física donde
> las redes de los operadores de una misma región convergen e intercambian
> tráfico libremente, de manera abierta, gratuita y neutral.

TÍTULO n: Redes Comunitarias
----------------------------

### Capítulo n.1: Disposiciones Generales

**ARTÍCULO n.1.- Reconocimiento de las Redes Comunitarias**. Las
instituciones y organismos gubernamentales y no gubernamentales podrán
actuar como prestadores de Servicios TIC y gozarán de plenos derechos
cuando dicha prestación se realice de forma abierta, libre, transparente
y sin fines de lucro. Adicionalmente podrán requerir, si así lo
quisieran, Licencias de Servicios de TIC sin costo asociado.

**ARTÍCULO n.2.-** Prohíbese a los beneficiarios de los permisos la
venta y/o cualquier otro tipo de comercialización de los servicios de
acceso a la red. En caso de constatarse que dichos permisos se utilizan
para prestar servicios a terceros a título oneroso, los permisos
caducarán de pleno derecho, sin resarcimiento alguno.

**ARTÍCULO n.3.-** Las Redes Comunitarias podrán brindar Servicios de
TIC en las bandas no-licenciadas mediante la utilización de equipamiento
homologado, sin la necesidad de abonar tasas por la declaración de sus
estaciones concentradoras.

**ARTÍCULO n.4.-** En los casos en que los servicios prestados por Redes
Comunitarias requieran de asignación de espectro en bandas licenciadas,
la Autoridad de Aplicación evaluará la factibilidad de la asignación en
el área de cobertura específica, considerando la reutilización de bandas
que no estén siendo aprovechadas o asignando frecuencias nuevas. De
aprobarse, el permiso se brindará sin imponer a la Red Comunitaria
solicitante aranceles por la asignación o el uso.

### Capítulo n.2: Régimen de Promoción de Redes Comunitarias

**ARTÍCULO n.5.- Promoción de las Redes Comunitarias.** El Fondo
Fiduciario del Servicio Universal destinará un porcentaje mayor o igual
al 2% de los fondos para otorgar subsidios a la creación de nuevas Redes
Comunitarias y la expansión o mejora de las existentes.

**ARTÍCULO n.6.-** Se exime a las Redes Comunitarias del pago de
cualquier tipo de tasas o aranceles en relación con su actividad en
Servicios de TIC, sean estos privados, municipales, provinciales o
nacionales.

**ARTÍCULO n.7.-** La Redes Comunitarias podrán participar sin costo de
los Puntos de Intercambio y Nodos satelitales regionales.

TÍTULO V Capítulo 4: Acceso e interconexión
-------------------------------------------

**ARTÍCULO n.-** La Autoridad de Aplicación garantizará la creación de
Puntos de Intercambio regionales y el transporte entre los mismos.

**ARTÍCULO n.-** Los Puntos de Intercambio estatales, académicos o
privados, deberán operar siempre sin fin de lucro.

**ARTÍCULO n.-** Las empresas que provean Servicios TIC deberán
conectarse a los Puntos de Intercambio de las zonas donde operen, con
suficiente capacidad para mantener una calidad de servicio aceptable. La
Autoridad de Aplicación definirá los parámetros de medición de calidad
de servicio y sus límites.

TÍTULO VII: Consideraciones Generales sobre los Servicios de TIC
----------------------------------------------------------------

**ARTÍCULO 56. Calidad del Servicio.** La tasa de transferencia deberá
ser simétrica.

**ARTÍCULO 57.-** Se garantizará la provisión a los usuarios finales de
Direcciones de Internet (IPv6 o el protocolo que lo reemplace) públicas
y fijas sin costos adicionales.

**ARTÍCULO 58.-** Los usuarios de Servicios de TIC estáran facultados a
extender a terceros los servicios que reciben, siempre que esta
actividad se realice sin un fin de lucro.
