---
title: ConvivenciaPpar
layout: post
categories: []
---

Convivencia Pirata
==================

### Código de conducta para el canal \#ppar del Partido Interdimensional Pirata

Con la idea de que este canal sea un espacio de acercamiento al [Partido
Interdimensional
Pirata](https://wiki.partidopirata.com.ar/P%C3%A1gina_principal) libre,
inclusivo, seguro, transfeminista, no sexista, no fascista, pensamos las
siguientes reglas:

-   Cualquier persona interesada en participar o estar al tanto de lo
    que hacemos en el PiP puede ingresar al canal

Si entraste por primera vez al canal, te invitamos a presentarte (no
hace falta decir datos reales, sólo con el nick alcanza) y a contarnos
qué te atrajo a entrar o qué te interesa del canal. Esto es para
promover la camaradería pirata y que podamos tender lazos y nadie se
quede sola lurkeando en las sombras.

-   Si compartimos conocimientos técnicos, tratemos de incitar la
    participación de todas (aflojemos con las conversaciones entre
    chabones cis de sistemas)

En este canal bienvenimos a todas las personas por igual, tengan
conocimientos técnicos o no, y entendemos que espacios para aprender
entre todas son los que faltan. ¡No intimidemos a las demás e
invitémoslas a participar! Si usamos términos técnicos, los explicamos
para que todas lo entiendan.

-   ¡No toleramos el mansplaining!

Prestemos atención a no realizar ningún tipo de mansplaining hacia las
compañeras mujeres cis, trans, travas o lesbianas. No toleraremos esta
práctica.

-   ¡No toleramos a
    [trolls](https://es.wikipedia.org/wiki/Trol_(Internet))!

No permitiremos bajo ningún motivo: hostigamiento o maltrato hacia
compañeras; comentarios fascistas, machistas, racistas, ni
discriminatorios de ningún tipo; desvío de los temas que tratamos en el
canal o en el PiP con el motivo de imponer una agenda propia que no
corresponde a las temáticas del canal; compartir contenido que no
respete las reglas citadas en este código de convivencia (y sobre todo
en este mismo ítem) sin un marco teórico, crítica o contexto para
entender por qué se comparte; comentar o compartir contenidos en nombre
del PiP que vayan en contra de nuestra ideología o no nos representen, o
ninguna otra forma de trolleo.

Tratamos de aplicar estas reglas a todas las personas que estén en el
canal. Si no se cumplieran, serán invitadas colectivamente a irse P)
