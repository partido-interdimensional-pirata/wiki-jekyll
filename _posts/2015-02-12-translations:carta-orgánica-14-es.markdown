---
title: Translations:Carta Orgánica/14/es
layout: post
categories: []
---

Se considerará afiliado a todos los ciudadanos que reúnan los requisitos
exigidos por la presente Carta Orgánica, la ley electoral vigente y que
procedan a llenar las fichas de afiliación requeridas por esta misma
ley.
