---
title: Servicios seguros
layout: post
categories:
- Privacidad
---

[Categoría:Privacidad](Categoría:Privacidad "wikilink")

Proveedores de correo, chat, etc. que se preocupan por la seguridad y la
privacidad de las personas:

-   [RiseUp](https://riseup.net): uno de los más usados ultimamente,
    tiene correo, chat, VPN, pads, [plataforma de
    organización](Plataformas_de_participación#Crabgrass "wikilink"),
    etc. Es un colectivo yanqui que trabaja en el tema hace años. El
    problema es que están alojados en EEUU.

<!-- -->

-   [Lista de servidores de colectivos activistas de
    RiseUp](https://help.riseup.net/en/radical-servers)
