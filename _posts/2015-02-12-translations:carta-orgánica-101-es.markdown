---
title: Translations:Carta Orgánica/101/es
layout: post
categories: []
---

-   Por el Pirata representado en carácter regular, esto es, con
    anterioridad al hecho y habiendo sido registrado debidamente.
-   Por la Asamblea Pirata en carácter excepcional, *in situ et in
    momentum*;
