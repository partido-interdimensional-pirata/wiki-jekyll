---
title: Translations:Carta Orgánica/19/en
layout: post
categories: []
---

Affiliated Pirates can be selected to act as representatives as needed
by electoral law\'s requirements, or by necessity of a representative
during specific cases following this Charter\'s dispositions and the
Party\'s Bodies organized by it.
