---
title: Propuesta:CharlaFocoLibre
layout: post
categories: []
---

Quiénes van
-----------

-   

¿Qué vamos a hablar?
--------------------

Tema: Horizontalidad como forma de organización política Resumen: TBD

¿Qué es Foco Libre?
-------------------

El Foro de Conocimiento Libre -- FoCo Libre es un espacio de difusión de
iniciativas y proyectos comunitarios, académicos, públicos y privados
asociados con el conocimiento libre. También es un ámbito de reflexión y
debate acerca de las implicancias sociales, políticas, económicas y
tecnológicas vinculadas con la libre circulación de la cultura y el
conocimiento. Además es un lugar de encuentro y trabajo conjunto para
todas las organizaciones y comunidades que trabajan sobre estas
temáticas.
