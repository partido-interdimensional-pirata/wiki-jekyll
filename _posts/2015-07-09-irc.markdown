---
title: IRC
layout: post
categories: []
---

También podes charlar con otros/as piratas [haciendo click
aca](https://webchat.pirateirc.net/?channels=ppar) o utilizando un
cliente IRC como [XChat](http://xchat.org/download/) o
[Pidgin](http://www.pidgin.im/download/).

`Servidor: irc.pirateirc.net`\
`Canal: #ppar`\
`Puerto: 6697`\
**`Y`` ``asegurarse`` ``de`` ``que`` ``este`` ``tildado`` ``Use`` ``SSL`` ``o`` ``Use`` ``secure`` ``connection`**
