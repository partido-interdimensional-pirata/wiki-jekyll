---
title: Translations:Carta Orgánica/49/en
layout: post
categories: []
---

Agreement is a last-chance decision-making method, only used when
Consensus is not possible.
