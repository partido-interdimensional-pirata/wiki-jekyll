---
title: Translations:Carta Orgánica/157/es
layout: post
categories: []
---

El Dominio de un Barco Pirata determina cuáles son sus competencias y
sólo puede ser modificado mediante Propuesta a la Asamblea Permanente.
