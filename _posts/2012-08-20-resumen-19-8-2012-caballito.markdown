---
title: Resumen 19-8-2012 Caballito
layout: post
categories: []
---

Fuimos Jorge, Gala y yo al bar que esta en Doblas y Av. Rivadavia.

Hablamos de:

Resaltar el tema libertad en internt, sopa, como es un espacio en el que
uno puede comunicarse sin distincion de genero, religion, condicion
social o lo que sea y como sopa atenta contra eso.

### Carta Organica - temas a incluir

Presencia virtual en las asambleas. Que las asambleas fisicas permitan
participar por medio de IRC (por ej) y previa identificacion del
afiliado participar. Esto queda guardado como constancia y resumen de la
reunion. En caso de que sean muchos crear algun sistema para que primero
pidan la palabra.

Como crear un PP local, ser al menos 3, que no exista otro PP en la
localidad, adherir a las bases, carta organica y principios, tener
reuniones fisicas abiertas al menos 1 vez al mes, tener un canal de
comunicacion online que permita el intercambio entre miembros sin
moderacion previa. Los miembros de los PP locales participan de los PP
provinciales y el PP nacional.

Proxy party. Permitir a organizaciones con intereses comunes presentar
propuestas que no contradigan los principios piratas. Estas se debaten y
se votan dentro del PP. En caso de adherir se presentarian como proyecto
pirata, en caso de ser rechazada o de requerir modificaciones se le
comunica a la organizacion.

Incluir en la carta organica que somos defensores de todos aquellos que
sean acusador por violaciones al derecho de autor o propiedad
intelectual. Esto nos convertiria en terceros interesados y nos
permitiria presentarnos como defensores en estos casos.

Conseguir una carta organica que no haya sido rechazada y funcione como
plantilla para la nuestra.

### Organizacion

PP barriales! Organizarnos para cubrir la capital. Mas o menos pensamos
en 5 centros:

-   Barracas-hacklab: zona sur y centro
-   Caballito
-   Palermo/Barrio norte
-   Belgrano
-   Oeste (x calle cuenca)

Llegando a 30 miembros fijos se podria encarar el alquiler de un local
barrial y con 150 personas juntando adhesiones/afiliaciones estariamos
mas o menos en numero para las 4 mil.

**Pagina web**

-   Agregar reuniones barriales/locales con dia y lugar. Miembros de
    cada pp barrial.
-   Agregar la opcion de enviar propuestas
-   Seccion destacada de eventos
-   Que el blog de alguna forma este relacionado con los eventos en fb,
    ya sea un post con el link o lo que sea.

Ir a facultades/universidades entre als 7 y 8 a hablar con las
diferentes organizaciones, centros de estudiantes y demas.

Conseguir gente que pueda dar una charla, musicos y bailarines.
Organizar fiestas y eventos. Crear los eventos en facebook y
relacionarlo con el blog de alguna forma.

Tambien charlamos de castigos!!! Lo mandamos a la bodega al que falte a
una reunion, al carajo al que no cumpla con una tarea asignada y lo
hacemos caminar la plancha al que traicione los principios piratas!! Nos
imaginabamos la situacion de presentarle a Servini de Cubria una nota
diciendo \"tal fue mandado al carajo por incumplimiento de tareas\"
xDDDD
