---
title: Reuniones y temáticas
layout: post
categories: []
---

### Objetivos propuestos

-   Redacción del Manifiesto.
-   Redacción de las reglas y netiqueta.
-   Propaganda del Partido, panfletos y marcadores.
-   Eventos: Fábrica de Fallas.

### Temario para la próxima reunión

------------------------------------------------------------------------

Este está a sujeto a modificación inclusive una vez comenzada la
reunión. Se sugiere que no haya más de 3 temas, así no se hace tan
larga. Sugerimos que si desea profundizar en alguna propuesta, puede
dejar un link en la misma wiki explayandosé todo lo que crea necesario,
pero sea considerado, no tenemos todo el día para leer, por lo tanto sea
breve y conciso.

-   Hoja de ruta (próximas acciones a seguir)
-   Contenido de la página
-   Objetivos politicos del Partido
-   Manifiesto
-   Estructura del Partido
-   Opciones y requerimientos de la membresia

### Próximos Eventos

Jueves 22 en el canal irc del Partido Pirata, a partir de las 21hs.
<http://partido-pirata.blogspot.com/2009/09/nuestro-canal-de-irc-para-comunicarnos_20.html>

------------------------------------------------------------------------

-   [Fábrica de Fallas](http://culturalibre.fmlatribu.com/)

21 y 22 de noviembre en nuestra casa de Lambaré 873.
