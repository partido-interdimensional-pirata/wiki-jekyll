---
title: Translations:Carta Orgánica/4/en
layout: post
categories: []
---

The Pirate Party is a functional organization with an horizontal,
participative and decentralized character. This means it\'s not
hierarchical.
