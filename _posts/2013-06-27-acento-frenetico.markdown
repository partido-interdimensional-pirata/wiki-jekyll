---
title: Acento frenetico
layout: post
categories: []
---

COLECTIVO

*\' eti.que.tas\_*\'

*\'heteronorma, porno, arte, amor, lúdico, morbo, pasión, ritual,
afectividad, fusión, sensorial, explícito*

*\' localización*\'

La Plata, Buenos Aires

pagina web\'[1](http://www.acentofrenetico.com/)

*\' Descripción\_*\'

septiembre.2012 /

qué? manifiesto? para qué?

marzo.2012 /

acento frenético toma la imagen pornográfica, entendiendo a la
pornografía como la espectaculización del acto sexual, la utiliza como
puente y medio para resignificar su lugar, se mueve en el circuito del
arte para abrir y concretar su búsqueda artistico-conceptual. se
cuestiona el arte como un agente mas de la heteronorma, consecuente al
sistema heterocapitalista.

cómo nombrarlo entonces?

amor = heteronorma, arte = heteronorma, pasión = heteronorma, porno =
heteronorma, naturaleza = heteronorma.

cómo aborda acento frenético éstos conceptos? qué hace con ellos?
alimenta los estereotipos homo.hetero publicitarios? qué indaga? qué
critica? qué reflexiona? abre? cierra? qué es? para qué? cómo? con
quién? quiénes? cuándo? hasta cuándo?

acento frenético es una posibilidad de de-construcción.

marzo.2011 /

acento frenético rompe con la heteronorma sexual, proponiendo una
renovación del género porno y naturalizándolo como hecho artístico.

el amor abordado desde lo lúdico, el morbo, la pasión, generando un
ritual donde el cuerpo es estimulado sexual y afectivamente. permitiendo
entender al arte sin pudo, trabajando con la creación de un espacio
donde se refleja la fusión entre lo sensorial y lo explícito.

acento frenético se manifiesta desde un porno que se expande de los
marcos pre-establecidos y que parte de la naturaleza del ser.

[Category:Nodo](Category:Nodo "wikilink")
