---
title: Translations:Carta Orgánica/65/es
layout: post
categories: []
---

Como lugar de Asamblea Permanente, el Partido Pirata reconocerá por
reglamento plataformas digitales de participación que representen los
principios definidos en este documento.
