---
title: Archivo:La Batalla del Copyright.jpg
layout: post
categories: []
---

[La batalla del
copyright](https://secure.flickr.com/photos/christopherdombres/5814893360/in/photostream)
\-- por [Christopher
Drombres](https://secure.flickr.com/photos/christopherdombres/),
[cc-by](http://creativecommons.org/licenses/by/2.0/deed.en)

[Category:Media](Category:Media "wikilink")
