---
title: Apostasía de Google
layout: post
categories:
- Apostasía de Redes Sociales
---

[Categoría:Apostasía de Redes
Sociales](Categoría:Apostasía_de_Redes_Sociales "wikilink")

Apostasía de Google
===================

Reemplazar el buscador
----------------------

Hay un montón de buscadores que dicen que respetan nuestra privacidad.
Uno de los más conocidos es [DuckDuckGo](https://duckduckgo.com/). Sin
embargo, algunas personas desconfían de DDG, porque no es software
libre, aunque donan 10% de sus ingresos por publicidad a proyectos de
software libre.

Existe un proyecto libre llamado [SearX](https://searx.me/), que como
DDG, hace búsquedas en nuestros nombre en Google y otros buscadores,
pero sin entregar nuestra identidad. Se les llama meta-buscadores.

Al ser libre, puede haber muchas instancias de SearX funcionando y
podemos elegir la que más nos guste (¡o tener una nosotras!).

[Listado de instancias de SearX](https://stats.searx.xyz/)

¡Las piratas hemos estado usando SearX desde hace varios años sin
extrañar los resultados de Google!

Reemplazar el correo
--------------------

Hay muchos proveedores de correo electrónico, pagos y gratuitos, para
elegir. El factor decisivo es la confianza que genere el proyecto detrás
de ese servicio.

Para usos activistas, recomendamos [RiseUp](https://riseup.net/), un
colectivo norteamericano y [Autistici](https://autistici.org/), de
Italia, que ya tienen un recorrido de varios años proveyendo servicios
de correo, entre otros.

Hay tablas comparativas de proveedores de correo electrónico, para quienes gusten investigar por su propia cuenta:
* https://www.prxbx.com/email/
* https://www.privacytools.io/providers/email/


Cifrar el correo
----------------

Podemos usar este cliente de correo electrónico con interfaz de chat que cifra de manera oportunista con GPG usando el protocolo autocrypt: https://delta.chat/es/

Si queremos enviar correos electrónicos cifrados con un cliente más estándar, como Thunderbird, esta guía detalla un procedimiento más manual: https://emailselfdefense.fsf.org/es/

Para hacerlo desde el celular, por otro lado, se puede usar k9mail.


Reemplazar los mapas
--------------------

https://osmand.net/


Reemplazar la tienda de aplicaciones
------------------------------------

https://f-droid.org/packages/org.gdroid.gdroid/


Reemplazar el traductor
-----------------------

https://www.apertium.org/
https://tatoeba.org/
http://goldendict.org/


Reemplazar la edición de documentos en línea
--------------------------------------------

* Procesador de texto: https://etherpad.org/
* Planilla de cálculo: https://ethercalc.net/
* Documento markdown (también acepta LaTeX, HTML, etc.): https://github.com/hackmdio/codimd#codimd---the-open-source-hackmd


Reemplazar el alojamiento de videos
-----------------------------------

https://joinpeertube.org/

Para ayudar en la transición:
* Cliente de YouTube para el celular que bloquea publicidades y permite descargar: https://newpipe.schabi.org/
* Complemento para el navegador que nos avisa si un video de YouTube está disponible en Peertube: https://addons.mozilla.org/es/firefox/addon/peertubeify/
* Programa para la terminal de GNU/Linux que permite descargar videos de YouTube y otras fuentes: https://yt-dl.org/

