---
title: Translations:Carta Orgánica/127/es
layout: post
categories: []
---

Los Fondos del Partido serán depositados en una cuenta única del Banco
de la Nación Argentina o de la Ciudad Autónoma de Buenos Aires, a nombre
del Partido Pirata y a la orden conjunta o indistinta de hasta cuatro
(4) Piratas Afiliados, de los cuales dos (2) deberán ser el Secretario
General y el Secretario de Administración y Actas, quienes cumplen la
función de los cargos previstos en el artículo 20 de la ley 26.215, uno
de los cuales, necesariamente, deberá suscribir los libramientos que se
efectúen, mientras que los otros dos (2) serán parte del Órgano
Fiduciario.
