---
title: Manifiesto Pirata Borrador (España)
layout: post
categories: []
---

Preámbulo

El propósito de la democracia es que el gobierno represente al pueblo.
Cuando un gobierno no representa al pueblo, sino a una cierta subclase,
eso no es democracia, eso es aristocracia. Cuando un gobierno no atiende
a los hechos, sino a las creencias, no es una democracia, sino una
teocracia. Cuando un gobierno no escucha al pueblo, o al imperio de la
ley, no es una democracia, es una dictadura. Creemos que la democracia
es el ideal, y es a tal fin que aspiramos y por el que trabajamos.

Cuando las leyes, las constituciones, o el tejido de una sociedad son
ignorados para consolidar las bases del poder de quienes tienen el
control, cuando los derechos y libertades son ignorados o considerados
menos importantes que la protección de algún tipo de evento extraño,
cuando las protecciones en sí mismas son la causa de los efectos que
supuestamente tratan de evitar, cuando los castigos y las protecciones
no evitan sino que incitan, cuando la codicia de unos pocos se considera
más importante que los derechos de la mayoría, debemos cuestionarnos los
motivos de aquellos que buscan todo esto.

Con este fin, establecemos estos Partidos Piratas por todo el mundo, y
afirmamos nuestro común propósito en este nuestro manifiesto de acción,
para que la sociedad no se derrumbe por causa de escarbar en busca de
las migajas del presente, sin pensar ni preocuparse en el futuro.
\[editar\] Sobre derechos y libertades

La plena democracia sólo puede ser alcanzada a través de la protección
del imperio de la ley y la defensa de cinco derechos humanos básicos,
que son la verdadera base de la democracia:

`   * libertad de expresión,`\
`   * privacidad,`\
`   * presunción de inocencia y derecho a un juicio justo,`\
`   * igualdad ante la ley y no discriminación,`\
`   * y derecho a la vida y a la integridad física y moral. `

Para entender el contexto del últimos punto, podemos decir que todo el
mundo tiene derecho a la vida y a la integridad física y moral, y que
bajo ninguna circunstancia debe ser sujeto a tortura, castigo o trato
alguno inhumano o degradante y no podra en modo alguno ser discriminados
por razón de nacimiento, raza, sexo, religión, opinión o cualquier otra
condición o circunstancia, personal o social.

Ninguna reivindicación o postura tiene validez alguna si no es expresada
por medio de pensamiento y argumentación razonados; la violencia en
ningún modo es camino para conseguir fines políticos en una democracia.

Las leyes penales existentes y futuras, incluyendo las leyes
antiterroristas, deberán ser revisadas para garantizar su compatibilidad
con los derechos humanos y las libertades civiles, y reformadas cuando
aquéllas entren en conflicto. \[editar\] Sobre restricciones de ideas e
información

El uso de expresiones como propiedad intelectual tiene que cesar. Es
nocivo y lleva a engaño, dado que confunde aspectos legales en torno a
bienes inmateriales con el propósito de confundirlos con la propiedad
que una persona puede tener sobre los bienes materiales. \[editar\]
Sobre Patentes

El actual sistema de patentes no es sostenible; las biopatentes y las
patentes de software son dos áreas donde resulta evidente que existe la
necesidad de un cambio.

Así mismo, no debe permitirse que las patentes farmacéuticas provoquen
la existencia de fármacos no disponibles para algunos países o grupos
sociales; ésto se hace más evidente en el caso de pandemias y
emergencias naturales. Estas medidas, combinadas con otras como la
reducción de la vigencia de las patentes, serán perseguidas con el fin
de disminuir el impacto de los monopolios creados por el sistema de
patentes. \[editar\] Sobre Marcas Registradas

Las marcas registradas están pensadas, como su nombre sugiere, como
etiquetas para identificar un bien o servicio. Estamos a favor de las
marcas registradas, en la medida en que benefician a todos.

El abuso de las marcas registradas, como sustituto de o añadido a los
derechos de autor, es algo que no se puede permitir que continue, dado
que mina la confianza del público. Acorde con ésto, el material sujeto a
derechos de autor no podrá ser susceptible de convertirse en marca
registrada, y del mismo modo las marcas registradas no podrán quedar
sujetas a derechos de autor. \[editar\] Sobre Derechos de Autor

El término propiedad intelectual se usa a menudo con el fin de confundir
a la gente, permitiendo la extensión de los derechos de autor; los
derechos de autor son derechos. No son propiedad, y no deberán ser
tratados como tal, para ser traspasados de generación en generación. En
ningún otro campo se da que el trabajo de una persona siga dando rentas
a sus descendientes. Más aún, dado que no es, y nunca ha sido,
propiedad, la noción de robo es en este contexto, completamente carente
de sentido.

Estos hechos requieren que las leyes que afectan a los derechos de autor
cumplan con los siguientes principios: reducción de la vigencia de los
derechos materiales de autor; libre compartición sin ánimo de lucro de
obras culturales, lo que implica el cese de la persecución de las
herramientas destinadas a la compartición de cultura tales como las
herramientas P2P; y, dada nuestra postura en cuanto a que un incremento
de la compartición de cultura debería ser vista como un desarrollo
positivo dentro de la sociedad, la abolición del canon por copia
privada.

El equilibrio entre los derechos de autor y el resto de los derechos
humanos es necesario. Hoy, la balanza está desequilibrada, aunque no del
lado del autor, sino del lado de los editores que han alienado los
derechos de los autores, y el desequilibrio crece sin control y con la
connivencia de las autoridades; el equilibrio debe ser restaurado, no
sólo para restaurar a los ciudadanos el conjunto de sus derechos, sino
también para evitar que los autores jamás puedan ser alienados.
\[editar\] Sobre la Sociedad de la Información

Los avances tecnológicos han permitido que todo el mundo pueda ejercer
sus derechos y abrazar sus libertades de formas nunca antes imaginadas.
Ha permitido una participación mayor en la democracia, y ha ayudado a
eliminar barreras entre las gentes de todo el mundo; sin embargo,
indebidamente usado, pudieran convertirse en una herramienta de
división, discriminación y recorte de derechos y libertades. Para evitar
esto y extraer el máximo partido a todo lo que la Sociedad de la
Información puede ofrecer, hay tres objetivos por alcanzar.

`   * Universalización del Internet tanto alámbrico como inalámbrico es necesario si queremos que todos disfrutemos de los beneficios de la Sociedad de la Información y evitar una brecha digital que podría surgir, y de hecho surge, debido a condiciones sociales, personales o geográficas.`\
`   * Preservar la neutralidad en la Red, dado que es una de las principales vías para asegurar una Sociedad de la Información libre de peligros como la censura, los ataques a la privacidad, y el surgimiento de monopolios de las telecomunicaciones.`\
`   * La neutralidad tecnológica de las Administraciones Públicas, a través del uso obligatorio, por parte de éstas, de formatos y estándares abiertos, los cuales potenciarán las relaciones entre los ciudadanos y las Administraciones Públicas, incrementando la accesibilidad. `

\[editar\] Sobre la transparencia y responsabilidad de los gobiernos

El deber primario de los representantes electos es representar a sus
electores. El papel principal de los miembros gubernamentales es
asegurar la buena marcha de su gobierno. Ambos deben hacer esto al
tiempo que cumplen y apuestan tanto por el imperio de la ley, como por
los derechos y libertades. En aras de asegurar esto, estos cargos
públicos deben ser transparentes en el desempeño de sus acciones y
actividaes, y el escrutinio público de los asuntos de Estado no sólo
deberá ser permitido, sino que debe tener lugar.

Las elecciones producen un compromiso entre el electorado y los electos,
uno basado en una promesa, un manifiesto, o una declaración de
intenciones. Debido a esto, no deben los electos caer en promesas hechas
a la ligera, y deber ser no sólo legal sino también políticamente
responsables de sus actos en campaña electoral.

Sin transparencia o sin responsabilidad, no puede haber confianza. Un
gobierno en el que no se confía, no puede ser un gobierno del pueblo
para el pueblo, y por ende no puede ser considerado democrático. Porque
creemos en la democracia, los ciudadanos merecen ser representados por
políticos que hagan del servicio público su máxima prioridad. \[editar\]
Sobre los temas ajenos al ideario

Tener posturas en cada uno de los temas no hace que un partido se
preocupe por los problemas de la gente; en lugar de eso, lo que hace es
que los partidos ofrezcan programas monolíticos en los cuales la mayoría
de sus votantes no se sienten debidamente representados, tanto menos
representados cuanto mayor sea el número de votos para ese partido. Los
votantes de los partidos mayoritarios intentan verse reflejados en
dichos programas, sólo para descubrir que en temas controvertidos como
impuestos, educación, sanidad y otros, no son capaces de encontrar todo
lo que buscan en un único partido; los programas electorales monolíticos
son un simple tómalo o déjalo.

Para partidos como aquellos que estamos unidos en nuestro movimiento,
centrarnos en habilitar y fortalecer las bases de la democracia y los
derechos y libertades que residen en ésta, asegurar la libertad para que
la información y la cultura fluyan, llevar al mundo a la Sociedad de la
Información, allanar el camino a la innovación, es la mejor manera de
conseguir nuestros objetivos dado que dichos objetivos, objetivos para
permitir que los ciudadanos elijan la sociedad en la que quieren vivir,
requieren de nosotros que seamos fuertes como movimiento; porque la
fuerza proviene de la unidad; y la unidad viene generalmente en
democracia a través del consenso.

Consiguiendo nuestro objetivo, permitiremos que los ciudadanos se
expresen por sí mismos en esos temas controvertidos, incrementando las
opciones para el consenso; y allanaremos el camino para un verdadero
pluralismo en los parlamentos, dado que abrir el conocimiento y la
información a los ciudadanos permitirá que los individuos puedan escoger
la voz que mejor represente sus intereses, siendo una de esas voces la
nuestra. Por consiguiente, apoyarnos constituye la mejor forma de que
los ciudadanos consigan esos fines que quedan ajenos a nuestro ideario.
