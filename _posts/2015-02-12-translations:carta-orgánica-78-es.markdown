---
title: Translations:Carta Orgánica/78/es
layout: post
categories: []
---

El moderador es responsable de mantener el buen clima y el Orden del Día
durante la Asamblea General y de asegurar el carácter definido para
estas.
