---
title: Translations:Carta Orgánica/103/es
layout: post
categories: []
---

-   No aceptar el cargo conferido, o renunciar al mismo si ya hubiese
    sido aceptado.
