---
title: Translations:Carta Orgánica/81/es
layout: post
categories: []
---

La rotación será automática al superar su duración máxima definida por
reglamento, no pudiendo esta superar el término de un (1) año.
