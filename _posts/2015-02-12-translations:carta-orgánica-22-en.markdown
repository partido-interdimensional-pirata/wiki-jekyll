---
title: Translations:Carta Orgánica/22/en
layout: post
categories: []
---

-   Persons referred by the article 24 of lay 23.298.
-   Those convicted for electoral offenses.
-   Those sanctioned for electoral fraud.
-   Those convicted of crimes against humanity or having participated on
    de facto governments.
-   Those who had incurred or incurr on human rights violations.
-   Those who had incurred or incurr on violations of the Pirate
    Founding Principles.
-   Those who were expelled by the dispositions on this Charter.
