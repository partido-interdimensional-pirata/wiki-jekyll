---
title: Translations:Carta Orgánica/57/en
layout: post
categories: []
---

Pirate Party\'s Assemblies have a democratic, organizational, solidary,
friendly, horizontal, decentralized, open and participatory character.
