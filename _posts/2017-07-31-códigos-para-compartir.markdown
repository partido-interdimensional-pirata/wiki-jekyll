---
title: Códigos para compartir
layout: post
categories: []
---

\>\> Codigos para compartir, hackear, piratear en libertad.
===========================================================

> PPAr utiliza el femenino para referirse a las personas en general. En
> ese sentido, alentamos las diferentes formas, estrategias y
> herramientas para incorporar prácticas no antropocéntricas, sexistas,
> ni cisexistas en la lengua. Otras alternativas que apoyamos son el uso
> de la letra e, arrobas, equis, asteriscos, etc.

Introducción
------------

Este es un ejemplo de código que busca aportar un marco de consenso para
garantizar la asistencia, permanencia y cómoda estadía de todas las
personas que habitan los espacios de software libre, código abierto,
computación, tecnologías, etc. Para esto, fija un piso de conductas
deseables, aceptables, indeseables y/o intolerables para la comunidad.
Podés usarlo sin cambios o modificarlo para adaptarlo a tus actividades.

Este código está en permanente mutación colectiva, y se alimenta, copia
e inspira de las siguientes fuentes:

[<https://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy>](https://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy)

[<https://trans-code.org/code-of-conduct/>](https://trans-code.org/code-of-conduct/)

[<https://openhardware.science/logistics/gosh-code-of-conduct/>](https://openhardware.science/logistics/gosh-code-of-conduct/)

[<https://hypatiasoftware.org/code-of-conduct/>](https://hypatiasoftware.org/code-of-conduct/)

Procuramos mantener y fomentar una comunidad abierta, que invite y logre
la participación de cada vez más personas, en toda su diversidad.
Sabemos que los espacios de Software Libre, informática, sistemas, etc.
son habitados mayormente por varones cis, blancos y de clase media, pese
al reconocimiento de la necesidad de eliminar la brecha de géneros. En
este sentido, este es nuestro pequeño aporte, hecho de prácticas
colectivas de múltiples dimensiones, reflexiones, lecturas, experiencias
que crecen día a día.

Puntos importantes para garantizar que nuestro espacio no resulte expulsivo
---------------------------------------------------------------------------

### Escucharnos a todas y entre todas en un clima seguro

-   Para la escucha activa, preferimos preguntar primero, en lugar de
    hacer juicios.
-   Hacer silencio a veces es la condición para que otras puedan
    animarse a hablar. Escuchar es un ejercicio que requiere práctica.
-   Nos interesa lo que todas tengan para decir. Por lo tanto, si estás
    más entrenada en el ejercicio de participar, hablar, opinar, tené en
    cuenta que quizás haya otras que no lo estén tanto: darles el
    espacio si quieren tomarlo. ¡Pero recordá que incentivar no es lo
    mismo que presionar!
-   Le damos tiempo y espacio a todas para que puedan responder.
-   Ajustamos nuestro comportamiento apropiadamente cuando nos lo pidan,
    para sumar al clima de respeto.
-   Es una falta de respeto repetir un comportamiento dañino que ya se
    identificó como tal. Puede incomodar, lastimar y expulsar a otras,
    por lo que preferimos llamar la atención sobre este punto todas las
    veces que sea necesario y tolerable.
-   Evitamos esto nosotras y ayudamos a otras a darse cuenta cuando lo
    están haciendo.
-   En los casos en los que los llamados de atención resultan
    insuficientes, preferimos apelar a este código para sostener la
    convivencia.
-   Una manera en la que los espacios de Software Libre pueden y suelen
    ser expulsivos es mediante actitudes que no contemplan la diversidad
    de saberes e interlocutor\*s. So pretexto de incluir tecnicismos,
    muchas compañeras quedan al margen de lo que está sucediendo, muchas
    veces, sin que nadie tenga la mínima delicadeza de verificar que
    todas estén siguiendo la conversación. Recomendamos fervientemente
    estar atentas a estas dinámicas para poder evitarlas y/o
    revertirlas.
-   La otra cara de la situación anterior es el famoso \"mansplaining\":
    un tipo poniéndose en el lugar de la autoridad del saber para
    (sobre-) explicar todo a la otra, de manera paternalista y sin tener
    en cuenta lo que la otra quiere o no escuchar, decir, lo que sabe o
    hace, etc.
-   Creemos que no hace falta ser \"una voz autorizada\" para opinar y
    participar. La cultura libre se comparte entre tod\*s.
-   \"Compartir es bueno\" vs. \"Google is your friend\". La
    meritocracia y ciertos códigos tradicionales de ciertas
    ciber-comunidades suelen operar de manera contraria a la propuesta
    de la cultura libre de compartir. Apoyamos la cultura piratil que
    suma ++ piratas a los barcos. Creemos que la cultura es para tod\*s
    y desafiamos las prácticas elitistas.
-   No damos por sentado que la persona con la que estamos interactuando
    comparte gustos, creencias, pertenencias de clase, sexualidad, etc.
    Podemos ser violentas si hacemos una lectura equivocada de la otra.
    Recomendamos siempre preguntar de manera respetuosa y evitar
    comentarios o chistes que puedan herir a las otras.
-   Usamos lenguaje amable e inclusivo y mostramos conductas amables e
    inclusivas.
-   Respetamos los diferentes puntos de vista, experiencias, creencias,
    etc., y lo tenemos en cuenta cuando estamos en grupo para verlo
    reflejado en nuestras actitudes.
-   Aceptamos las críticas. En especial las constructivas ;)
-   Nos enfocamos en lo que es mejor para la comunidad, sin por ello
    perder de vista la calidez, el respeto y la diversidad entre cada
    una de nosotras.
-   Mostramos empatía con las otras. Queremos comunicarnos y compartir.
-   Respetamoslos límites que establecen otras personas (espacio
    personal, contacto fisico, ganas de interactuar, no querer dar datos
    de contacto o ser fotografiadas, etc.)
-   ¡Queremos y (creemos) en sumar piratas!

Consentimiento para documentar o compartir en medios
----------------------------------------------------

-   Si vas a filmar o sacar fotos, comentalo en la sala, para que las
    personas estén al tanto y puedan dar o no su consentimiento.

<!-- -->

-   Si hay menores, consultalo con su familia responsable.
-   Implementamos El Semáforo para identificar quiénes quieren o no ser
    fotografiadas y bajo qué condiciones. Tenelo en cuenta y pedí las
    referencias, tanto si vas a tomar registro como

Nuestro compromiso contra el acoso
----------------------------------

En el interés de fomentar una comunidad abierta, diversa, y acogedora,
nosotras como contribuyentes y administradoras nos comprometemos a hacer
de la participación en nuestro proyecto y nuestra comunidad una
experiencia libre de acoso para tod\*s, independientemente de la edad,
diversidad corporal, capacidades, neuro-diversidad, etnia, identidad y
expresión de género, nivel de experiencia, nacionalidad, apariencia
física, raza, religión, identidad u orientación sexual.

Ejemplos de comportamiento inaceptable por parte de participantes:
------------------------------------------------------------------

-   Comentarios ofensivos relacionados con el/los género/s, la identidad
    y expresión de género, la orientación sexual, las capacidades, las
    enfermedades mentales, la neuro(a)tipicalidad, la apariencia física,
    el tamaño corporal, la raza o la religión.
-   Comentarios indeseados relacionados con las elecciones y las
    prácticas de estilo de vida de una persona, incluidas, entre otras,
    las relacionadas con alimentos, salud, crianza de los hijos, drogas
    y empleo.
-   Comentarios insultantes o despectivos (\\\*trolling\\\*) y ataques
    personales o políticos.
-   Dar por sentado el género de las demás personas. En caso de duda,
    preguntá educadamente por los pronombres. No uses nombres con los
    que las personas no se identifican, usá el nombre, nickname o apodo
    que hayan elegido (¿Realmente necesitas el nombre y el número de
    DNI, datos biométricos, carta natal, etc.?).
-   Comentarios, imágenes o comportamientos sexuales innecesarios o
    fuera de lugar en espacios en los que no son apropiados.
-   Contacto físico sin consentimiento o reiterado tras un pedido de
    cese.
-   Amenazas contra otras personas.
-   Incitación a la violencia contra otra persona, que también incluye
    alentar a una persona a suicidarse o autolesionarse.
-   Intimidación deliberada.
-   Acechar (stalkear) o perseguir.
-   Acosar fotografiando o grabando sin consentimiento, incluido también
    subir información personal a internet sobre alguien para acosarla.
-   Interrumpir constantemente en una charla.
-   Hacer comentarios sexuales indeseados.
-   Patrones de contacto social inapropiados, como por ejemplo
    pedir/suponer niveles de intimidad inapropiados con las demás.
-   Seguir tratando de entablar conversación con una persona cuando se
    te pidió que no lo hagas.
-   Divulgar deliberadamente cualquier aspecto de la identidad de una
    persona sin su consentimiento, excepto que sea necesario para
    proteger a otras personas de abuso intencional.
-   Hacer pública una conversación privada de cualquier tipo.
-   Otros tipos de conducta que pudieran considerarse inapropiadas en un
    entorno de camaradería.
-   Reiteración de actitudes que las participantes señalen como
    ofensivas o violatorias de este código.

### Consecuencias

-   Se espera que la persona a la que se la haya pedido que cese un
    comportamiento que infringe este código acate el pedido de forma
    inmediata, incluso si no está de acuerdo con este.
-   Las administradoras pueden tomar cualquier acción que juzguen
    necesaria y adecuada, incluido expulsar a la persona de la actividad
    sin advertencia. Esta decisión la toman las administradoras en
    consenso y se reserva para casos extremos que comprometan la
    continuidad de la actividad o bien la posibilidad de permanencia en
    ella de otras participantes sin sentirse agraviadas o amenazadas.
-   Las administradoras se reservan el derecho a prohibir la asistencia
    a cualquier actividad futura.

Como mencionamos antes, este código está en permanente mutación
colectiva. El objetivo principal es generar un ambiente inclusivo y no
expulsivo, un ambiente transparente y abierto en el que no haya
escalones faltantes (\"el escalón que falta en la escalera y todo el
mundo sabe y avisa pero nadie se quiere hacer cargo\"). Es importante
que se adapte a las actividades y se nutra de las constribuciones de las
asistentes. Recibir tus comentarios y aportes nos ayudará a cumplir con
su objetivo principal.

Sigamos en contacto! contacto\@partidopirata.com.ar

Si pasaste por alguna situación que quieras compartir -te hayas animado
o no a decirlo en el momento-, podés ponerte en contacto con nosotras.

Con respecto a quejas o avisos acerca de situaciones de violencia,
acoso, abuso o repetición de conductas que se advirtieron como
intolerables, tomamos la responsabilidad de tenerlas en cuenta y
trabajar en ellas para que el resultado sea el favorable al espíritu de
colectiva que elegimos y describimos aquí. Si bien consideramos que las
prácticas punitivistas no van con nosotras, nuestra decisión explícita
es *escuchar a la persona que se manifiesta como violentada o víctima y
acompañarla*.
