---
title: Translations:Carta Orgánica/27/en
layout: post
categories: []
---

In the case of unreachability of the sharing platform holder, the
platform would be considered a \"tacit\" pirate and its diffusion and
protection will become part of the Party\'s objectives.
