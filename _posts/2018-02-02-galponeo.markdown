---
title: Galponeo
layout: post
categories:
- Glosario
- Organización
---

[Categoría:Glosario](Categoría:Glosario "wikilink")
[Categoría:Organización](Categoría:Organización "wikilink")

(Recuperado y adaptado de otra wiki)

Galponeo, galponear, es el acto de discutir detalles nimios con el fin
(premeditado o inconciente) de retrasar las tareas. Es uno de los
problemas principales de la [Adhocracia](Adhocracia "wikilink"). Se lo
reconoce inmediatamente porque el locutor añade complejidad innecesaria
a una tarea simple o usando un nosotros inclusivo pero indeterminado,
dando lugar a que la tarea no sea asignada a nadie, usualmente con las
alocuciones \"tenemos que\...\", \"habría que\...\", etc.

Origen
------

Viene del inglés *bikeshedding*.

Historia
--------

El siguiente texto fue usado originalmente para explicar y sancionar
este comportamiento:

### ¿Por qué debería importarme el color del bicicletero?

> La respuesta muy muy corta es que no debería. La respuesta más o menos
> larga es que sólo porque sos capaz de construir un bicicletero no
> significa que deberías impedir a otros construir uno, sólo porque no
> te gusta el color con que planean pintarlo. Es una metáfora que indica
> que no necesitás argumentar sobre cada pequeña característica sólo
> porque sabés lo suficiente para hacerlo. Algunas personas han
> comentado que la cantidad de ruido generado por un cambio es
> inversamente proporcional a la complejidad del cambio.\
> \
> Este fenómeno, también llamado Ley de la Trivialidad de Parkinson,
> sugiere que la mayoría de la gente opina, discute y objeta demasiado
> sobre cosas triviales que se sienten capacitados de hacer (como un
> bicicletero) y no sobre cosas complejas sobre las que posiblemente no
> comprendan todas sus implicaciones (como una planta de energía
> atómica, en el ejemplo original).\
> \
> Esto hace que llegar a consenso sea difícil o imposible. Acá creemos
> que actuar adhocráticamente respecto a cuestiones triviales y
> reversibles disminuye la dificultad. \-- El color del bicicletero

Sin embargo, el \.... se dió cuenta que hacía falta encontrar un término
para nombrar un comportamiento dañino para la
[adhocracia](adhocracia "wikilink"). Los gringos tienen *bikeshedding*,
pero la traducción literal \"bicicletero\" ya tiene otro uso (aunque
similar) en el castellano rioplatense. En agosto de 2012, el concilio
del \... se decidiría por \"galponeo\".

Uso
---

`   Los ejemplos de uso de este artículo fueron galponeados.`

Cómo resolverlo
---------------

Pedile amablemente a la galponeadora que se encargue de lo que está
proponiendo :)

Pueden pasar dos cosas:

-   Se da cuenta que está galponeando y se pone a hacerlo
-   No hace nada

Las dos cosas están bien.

Conceptos relacionados
----------------------

-   [Lombardización](http://uqbar-wiki.org/index.php?title=Lombardizaci%C3%B3n):
    \"Dícese de aquella actitud donde una propuesta viene siempre
    acompañada de un compromiso de acción.\"
