---
title: Translations:Carta Orgánica/73/es
layout: post
categories: []
---

La Asamblea General deberá informar en un plazo no mayor a siete (7)
días y a modo de resumen histórico público, una elaboración por escrito
de lo acontecido, participantes, decisiones tomadas y porcentuales así
como un registro de la actividad por medios digitales.
