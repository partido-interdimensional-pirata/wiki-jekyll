---
title: Translations:Carta Orgánica/45/es
layout: post
categories: []
---

Todas las decisiones del Partido, en cualquiera de sus instancias de
decisión, se harán por Consenso. En caso existir Disenso Relevante en
proporción definida por reglamento, se tomarán decisiones por Acuerdo.
