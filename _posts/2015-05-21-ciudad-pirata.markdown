---
title: Ciudad Pirata
layout: post
categories: []
---

Objetivos
---------

-   Llevar las ideas piratas a las facultades de exactas y diseño de la
    UBA.
-   Articular con los centros de estudiantes, las cátedras y Velatropa.

Actas
-----

-   [25 de Marzo de
    2015](http://wiki.partidopirata.com.ar/Actas:2015-03-25)
