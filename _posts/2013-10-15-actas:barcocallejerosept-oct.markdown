---
title: Actas:BarcoCallejeroSept/Oct
layout: post
categories: []
---

Nos arrancamos a juntar los viernes a las 19hs en la Comuna 3. Estos son
los temas tratados (aprox) en cada reunion.

Los gastos los bancamos en modo vaquita cuando se necesita para algo.
Gutenberg se copa con los volantes y calcos.

0 - 6/9/13
----------

aza, charly, gala

-   Crew, armar el grupo
-   Carta Organica, darle difusion y uso
-   Coyuntura, nos chupa un huevo
-   Irc, mudarnos a <https://webchat.pirateirc.net/?channels=ppar> que
    ofusca la ip y es pirata!
-   Gastos \$12 en 120 volantes

1 - 13/9/13 :O
--------------

Se suman tom, gutenberg y jack.

-   Corregir volantes, ver niveles de lectura. Calcos.
-   Necesidad de barco de propaganda.
-   Salir a la calle.
-   facultades
-   Marcha 16

2 - 20/9/13
-----------

-   Facebook?
-   Joaquin V Gonzales / facultades
-   Sumar piratas, antes de salir a buscar gente afuera

( ALIENS!!! )

3 - 4/10/13
-----------

Se suman fran, mauri y martin

Volanteada en el Joaquin V Gonzales.

-   Crowdfunding pirata
-   Banderas / Palos
-   Revista Barcelona 22 Oct 8:30
-   Generar nuestro propio contenido/noticias AKA autobombo
-   Picada bucanera - Delirio en general
-   Monsatan!!!

Bandera \$150

4 - 11/10/13
------------

-   Mañana agite Monsatanico!
-   Propaganda
-   Dineros How To

Fuimos al encuentro, plantamos bandera, volanteamos, charlamos por ahi.

(Mas aventuras nos esperan\...)
