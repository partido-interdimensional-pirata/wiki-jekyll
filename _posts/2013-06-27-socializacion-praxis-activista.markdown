---
title: Socializacion praxis activista
layout: post
categories: []
---

En una primera fase se partirá desde el contexto del arte argentino,
donde se buscará plasmar a modo de contracartografía el mapeo de nodos
de desobediencias sexuales artísticos y las interrelaciones que emergen
en sus sinergías.

Destaca proyectos como CARA (http://proyectocara.com.ar/) que buscan
visibilizar proyectos en curso.

ICONOCLASISTAS

Rescatamos el concepto de investigación militante, en la línea del
Colectivo Situaciones
(http://www.nodo50.org/colectivosituaciones/nuestros\_propositos.htm) La
investigación militante no trabaja a partir de un conjunto de saberes
propios sobre el mundo, ni sobre cómo debieran ser las cosas. Muy por el
contrario, la única y dificultosa condición del militante investigador
es la de permanecer fiel a su "no saber". En este sentido, es una
auténtica antipedagogía(como quería Joseph Jacotot).

 La investigación militante es también el arte de establecer
composiciones que potencien las búsquedas y los elementos de
sociabilidad alternativa.

trabajar en colectivos autónomos que no obedezcan a reglas impuestas por
la academia l cual implica establecer un vínculo positivo con los
saberes subalternos, dispersos y ocultos, y producir un cuerpo
de saberes prácticos de contrapoder. Todo lo contrario de utilizar las
experiencias como campo de confirmación de las hipótesis de laboratorio.

Como es sabido, la investigación académica está sometida a todo un
conjunto de dispositivos alienantes que separan al investigador
del sentido mismo de su actividad: se debe acomodar el trabajo a
determinadas reglas, temas y conclusiones. El financiamiento, las
tutorías, los requerimientos de lenguaje, el papeleo burocrático, los
congresos vacíos y el protocolo, constituyen las condiciones en que se
desarrolla la práctica de la investigación oficial. La investigación
militante se aleja de esos ámbitos (claro que sin oponerse a ellos ni
desconocerlos), e intenta trabajar bajo condiciones alternativas,
creadas por el propio colectivo y por los lazos de contrapoder en los
que se inscribe, procurando una eficacia propia en la producción de
saberes útiles a las luchas. La investigación militante modifica
su posición: trata de generar una capacidad de las luchas de leerse a sí
mismas y, por tanto, de retomar y difundir los avances y las
producciones de otras experiencias. A diferencia del militante político,
para quien la política pasa siempre por la política, el militante
investigador es un personaje hecho de interrogaciones, no saturado de
sentidos ideológicos y de modelos sobre el mundo. La investigación
militante no es tampoco una práctica de "intelectuales comprometidos" o
de un conjunto de "asesores" de los movimientos sociales. El objetivo no
es politizar ni intelectualizar las experiencias. No se trata de lograr
que éstas den un salto, para pasar de lo social a la "política seria".
La pista de la multiplicidad es opuesta a estas imágenes del salto y
la seriedad: no se trata de enseñar ni de difundir textos claves, sino
de buscar en las prácticas las pistas emergentes de la nueva
sociabilidad. Separado de las prácticas, el lenguaje de la investigación
militante se reduce a la difusión de una jerga, una moda o una nueva
ideología pseudo universitaria desprovista de anclaje situacional.
Materialmente la investigación militante se desarrolla bajo las formas
del taller y la lectura colectiva, de la producción de las condiciones
para el pensar y la difusión de textos productivos, en la generación de
circuitos fundados en experiencias concretas de lucha, con el estudio y
entre núcleos de militantes investigadores. En el presente artículo
intentaremos presentar algunas elaboraciones surgidas a partir de
nuestra labor realizada en La Argentina entre los anos 2000 y 2003.
<http://eipcp.net/transversal/0406/colectivosituaciones/es>

Y en este sentido, el libro que parte de un diálogo con Universidad
Transhumante\...

Fue en ese contexto de preocupaciones que nos acercamos a la Universidad
Trashumante --UT-- con la propuesta de un encuentro para abrir una
discusión (es decir, una elaboración) sobre los procedimientos
involucrados en el armado de una red de educación popular.

Es decir: no se trataría de una entrevista, sino de un intercambio
originado en nuestra insistencia por conocer su experiencia.

Párrafos enteros dedicados a cronicar detalles minúsculos, dándose
tiempo para recorrer lentamente nombres y fechas que sólo adquieren su
valor por esa estrategia temporalizante que economiza en nociones
abstractas y se alarga hasta la crispación en impresiones en las que
nosotros, en otras ocasiones, no hubiéramos reparado. El énfasis
anecdotista, por su parte, llevaba a subrayar un gesto o una frase hasta
otorgarle un aspecto grandilocuente. Un relato así aburre, o cautiva si
es capaz de producir una nueva disposición de escucha.

¿A qué se debía semejante minuciosidad que parecía no querer más que
pulir al infinito esa suerte de inventario de situaciones?. Una primera
impresión --primera pero duradera-- nos indicaba que se trataba de un
monumental esfuerzo de visibilización de lo que cotidianamente queda al
margen, sin ser visto ni contado. Cierto, pero insuficiente; parecía
haber algo más. La catalogación de anécdotas no refería de modo simple a
una voluntad de mostrar lo periférico sin que, a la vez, no diera curso
a una operación más sutil, a una compilación de criterios en
apariencia----y, suponemos, con cierta intención---- arbitrarios; al
punto que, llevados al extremo, constituían algo así como una antología
disparatada , evidenciando que toda antología es en sí misma un
disparate, y por tanto, que frente a ella no vale tanto la corrección de
estilo como el interés que pueda suscitarnos.El anecdotario --una
especie de yacimiento ambulante que carga cada trashumante-- no tiene
pretensiones exclusivamente ilustrativas. Su sentido no es una moraleja
que brota con espontánea pedagogía, casi sin esfuerzo, de la coherencia
del relato; sino que más bien trabaja presentando una pregunta cuya
tarea es colocarnos frente a otra: ¿qué dignidad tiene este episodio
para ser contado?, ¿de dónde surge el criterio de validez de su
narración? Sólo indagando en la construcción de estos puntos de vista,
de estos modos de percepción ajenos a la síntesis, al efecto conclusivo,
a la pretensión analítica, se logra captar el flujo de imágenes y de
pensamiento puestos en juego.

ESP

`   El gran pollo de la Alameda`

<http://www.elgranpollodelaalameda.net/>

Proyecto editorial colectivo que, entre otras cosas, documenta y analiza
las luchas y resistencias que se han desarrollado en los barrios de San
Luis y la Alameda de Hércules contra el proceso de transformación
urbanística y de recambio poblacional que se está produciendo en la zona
norte del casco histórico de Sevilla desde mediados de la década de los
noventa.

`   Arquitecturas Colectivas`

mapeos, manuales Arquitecturas Colectivas es una red de personas y
colectivos interesados en la construcción participativa del entorno
urbano. Esta red proporciona un marco instrumental para la colaboración
en diferentes tipos de proyectos e iniciativas. Este se manifiesta
principalmente en tres formas: mediante sistemas de comunicación
(on-line, audiovisual o impresa), mediante proyectos colaborativos
(diseño y construcción de prototipos, cooperación internacional, apoyo a
procesos participativos) y mediante encuentros presenciales (intercambio
de experiencias, talleres, testeo de prototipos).

En su web destacan un apartado de "Herramientas" que dice

Una de las utilidades más importantes de este sitio web es proporcionar
espacios y herramientas para el trabajo en colaboración. Para ello nos
organizamos en grupos de trabajo. Cualquier usuario registrado puede
proponer la creación de un nuevo grupo. Los grupos autogestionan su
funcionamiento, sus componentes tienen control sobre la incorporación de
nuevos usuarios, creación y gestión de contenidos, privacidad de la
información, gestión de galerías de archivos, etcétera.

Te da la posiblidad de crear grupos de trabajo desde la web para

Grupo de investigación sobre mapeo de colectivos, procesos, recursos y
conflictos.    

Grupo de investigación sobre aspectos jurídicos y producción de guías
relacionadas.    

Grupo de investigación sobre procesos colectivos relacionados con la
vivienda. 

necesidad de manuales\_
<http://arquitecturascolectivas.net/noticias/grupo-juridico-guias-y-retornos>
