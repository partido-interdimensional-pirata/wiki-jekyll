---
title: Operación Leviatán Electrónico
layout: post
categories: []
---

Artículo original por el Partido Pirata de Canadá [Operation Electronic
Leviathan](http://wiki.pirateparty.ca/index.php/Operation_Electronic_Leviathan)

What Operation Electronic Leviathan is
--------------------------------------

As a response to warrantless surveillance Bill C-30, Bill C-11, SOPA,
and all similar future legislation the Pirate Party of Canada is
initiating Operation Electronic Leviathan. This operation aims to use
mass education and promotion of encryption, privacy tools, and copyleft
licensing as a means of promoting digital sovereignty. This is a free
(as in freedom) operation. Copy it, make it your own, and share it with
anyone.

**By using encryption you can make sure that whatever you have encrypted
(e.g. files, chats, etc.) cannot be read by the Harper Government,
Facebook, or anyone else.**

The concept of [Electronic
Leviathan](http://en.wikipedia.org/wiki/Electronic_leviathan) was
developed on the basis of research into a product of electronic
government, ICP-Brasil. During this research the Brazilian government
sought to somehow control the use of the internet as a doorway to the
sort of anarchy that prevails in the world of international relations.
ICP-Brasil and the Electronic Leviathan are good examples of not only
the threat the Internet is under from various states, but that the
Internet is a digital actualization of complete freedom and is sovereign
from governments.

Programs you can use to chat and share files anonymously and privately with encryption
--------------------------------------------------------------------------------------

\- Use [I2P](http://i2p2.de) to torrent files, chat on IRC, browse
internal websites (Eepsites), or anything else compatible with use
inside the I2P network layer (wide compatibility), all anonymously.

\- Use [Tor](http://torproject.org) to browse the Internet anonymously.

\- Use the [OTR](http://www.cypherpunks.ca/otr/) plugin for
[Pidgin](http://www.pidgin.im/) to encrypt chat on Facebook, MSN, IRC,
AIM, and more.

\- Use [HTTPS Everywhere](http://eff.org/https-everywhere) to encrypt
Internet traffic whenever HTTPS is supported (Facebook, Google,
Wikipedia, etc.)

\- Use [Truecrypt](http://truecrypt.org) to encrypt files and partitions
on M\$ Windows and [dm-crypt](http://www.saout.de/misc/dm-crypt/) on
GNU/Linux.

\- Use [GPG](http://www.gnupg.org/) to encrypt any text or files, or add
an additional layer of encryption to any other tool here.

Guides
------

\- [Setting up OTR and Pidgin](Setting_up_OTR_and_Pidgin "wikilink")

\- [Installing and using I2P](Installing_and_using_I2P "wikilink")

\- [Installing and using HTTPS
Everywhere](Installing_and_using_HTTPS_Everywhere "wikilink")

\- [Using Truecrypt and
dm-crypt](Using_Truecrypt_and_dm-crypt "wikilink")

\- [Cell phone privacy guide
(Android)](Cell_phone_privacy_guide_(Android) "wikilink")

How you can help:
-----------------

1\. Repost images made as a part of this operation on social media sites
you use. Sharing is our most powerful weapon, so spread the word!

2\. Create images and information that can be distributed to teach others
how to guarantee their privacy and promote digital sovereignty.

Ideas for information/imagery
=============================

\- Images with [install files for I2P and Tor (and other tools)
compressed and hidden inside
them](http://lifehacker.com/207905/hide-files-in-jpeg-images), providing
md5sums.

\- Setting up full disk encryption with dmcrypt (GNU/Linux) and
Truecrypt (Window\$).

\- Accessing various services (e.g. torrent trackers, IRC, etc.) on I2P

\- Encrypting and decrypting with GPG

\- Setting up a VPN connection, using either our VPN or the Swedish
PP\'s iPredator.

\- Guide for HTTPS Everywhere.

\- Imagery specifically promoting this operation.

\- Imagery promoting Pirate Linux

☠ See also: Pirate Linux - Your one-stop-shop for privacy and censorship circumvention ☠
========================================================================================

[Pirate Linux](http://piratelinux.org/) is a project started by a few
members of the Pirate Party of Canada. The main goals are:

1\. Educate the public about Pirate Party issues, and learn from their
feedback.

2\. Create a unified platform that Pirate Party members can use for
further developments as well as communication & meetings.

3\. Create a system that Pirate Party members will want to use for
themselves.

4\. Help people have easy access to a transparent system that allows them
to exercise a strong level of free speech and privacy.

5\. Give people access to useful open source content such as books,
videos, software.

6\. Remove peoples' dependence on copyrighted "content".

7\. Teach people how the technology (used in OS) works.

Discussion(submit images or information you\'ve made here):
===========================================================

[Reddit](http://www.reddit.com/r/OpElectronicLeviathan/)

[PPCA Forum](http://www.pirateparty.ca/forum/index.php?board=83.0)

[PPCA Facebook](http://www.facebook.com/piratepartyca)

[German Pirate Party OpEL](http://boomel.lhwclan.de/?p=1485)

Gallery - Images that teach about encryption and copyleft:
==========================================================
