---
title: Protege tus Datos
layout: post
categories: []
---

Borrar archivos permanentemente
-------------------------------

Cuando borramos un archivo usualmente este no se borra, el sistema
operativo simplemente lo \"olvida\" pero los datos siguen estando
grabados en el disco rígido. De seguir usando el disco eventualmente se
sobre-escribirían pero aún sobrescrito algunas veces se puede recuperar
con técnicas forenses.

Si queremos proteger la privacidad de nuestros datos debemos
sobre-escribir varias veces los archivos a borrar, para esto se utiliza
\'shred\', una herramienta que viene pre-instalada en los sistemas
GNU/Linux.

**Borrar un archivo**

Con este comando \'shred\' sobre-escribe el archivo con datos random y
lo re-nombra para que tampoco se filtre el nombre del archivo.

`shred -zuv archivo`

**Borrar todos los archivos en una carpeta**

Lamentablemente shred no soporta borrar toda una carpeta, para esto
tenemos que usar bash.

`find path_a_carpeta -type f -print0 | xargs -0 shred -zuv`

Crear una Partición Cifrada
---------------------------

Cifrar el Home
--------------

Cifrar el Disco
---------------
