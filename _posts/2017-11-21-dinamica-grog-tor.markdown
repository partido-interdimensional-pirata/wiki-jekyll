---
title: Dinamica Grog&Tor
layout: post
categories:
- grog&tor
---

[Categoría:grog&tor](Categoría:grog&tor "wikilink") Introducción:

Diferenciar (in)seguridad digital de autodefensa digital. La inseguridad
es el discurso de la derecha, salis a la calle y te pasa algo, salís a
internet y te roban los datos, etc. hablamos de autodefensa digital
porque la idea es que los cuidados son colectivos, que sabemos que
estamos bajo ataque pero que en conjunto nos podemos
defender/cuidar/proteger.

No vamos a contar historias de terror, por favor no las traigamos a la
discusion porque paralizan. Historia de terror es \"lei en este blog que
todos los android estan rotos y vamos a morir\", etc.

Si alguien esta pasando por una historia de terror real, no se exponga
en el taller, se puede acerca a hablarnos en privado.

Tratamos de no marearnos con terminos tecnicos, si vamos a hacer
preguntas super especificas tenemos otros canales \#desistemas para
hacerlas. Hacemos esto porque nos encanta hablar de tecnologia y sabemos
que podemos estar cinco horas hablando de cosas tecnicas.

Repartir las reglas en papel.

Nos vamos a dividir en mesas porque queremos que nadie se vaya sin
practicar algo y que sea realmente un taller donde compartimos lo que
sabemos mas que una bajada de linea.

No sacamos ni contamos informacion personal de mas.

Opciones para presentar el taller:

` * Tenemos estas tres mesas pero queremos saber cuales son las inquietudes que las atrajeron a este taller, a ver si coinciden con lo que tenemos pensado o tenemos que armar algo mas`\
` * Tenemos estos tres **temas** y queremos saber cuales son las inquietudes... luego de la ronda presentamos las mesas`

Tomar nota de las inquietudes.

Si una persona se suma mas tarde a una mesa, invitar a las que ya
estaban a explicarle y quedarse cerca, para no tener que volver a
explicar y que las demas practiquen enseñar.

Terminar con puesta en común / plenario

Ideas:

Armar un panfletito glosario para repartir con las reglas

pad : <https://pad.partidopirata.com.ar/p/grog_en_latribu>
