---
title: Translations:Carta Orgánica/139/es
layout: post
categories: []
---

El ejercicio contable durará el tiempo máximo definido para todos los
puestos elegidos salvo que se revoque el puesto por asamblea, o el
elegido decida renunciar o se vea inhabilitado de continuar en sus
funciones.
