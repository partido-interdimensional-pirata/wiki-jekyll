---
title: Translations:Carta Orgánica/59/en
layout: post
categories: []
---

This Mandate will end once the objective for which they were created is
fulfilled, or by resignation or assembly revocation.
