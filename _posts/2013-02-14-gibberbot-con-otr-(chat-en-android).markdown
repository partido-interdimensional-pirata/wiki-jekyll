---
title: Gibberbot con OTR (Chat en Android)
layout: post
categories: []
---

Introducción
------------

Esta guía tiene dos partes, por un lado la configuración de las cuentas
de Jabber (mensajería instantánea) del Partido Pirata en el cliente
libre Gibberbot para Android y por otro, la configuración y uso de
Off-The-Record (OTR), el complemento de conversación criptográfica de
Pidgin.

OTR asegura que las conversaciones no pueden ser interceptadas ni leídas
por terceros (el PPAr, el ISP, Google, la SIDE, el FBI, la CIA, etc.) y
que realmente hablamos con quien creemos que estamos hablando.

Instalación
-----------

[Gibberbot](https://guardianproject.info/apps/gibber/) es un cliente de
mensajería instántanea multicuenta para Jabber (GTalk, Facebook, etc)
disponible para Android.

Configuración de la cuenta
--------------------------

Al abrir Gibberbot por primera vez, veremos algo como esto:

![Pantalla de inicio](gibberbot_01.png "Pantalla de inicio")

Luego de seleccionar el idioma (inglés en este caso), continúa una
seguidilla de pantallas de bienvenida:

![](gibberbot_02.png "fig:gibberbot_02.png")
![](gibberbot_03.png "fig:gibberbot_03.png")
![](gibberbot_04.png "fig:gibberbot_04.png")
![](gibberbot_05.png "fig:gibberbot_05.png")

Listo con la bienvenida :) Ahora llegamos a la pantalla de configuración
de cuenta:

![](gibberbot_06.png "gibberbot_06.png")

Ponemos los datos requeridos\...

![](gibberbot_07.png "gibberbot_07.png")

Y no hay que olvidarse de ir a

Seguir las instrucciones. Para añadir la cuenta del PPAr, hay que elegir
[XMPP](https://es.wikipedia.org/wiki/XMPP) en tipo de cuenta y completar
con los datos propios. El campo *Recurso* es abierto y puede dejarse
vacío o completarse con la ubicación (en el caso de tener la misma
cuenta en varias computadoras) o con cualquier cosa.

No hay que tildar \"Crear esta nueva cuenta en el servidor\" si no se
tiene cuenta.

![Creación de cuenta del
PPAr](002_xmpp.png "Creación de cuenta del PPAr")

Al guardar los cambios, el gestor de cuentas muestra las cuentas activas
o inactivas.

![Gestor de cuentas con cuenta
habilitada](003_cuentas.png "Gestor de cuentas con cuenta habilitada")

Las cuentas del PPAr muestran todos los Piratas registrados :)

![Lista de amigos piratas
P)](004_piratas.png "Lista de amigos piratas P)")

Activación de OTR
-----------------

Luego de instalar el complemento, hay que habilitarlo.

![Menú de complementos](005_complementos.png "Menú de complementos")

![Habilitar OTR](006_otr.png "Habilitar OTR")

Iniciar una conversación privada
--------------------------------

![Ventana de
conversación](007_conversacion.png "Ventana de conversación")

![Iniciar la conversación privada desde el menú
OTR](008_iniciar_otr.png "Iniciar la conversación privada desde el menú OTR")

![La generación de la llave toma un tiempo y la ventana se bloquea. Para
acelerar el proceso hay que mover el puntero del mouse o
teclear.](009_generar_llave.png "La generación de la llave toma un tiempo y la ventana se bloquea. Para acelerar el proceso hay que mover el puntero del mouse o teclear.")

Antes de poder iniciar una conversación segura, hay que asegurarse que
la persona con la queremos hablar sea realmente ella. Para esto hay que
\"autenticar\" la cuenta. Al autenticar, se comprueba que la llave de la
otra cuenta está siendo utilizada por la persona con la queremos hablar
y viceversa. Luego de este paso, OTR no vuelve a pedir este paso, pero
sí hay que hacerlo con todos nuestros contactos.

![El proceso de autenticación es necesario para saber si estamos
hablando con quien creemos que vamos a
hablar.](010_autenticar.png "El proceso de autenticación es necesario para saber si estamos hablando con quien creemos que vamos a hablar.")

### Distintos modos de autenticación

OTR provee tres modos de comprobar que la persona con la que queremos
hablar es realmente la que pensamos. En general la explicación que da el
software es suficiente.

#### Pregunta y respuesta

Pregunta con respuesta compartida. Hay que tener cuidado porque no vamos
a leer la respuesta del compañero/a sino que Pidgin va a compararla con
la que ya dimos. Si nuestro compañero/a la escribe distinto la
autenticación va a fallar.

![](011_pregunta.png "011_pregunta.png")

#### Secreto compartido

En este paso hay que ponerse de acuerdo previamente en una
\"contraseña\".

![](012_secreto.png "012_secreto.png")

#### Comprobación manual

En esto paso cada participante debe tener la huella de la llave OTR del
otro, es decir la serie de letras y números que la identifica (en
realidad son [números
hexadecimales](https://es.wikipedia.org/wiki/Sistema_hexadecimal)).

![](013_huella.png "013_huella.png")

### Conversación privada!

Cuando la autenticación se completa, Pidgin avisará que la conversación
no está siendo grabada (es decir que no quedan registros!), además de
marcar \"Privado\" en verde en el menú.

![](015_autenticados.png "015_autenticados.png")

![](014_privado.png "014_privado.png")

[Category:Privacidad](Category:Privacidad "wikilink")
[Category:Software\_Libre](Category:Software_Libre "wikilink")
[Category:Guías](Category:Guías "wikilink")
