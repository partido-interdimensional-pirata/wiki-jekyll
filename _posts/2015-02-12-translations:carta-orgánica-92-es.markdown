---
title: Translations:Carta Orgánica/92/es
layout: post
categories: []
---

La Asamblea designará, si el mismo no hubiera sido ya designado por el
imputado, un Contramaestre que actuará en su defensa, velando por sus
derechos y obrando por su bienestar. La elección del mismo será
notificada fehacientemente y de manera inmediata al imputado.
