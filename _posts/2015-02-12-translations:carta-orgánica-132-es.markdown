---
title: Translations:Carta Orgánica/132/es
layout: post
categories: []
---

La Administración de los Fondos será llevada a cargo por el Órgano
Fiduciario del Partido Pirata, cuyos miembros serán elegidos en
Asamblea, siendo obligatoriamente Piratas Afiliados/as. La duración del
cargo cae bajo el Artículo 16º de este Documento. Este órgano llevará a
cabo la contabilidad, fiscalización, control, y demás acciones respecto
de la economía y los fondos del Partido, respondiendo directamente al
Órgano Contralor.
