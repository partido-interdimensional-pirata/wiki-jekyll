---
title: Borraodr manifiesto poetico
layout: post
categories: []
---

*propuesta \"poetica\"*

Proto Manifiesto
----------------

Contra todo intento de estigmatizacion de nuestro nombre, reivindicamos
a los Piratas, la Piratería y sus luchas.

Si en aquel mundo dominado por monopolios depredadores, por la
esclavitud, la autocracia y el racismo, los Piratas se dieron a si
mismos igualdad, libertad, autodeterminación y auténtica convivencia
multicultural, ignorando toda correcion política de su tiempo, entonces
es que reivindicamos su bandera, su legado y su nombre.

Porque nosotros también crecimos en la periferia, navegando los caóticos
mares marginales de la red y habitando remotas islas temporalmente
liberadas, y no conocemos otra forma de organizamos que no sea bajo un
principio de convivencia igualitaria: las Redes de Pares.

También resistimos los monopolios, sobre el conocimiento, la cultura, y
el patentamiento de la vida, y también resistimos a las metrópolis del
presente que depredan los recursos naturales en sus colonias: el planeta
entero.

Y por último también nos parecemos en algo más, es cierto, como todos
los Piratas y Filibusteros también asaltamos naves: para quebrar la
opresión de las falsas leyes de la escasez digital y sus falsos feudos
inmateriales, fraudulentos espejismos propagandisticos que nos
restringen el acceso a este nuevo mundo de la abundancia multicolor,
donde todos podemos crear, copiar, multiplicar y repartir la cultura y
el conocimiento.

Como Piratas que somos creemos en la libertad, en la diversidad, en la
auto-organizacion, en la horizontalidad, en sentirnos pares e iguales,
en democratizar la creacion, en la libertad de expresion y pensamiento,
y como nacimos compartiendo, creemos en compartir la mayor riqueza que
atesoramos como humanidad: la cultura, las ideas y el conocimiento, que
liberados del lastre analógico y anacrónico de la escasez, sólo queda
liberarlos del pesado lastre de la codicia.

Como Piratas que somos, no somos ingenuos, y sabemos que las mismas
armas digitales que nos liberan, las redes distribuidas, son las mismas
redes que centralizadas usarán para vigilarnos, controlarnos y
dirigirnos, por eso defendemos la privacidad, el anonimato y la
neutralidad de las redes, como herramientas de liberación que nos
permitan seguir navegando la red, y sostener nuestra autonomía,
capacidad de organizarnos y resistencia.

Como Piratas que somos aprendemos sobre la marcha, construímos sobre la
marcha, aprendemos de las luchas por la libertad del conocimiento que se
iniciaron en la prehistoria de Internet, aprendemos de la lucha por
proteger nuestros bienes comunes, los recursos naturales, los mares, los
vientos, las llanuras y las montañas.

Aprendemos que la libertad, la solidaridad y el compromiso colectivo son
el único camino sustentable para seguir existiendo como seres humanos en
este planeta.

------------------------------------------------------------------------

Algunas referencias
-------------------

(o de donde plagiamos copiamos y piratear estas palabras)

[oxcars - La avaricia Rompe el saco](http://oxcars09.exgae.net/)

:   \"Lo que nos estamos jugando no son simplemente los dividendos
    económicos, sino la propia concepción de la cultura y el derecho al
    acceso a la información (que nos ha costado unos cuantos cientos de
    años conseguir).\"

<!-- -->

:   \"La Cultura se da por la imitación y la copia. En la era digital y
    de la comunicación, lo digital son nuestros recuerdos y nuestra
    forma de comunicación, es la materia de la que está hecha nuestra
    memoria.\"

<!-- -->

:   \"La sociedad civil reclama el lucro cesante de todo el conocimiento
    que se esta reteniendo y sustrayendo al uso público en nombre de
    beneficios privados.\"

[Zona Temporalmente
Autónoma](http://lahaine.org/pensamiento/bey_taz.pdf) Hakim Bey

:   \"LOS PIRATAS Y CORSARIOS del siglo xviii crearon una «red de
    información» que envolvía el globo: primitiva y dedicada
    primordialmente a los negocios prohibidos, la red funcionaba
    admirablemente. Repartidas por ella había islas, remotos escondites
    donde los barcos podían ser aprovisionados y cargados con los frutos
    del pillaje para satisfacer toda clase de lujos y necesidades.
    Algunas de estas islas mantenían «comunidades intencionales»,
    completas mini- sociedades que vivían conscientemente fuera de la
    ley y mostraban determinación a mantenerse así, aunque fuera sólo
    por una corta -pero alegre- existencia.\"

[Sobre el corsario argentino Hipólito Buchardo o
Bouchard](http://partido-pirata.blogspot.com/2008/03/sobre-hiplito-buchardo-o-bouchard.html)

[Piratas, Corsarios, Filibiusteros](http://www.corsarios.net/)
(diferencias entre piratas, corsarios, bucaneros, filibusteros)

[Filibiusteros Cofradia Hermanos de la
Costa](http://www.corsarios.net/filibusteros/fi0quienes-filibusteros.html)

:   \"Con unos indicios en 1620, se potencia en la isla Tortuga. Tenian
    unas reglas y un codigo del honor simples, pero estrictos. No
    importaba el origen de la persona, ni su religión, ni su color.
    Convivian gentes de todos los paises, todas las religiones, y todos
    los colores.\"

<!-- -->

:   \"Libertad para cada uno. Cada uno podia hacer lo que quisiera con
    su vida, la cofradia se mantenia al margen. La vida privada era
    respetada, y uno podia abandonar la pirateria si quería.\"

<!-- -->

:   \"Sin propiedad.Dividian el botín entre todos, no teniendo
    individualmente, mas que las cosas personales básicas y su parte del
    botín.. Los barcos y la tierra eran propiedad de todos. Se
    organizaban expediciones continuamente. Nadie podia tener tierras en
    la isla. No habia propiedad privada.\"

[Utopía Pirata](http://es.wikipedia.org/wiki/Utop%C3%ADa_pirata)

[Libertalia](http://es.wikipedia.org/wiki/Libertalia) isla mitologica
liberada por los piratas

[Manifiesto
Ciberpunk](http://project.cyberpunk.ru/idb/manifesto_es.html)

:   \"6/ Observamos la Red, y la Red esta creciendo y haciendose más
    amplia. 7/ Pronto todo en este mundo será absorvido por la Red:
    desde los sistemas militares hasta el PC de casa. 8/ Pero la Red es
    la casa de la Anarquía. 9/ No puede ser controlada y en eso radica
    su poder. 10/ Cada hombre será independiente en la Red. 11/ Toda la
    información estará aquí, cerrada en el abismo de ceros y unos. 12/
    El que controla la Red, controla la información. 13/ Vivimos en una
    mezcla del pasado y el presente. 14/ El mal proviene del hombre y el
    bien de la tecnología. 15/ La Red controlará al pequeño individuo y
    nosotros controlaremos la Red. 16/ Pero, si tu no controlas, serás
    controlado. 17/ La información es el PODER!\"
