---
title: ShareLikeAPirate.en
layout: post
categories:
- Propuestas
- Propuestas
---

Blocking of The Pirate Bay in Argentina: campaign Share Like A Pirate
=====================================================================

ShareLikeAPirate is a campaign whose main objective is to spread news
and increase exposure of the Legal Actions presented by the Pirate Party
from Argentina for the blocking of TPB in the country. The idea is to
publish free content (personal pictures, music, videos, books, papers,
etc) mainly by people affected by the sentence to show that it is
censorship against the freedom of speech.

ShareLikeAPirate wants to increase exposure according to the following
strategy:

1.  Creating free content collectively under Creative Commons license
2.  Sharing free content through The Pirate Bay

\[Create\] Collective creation of free content
----------------------------------------------

1.  Organize online events where artists, programmers, hackers, makers
    or whoever wants generate free content (music, painting,
    photography, software, literature, etc) under the Creative Commons
    license
2.  Share this content through The Pirate Bay adding the hashtag
    \#ShareLikeAPirate to join this campaign
3.  We will search for content having the hashtag \#ShareLikeAPirate and
    we will add it to the legal appeal as part of our legal action to
    show how many people is affected by the blocking of TPB.

\[Share\] Sharing free content through The Pirate Bay
-----------------------------------------------------

The censorship to TPB do not only block us from publishing our free
content to The Pirate Bay but it also block us from accessing content
created by others. We will add to the legal action all content that it
is not accessible from TPB due the blocking.

Other collectives like netlabels, journals, magazines and independent
artists already created pirate content with Creative Commons licence. We
encourage the community to share content through The Pirate Bay in order
to adhere to our fight against this violation of fundamental rights.

Tasks
-----

-   Make creative flyers
-   Create guides to share content through The Pirate Bay
-   Contact netlabels, collectives and particulars related to the free
    culture and ask for support
-   Organize online events to publish content collectively
-   Contact TPB crew to put a flyer in promobay

[Categoría:Propuestas](Categoría:Propuestas "wikilink")

[Categoría:Propuestas](Categoría:Propuestas "wikilink")
