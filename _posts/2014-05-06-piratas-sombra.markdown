---
title: Piratas Sombra
layout: post
categories:
- Organización
---

[Categoría:Organización](Categoría:Organización "wikilink")

[Sombra pirata](Ayuda:Sombra_pirata "wikilink") es la pirata que se
compromete a darle la bienvenida a las nuevas piratas, responderles
dudas y guiarlas en general.

Sombras disponibles
-------------------

-   [Pirata:Seykron](Pirata:Seykron "wikilink")
-   [Pirata:Aza](Pirata:Aza "wikilink")
-   [Pirata:Fauno](Pirata:Fauno "wikilink")
