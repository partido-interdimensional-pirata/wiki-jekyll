---
title: Glosario de traducciones
layout: post
categories:
- Traducción
---

[Categoría:Traducción](Categoría:Traducción "wikilink")

Glosario de traducciones, si encontramos un término específico (técnico,
político, etc.) lo ponemos acá en idioma original con la traducción
consensuada y una explicación de lo que significa.
