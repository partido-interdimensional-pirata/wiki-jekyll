---
title: Translations:Carta Orgánica/46/es
layout: post
categories: []
---

Habrá Consenso sobre una propuesta cuando esta no cuente con la
desaprobación de un porcentaje definido por reglamento no superior al
10% de los piratas afiliados participantes, es decir exista Disenso
Relevante.
