---
title: Translations:Carta Orgánica/6/es
layout: post
categories: []
---

El Partido Pirata funciona redefiniéndose por cada información
exteriorizada entre sus miembros y las nuevas redes entre terceros. La
estructura no se da ex ante sino ex post, al momento de la reflexión y
adopción como propio de lo hecho por sus miembros en asambleas.
