---
title: BloqueoThePirateBay
layout: post
categories: []
---

¿Qué pasó?
----------

La historia arranca el lunes 30 de Junio del 2014 cuando el juez Gastón
Matías Polo Olivera [falló a favor de
CAPIF](http://www.scribd.com/doc/232015119/CAPIF-CAMARA-ARG-DE-PRODUCTORES-DE-FONOGRAMAS-Y-OTROS-c-THE-PIRATE-BAY-s-MEDIDAS-PRECAUTORIAS)
avalando el bloqueo completo a [The Pirate
Bay](https://tpb.partidopirata.com.ar/) en Argentina. El fallo de
primera instancia es una medida autosatisfactiva, es decir, un recurso
que no tiene caducidad y no requiere que haya juicio ya que asume que se
está perjudicando los derechos fundamentales de alguien.

Algunos puntos aberrantes que el juez Olivera (un experto que en teoría
cursó posgrado derecho de alta tecnología de la UCA) indicó en el fallo:

-   \"Descargar archivos devasta la cultura como se devasta el planeta
    con la contaminación\"
-   \"La descarga de archivos es como el robo de una panadería\"
-   \"Es como si alguien entrara a una librería y se llevara los libros
    sin pagar. Pero la gente tiene más escrúpulos con los objetos
    tangibles que con los intangibles.\"
-   \"Reconocer la protección de la garantía de la libertad de expresión
    al encumbrar al rango de información al régimen de intercambio de
    bytes P2P entre usuarios y asistencia del tracker para la violación
    de derechos de autor, es denigrar uno de los más grandes logros del
    hombre libre\"

El expediente se puede consultar en el [Poder Judicial de la
Nación](http://scw.pjn.gov.ar/scw/expediente.seam?cid=236468).

El fallo está lleno de errores conceptuales y no tiene sustento, incluso
viola varias leyes y tratados internacionales (Art. 19 y 29 de la
Declaración Universal de los Derechos Humanos; el Art. 13 del Pacto de
San José de Costa Rica; Art. 11 de la Convención Americana sobre
Derechos Humanos, entre otros)

Bitácora de las acciones contra CAPIF
-------------------------------------

### Apoyos

Varios grupos hacktivistas se encargaron de hacerles llegar nuestro
mensaje a CAPIF y otras empresas/instituciones relacionadas a través de
acciones contra sus sitios institucionales.

En general, toda la comunidad del cyberespacio nos ayudó muchísimo a
difundir y llegar a muchas personas. Esto es muy importante para hacer
presión y lograr que den a lugar las acciones judiciales.

Escribimos un artículo en el sitio: [Una medida desproporcionada e
inútil](http://partidopirata.com.ar/2014/07/01/una-medida-desproporcionada-e-inutil/)

El sitio TorrentFreak nos ayudó con una cobertura muy completa en dos
artículos: [The Pirate Bay Now Blocked in
Argentina](http://torrentfreak.com/the-pirate-bay-now-blocked-in-argentina-140701/)
y [Hackers Turn Music Industry Site into The Pirate
Bay](http://torrentfreak.com/hackers-turn-music-industry-site-into-the-pirate-bay-140701/).

Falkvinge, fundador del Partido Pirata de Suecia, escribió un artículo:
[Argentinian Block Of Pirate Bay Shows Poor Understanding Of Modern
Privacy-Of-Location](https://www.privateinternetaccess.com/blog/2014/07/argentinian-block-of-pirate-bay-shows-poor-understanding-of-privacy-of-location/)

La Confederación Pirata de España y otros partidos piratas europeos nos
ayudaron con la difusión de las noticias. Los piratas de Chile, Brasil y
otros latinoamericanos nos ayudaron a difundir la denuncia por Facebook,
por lo que llegó a muchísima gente (más de 40.000 personas según las
métricas de Facebook).

Alt1040 también publicó un artículo: [En Argentina ordenan el bloqueo a
The Pirate
Bay](http://alt1040.com/2014/06/argentina-bloquea-the-pirate-bay)

Curiosamente, lanacion.com cubrió la noticia y las repercusiones:
[Hackearon el sitio de Capif para redireccionarlo a The Pirate
Bay](http://www.lanacion.com.ar/1706155-hackean-el-sitio-de-capif-y-lo-redireccionan-a-the-pirate-bay),
[El bloqueo argentino a The Pirate Bay también afecta a
Paraguay](http://www.lanacion.com.ar/1706497-el-bloqueo-argentino-a-the-pirate-bay-tambien-afecta-a-paraguay),
[La Justicia ordena el bloqueo de The Pirate Bay en la
Argentina](http://www.lanacion.com.ar/1705910-la-comision-nacional-de-comunicaciones-ordena-el-bloqueo-de-the-pirate-bay-en-la-argentina)

### Acciones legales

Hay dos posibles estrategias legales:

1.  Que TPB apele la sentencia. Ya nos comunicamos con gente de TPB en
    Suecia y nos dijeron que ellos tienen sus propios problemas legales
    y no pueden darnos la representación legal, prefieren mantenerse en
    la clandestinidad. Esta estrategia está descartada.
2.  Presentar amparos contra la sentencia porque perjudica a personas
    físicas o jurídicas que distribuyen contenido propio por TPB. Ya hay
    dos amparos confirmados: el de Via Libre y el de Mako (un pirata de
    Córdoba). Hay un tercer artista afectado que está dispuesto a
    presentar un amparo, aún hay que confirmarlo.

Los pasos a seguir son:

1.  Conseguir pruebas (links a contenido en TPB)
2.  Preparar los amparos
3.  Hacer las presentaciones
4.  Conseguir el dinero para pagar los honorarios del abogado

Hay dos opciones:

1.  El juez da a lugar los amparos y levante el bloqueo a TPB
    provisionalmente. Esto haría que se inicie un proceso a parte.
2.  El juez no da a lugar los amparos y hay que ir a cortes
    internacionales, ya que Argentina subscribe a varios tratados.
