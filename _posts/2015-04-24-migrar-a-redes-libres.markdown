---
title: Migrar a Redes Libres
layout: post
categories: []
---

GNU Social
----------

Es una red de microblogin similar a \$Twitter en su funcionamiento pero
*federada* osea hay varios servidores en los que te podés registrar y
seguir contactos en otros servidores y *libre* osea podemos instalar
nuestro propio servidor y ver el código fuente.

Hay [varios servidores de GNU Social](https://gnu.io/social/try/) donde
podés crear tu cuenta.

### Cómo postear a \$Twitter desde GNU Social

Si no quieren dejar completamente el \$TW, usando
[IFTTT](https://ifttt.com/) podemos reenviar todas las publicaciones.

1.  En el **IF** seleccionamos Feed RSS
2.  En nuestro perfil de GNU Social vamos a *Ver el Código Fuente* y
    buscamos nuestro *feed RSS*. La dirección deberia ser similar a
    <https://quitter.is/api/statuses/user_timeline/6561.rss>
3.  En el **THEN** le ponemos que twittee el *EntryContent*

Y listo, ahora cada post (y contacto que sigamos, oops) en GNU Social
saldra en \$TW.

Diaspora
--------
