---
title: Translations:Carta Orgánica/135/es
layout: post
categories: []
---

Todas las disposiciones y gastos hechos por el Órgano Fiduciario debera
ser pública e informada a la asamblea en el plazo de quince (15) dias
luego del final de cada mes, el incumplimiento sera punible.
