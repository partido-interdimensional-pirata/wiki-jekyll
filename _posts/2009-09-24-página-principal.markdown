---
title: Página Principal
layout: post
categories: []
---

<big>**MediaWiki ha sido instalado con éxito.**</big>

Consulta la [Guía de
usuario](http://meta.wikimedia.org/wiki/Help:Contents) para obtener
información sobre el uso del software wiki.

Empezando
---------

-   [Lista de ajustes de
    configuración](http://www.mediawiki.org/wiki/Manual:Configuration_settings)
-   [PMF sobre MediaWiki](http://www.mediawiki.org/wiki/Manual:FAQ)
-   [Lista de correo de anuncios de distribución de
    MediaWiki](https://lists.wikimedia.org/mailman/listinfo/mediawiki-announce)
