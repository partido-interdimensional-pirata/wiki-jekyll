---
title: Como hacer un pedido de acceso a información pública
layout: post
categories:
- Guías
- Guías
---

### Proceso

### Modelos

-   [Formulario
    Modelo](http://www.jgm.gov.ar/archivos/AccesoInfoPub/formulario_solicitud_informacion.pdf)
    (La información estadistica es opcional)

### Guías

-   [Guía de acceso a la información publica
    FOPEA](http://www.scribd.com/doc/99704694/Guia-de-acceso-a-la-informacion-publica?secret_password=2g4bwuajg657fejgfor5)
-   [Guía completa con legislación
    nacional](http://blogs.lanacion.com.ar/data/acceso-a-la-informacion-2/como-hacer-un-pedido-de-acceso-a-la-informacion-publica/)

[Categoría:Guías](Categoría:Guías "wikilink")

[Categoría:Guías](Categoría:Guías "wikilink")
