El barco de comunicación se encarga de mantener la presencia y la imagen pública de los piratas; generar, diseñar y ayudar en la confección de material de difusión; identificar eventos de interés para que los piratas puedan asistir.

Todo el contenido generado por este barco es de dominio público (licencia Creative Commons) y está sujeto a las condiciones de Consenso y Disenso estipuladas en la Carta Orgánica. Cualquier decisión consensuada dentro del barco debe ser comunicada debidamente a la Asamblea y está expuesta a críticas y Disenso; el barco debe adaptar las decisiones a cualquier lineamiento que se desprenda del debate en la Asamblea y sobre el que haya Consenso.

__TOC__

= Actividades =

Las actividades del barco de Comunicación y Relaciones Públicas se resumen en los siguientes puntos:

* Generar circulación y mantener la presencia (crítica) del PPAr en las redes sociales.
* Definir y promover la imagen pública del PPAr, proponiendo lineamientos a la Asamblea General de piratas, etc.
* Generar, diseñar y ayudar en la confección de material de difusión (memes, panfletos, videos, etc.)
* Identificar eventos futuros relacionados con los intereses del PPAr.
* Identificar grupos de interés y proponer estrategias para representar a estos grupos de interés.

= Contacto =
* [https://asambleas.partidopirata.com.ar/listinfo/comunicacion Lista de correo]

= Tripulación =

[[Pirata:Aza|Aza]]

[[Pirata:Betiel|Betiel]]

[[Pirata:Gutemberg|Gutemberg]]

[[Pirata:Ihara|Ihara]]

[[Pirata:Nicoff|Nicoff]]

[[Pirata:Seykron|Seykron]]