{{#switch: {{#expr: (({{días desde|2009-10-27}}+{{{1|0}}})/4 ) mod 4}}
|0=A
|1=B
|2=C
|3=D}}<noinclude>

A qué letra corresponde el artículo destacado en portada.

* {{tl|Portada:Destacado/Letra}} → {{Portada:Destacado/Letra}}

Parámetro opcional: desplazamiento.

* {{tl|Portada:Destacado/Letra|-2}} → {{Portada:Destacado/Letra|-2}}
* {{tl|Portada:Destacado/Letra|-1}} → {{Portada:Destacado/Letra|-1}}
* {{tl|Portada:Destacado/Letra|0}} → {{Portada:Destacado/Letra|0}}
* {{tl|Portada:Destacado/Letra|1}} → {{Portada:Destacado/Letra|1}}
* {{tl|Portada:Destacado/Letra|2}} → {{Portada:Destacado/Letra|2}}
* {{tl|Portada:Destacado/Letra|3}} → {{Portada:Destacado/Letra|3}}


==Véase también==
* [[Plantilla:Portada:Destacado/A]]
* [[Plantilla:Portada:Destacado/B]]
* [[Plantilla:Portada:Destacado/C]]
* [[Plantilla:Portada:Destacado/D]]

</noinclude><noinclude>
[[Categoría:Wikipedia:Plantillas de la portada|{{BASEPAGENAME}}]]
</noinclude>