{{Propuesta
| descripcion = Reforma de la Carta Orgánica para definir las propuestas como un Proceso de Consenso en lugar de un estado.
| estado = nuevas
}}

== Marco ==
Terminar los documentos para constituir legalmente el partido.

== Problema ==

# Para fomentar la participación en las actividades del partido no alcanza con aceptar una Propuesta por Consenso, también hay que llevarla a cabo.
# Esto significa que en cualquier momento de la vida de la Propuesta podrían surgir elementos que requieran un nuevo consenso.
# Por esto es necesario redefinir nuestras propuestas como un Proceso de Consenso, que se extiende durante toda la vida de la propuesta, no solamente con la aceptación de la Asamblea.

== Propuesta ==

Modificar los artículos 8 y 9 de la Carta Orgánica para reflejar el Proceso de Consenso.