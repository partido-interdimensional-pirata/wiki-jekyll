= Análisis de Plataformas de Participación =

Viene del pad [https://pad.partidopirata.com.ar/p/PlataformasDeParticipacion PlataformasDeParticipacion]


== Requisitos == 

* Tiene que ser Software Libre
  http://es.wikipedia.org/wiki/Software_libre
  http://www.gnu.org/philosophy/free-sw.es.html

== Listas de correo ==
http://asambleas.partidopirata.com.ar

* Cada participante se suscribe con su cuenta de correo

* Los mails llegan directo a la casilla

* Es la forma en que nos manejamos siempre

* Formato abierto (se puede escribir de cualquier forma)

Pero

* Hay que saber la netiqueta, sino te confundís y confundís a los demás

* Son muchos mails por día

* Los webmails (hotmail, gmail, yahoo) no te facilitan la lectura y la escritura (no respetan la netiqueta)

* Formato abierto (se pueden romper los temas de conversación)

* No tiene forma de representar decisiones. Proyecto de estructuración de decisiones via mail https://github.com/eudemocracia/foil

* No hay separación lógica entre cosas para hacer y discusiones


== AdHoc ==
http://adhoc.partidopirata.com.ar

* Está hecho para llevar el progreso de cosas para hacer y responsables (es un bug tracker + manejador de proyectos de software)
* Adaptable a cualquier forma de trabajo
* Cada tarea está pre-decidida -> se hace o se abandona (es decir no tiene forma de representar toma de decisiones)
* Diagrama de Gantt con el progreso del "proyecto"

Pero

* Difícil de agarrar
* No tiene forma de representar toma de decisiones (es decir todo está pre-decidido -> se hace o se abandona)


== Loomio ==
http://loomio.org

* Cada tema se puede discutir y se hace al menos una propuesta para resolverlo
* Cada propuesta tiene una fecha de cierre
* No se vota por mayoría sino que hay cuatro formas (a favor, en contra, indiferente, bloqueo)
* El estado de la votación/consenso se ven en un gráfico de torta
* Los votos se pueden cambiar
* Se manda un resumen configurable con lo que pasó en el día
* Los desarrolladores son copados (nos preguntaron cómo nos parece mejor traducir la plataforma)

Pero

* En el momento en que lo probé (fauno) estaba en desarrollo y mantenerlo vivo se ponía jodido.


== Lorea ==
http://red.anillosur.net/g/ppar

* Funciona como una red social (amistad, suscripción, etc.)
* Conocemos a los desarrolladores
  // estoy dispuesto en ayudar por supuesto -- hellekin (admin de anillosur)
* Anillosur ya tiene una comunidad latinoamericana (sino podemos instalar nuestra propia red)
* Tiene las formas de participación de una asamblea:
  * Tareas
  * Toma de decisiones (similar a loomio, sin gráfico)
  * Asamblea con temario (salen de las decisiones y tareas)
* Es federado (pueden haber varios lorea colaborando entre sí)
* Secciones de links, videos y documentos compartidos

Pero

* La federación todavía no funciona


== Reddit (requiere fork) ==
http://reddit.com

* Los temas y los comentarios se pueden votar para arriba o para abajo (o sea que se visualiza el consenso?)
* Se puede hacer indexeo múltiple de temas (por ejemplo, que lo vean ciudades y provincias como tema propio)
* Permite cierto anonimato (puede modificarse). Sólo pide nombre de usuario al registrarse, email es opcional.

Pero

* No tiene forma de tomar decisiones (representada en la plataforma)
* Difícil de entender al principio (?)
* Hay que estar pendiente de la plataforma (tiene notificaciones por mail?)

  // Paulo había creado un subreddit pero no lo usamos nunca -> http://reddit.com/r/ppar -- fauno
  // para probar entre nosotros podría estar bueno, pero para un uso más serio/masivo habría que forkear la plataforma -- mbaragiola
  // a que le decis forkear? -- fauno
  // copiar el repositorio con el código que tienen en github y adaptarlo a nuestras necesidades -- mbaragiola
  // Reddit.com es como Facebook y Twitter, no sirve para comunicación interna pero podría ser una herramienta más para difusión/debate hacia afuera. O para charlar con los piratas anglofonos: http://www.reddit.com/r/pirateparty
  // hay que diferenciar a Reddit (plataforma + red hosteada + sus usuarios + subreddits) de la plataforma reddit. Yo no digo de crear un subreddit. Sino de forkear la plataforma, hostearla nosotros y utilizarla como plataforma de debate para llegar a acuerdos cuando seamos muchos. Creo que hay que preverlo ahora que podemos, y no esperar a que seamos más -- mbaragiola
  // también está meneame.net
    //Meneame.net es un fork de reddit. de igual manera, me parecen mucho mejor los comentarios en reddit que son como tipo arbol, en meneame son lineales.


== LiquidFeedback ==
http://liquidfeedback.org

* Otros Partidos Piratas lo usan

Pero

* Hay consenso en que la democracia líquida no es lo que queremos
* El esquema de licenciamiento no es muy amigable (licencia open source (MIT), resistencia al copyleft ("no vamos a aceptar parches GPL"), cesión de copyright a favor de los desarrolladores (pueden cambiar la licencia o hacerlo propietario)) [Fuente: http://dev.liquidfeedback.org/trac/lf/ "licensing policy"]

== AFK ==
Dinámicas, prácticas, etc. lejos de los teclados

=== Dotmocracy ===
http://dotmocracy.org/

Son unas planillas para toma de decisiones que pintan lindas

== Otros ==

=== Crabgrass ===

hecho por riseup, aunque está medio muerto y depende de una versión deprecada de ruby, https://we.riseup.net

* Electronic Direct Democracy (E2D) parties around the world
http://e2d-international.org/wiki/index.php?title=E-Voting_Taskforce

=== Vilfredo ===
The Vilfredo Goes To Athens project (often referred to as "Vilfredo") starts from the research of what would happen from a mathematical point of view if we consider seriously the principle: "without consensus there is no law." It also assumes that in many case there is a possible solution out there that would generate the consensus. Sometimes key individuals (often regarded as leaders) would find this solution by speaking with the various parts. What this software is trying to do is to help the discussant by providing the showing where the key differences are. To help the people find their solutions without the need of deified leaders (which eventually can become a burden themselves). http://metagovernment.org/wiki/Vilfredo Traducción: https://github.com/eudemocracia/Vilfredo

  // Se ve copado como herramienta de busqueda de consenso -- nicofff

Pero

* Es por consenso si o si. Si no hay consenso entra en loop infinito
* Vulnerable a el efecto color del bicicletero
  // A lo mejor se lo pueda modificar para setearle una cantidad maxima de tiempo/iteraciones, al fin del cual, sale por mayoria. -- nicofff
  // a mi me anduvo re lento el sitio puede ser? -- fauno
  // El codigo es un asco. Esta hecho en PHP puro, sin ningun ORM/MVC. Estoy pensando en reimplementarlo en django (me copa mas la idea) o en Zend Framework (que lo quiero/tengo que aprender)  -- ?
  // El sitio me anduvo bien -- ?

=== Bettermeans ===

Bettermeans es una herramienta de participación ejecutiva abierta y colaborativa que permite la auto-organización democrática de grupos y organizaciones y el logro de resultados concretos. Es un upgrade democrático de Redmine.

Pero

* El proyecto fue abandonado a fines de 2011 y depende de una versión depreciada de ruby y de rails. Traducción: https://github.com/eudemocracia/bettermeans


=== Friction-Free Democracy ===

Our project aims to develop the software tools that will enable organizations of all kinds and sizes to improve their decision-making processes, regardless if it is about political elections, public policies, social subjects or business discussions. http://www.frictionfreedemocracy.org/software-libs

Pero

* Recién se está iniciando su desarrollo.


=== Kune ===

http://es.wikipedia.org/wiki/Kune
http://kune.ourproject.org/es/
https://kune.cc/


=== Democracia en Red ===

DemocraciaEnRed is an online space for deliberation and voting on political proposals. The software aims to stimulate better arguments and come to better rulings. https://github.com/DemocraciaEnRed/condecoradosapp

Pero

* Es democracia líquida y desarrollado por el PdR

  // Porqué es malo que sea desarrollado por el PdR???  -- ?
  // Tal cual...para colmo esta bastante bueno lo que vienen haciendo. -- ?