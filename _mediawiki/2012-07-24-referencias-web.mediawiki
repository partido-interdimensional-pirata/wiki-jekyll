* [http://infoleg.mecon.gov.ar/infolegInternet/anexos/15000-19999/19442/texact.htm Código electoral nacional]
* [http://infoleg.mecon.gov.ar/infolegInternet/anexos/20000-24999/23893/texact.htm Ley orgánica de los partidos políticos]
* [http://infoleg.mecon.gov.ar/infolegInternet/anexos/75000-79999/75022/norma.htm Ley de financiamiento de los partidos]
* [http://infoleg.mecon.gov.ar/infolegInternet/anexos/105000-109999/105144/texact.htm Reglamentación de las elecciones internas abiertas]
* [http://infoleg.mecon.gov.ar/infolegInternet/verNorma.do?id=65634 Reglamentación de la ley del cupo femenino]
* [http://www.pjn.gov.ar/ Poder judicial de la nacion
* [http://www.mininterior.gov.ar/asuntos_politicos_y_alectorales/dine/infogral/dine.php Ministerio del Interior-Dirección Nacional Electoral]