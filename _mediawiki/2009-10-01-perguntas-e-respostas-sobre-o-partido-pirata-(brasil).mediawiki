
== 1) Como surgiu o Partido Pirata Brasileiro? ==


Surgiu em 2007, inspirado no movimento internacional que teve início após a participação do primeiro Partido Pirata nas eleições, na Suécia. Nossa pauta, além de incluir mudanças nas leis sobre direitos autorais, para permitir o amplo acesso a cultura e informação, inclui a transparência governamental e o uso do software livre na administração pública.

No começo, nos organizamos através do Fórum do Partido Pirata Internacional. Em 2008, criamos o site e um fórum próprio. No final de 2008, havia cerca de 350 pessoas cadastradas no site e participando das dicussões. Desde então passamos a ganhar novos membros e crescer.

Em janeiro de 2009, realizamos nosso primeiro encontro presencial, durante a "desconferência" que promovemos no Campus Party. Em março do mesmo ano, realizamos um segundo encontro presencial em São Paulo, chamada "I Insurgência Pirata", onde definimos nossas diretrizes, projetos e campanhas.

Atualmente (setembro de 2009) estamos iniciando o processo de legalização.


== 2) O que defende o Partido Pirata Brasileiro? ==


O Partido Pirata defende o direito de qualquer pessoa compartilhar a cultura e de acessar informações públicas. Entendemos que as leis que regulam a "propriedade intelectual" perderam sua função original de estimular a inovação e promover o interesse público, para servir apenas aos interesses econômicos, gerando monopólios injustificáveis que beneficiam principalmente à grandes corporações, principalmente da midia e do entretenimento - e raramente ao inventor ou autor. Assim, criou-se um sistema abusivo que permite monopolizar ideias e criações humanas, que são essencialmente coletivas, resultados do conhecimento acumulado de gerações.

Com os meios digitais, a única forma de impedir violações aos direitos de "propriedade intelectual" é através do controle sobre as comunicações entre os cidadãos. A privacidade e a liberdade de expressão dos cidadãos são direitos humanos, assim como também são o direito à educação e à comunicação. Estes devem prevalecer contra qualquer direito de fundo econômico, sobretudo privado.

Nossa posição é firme e clara: compartilhar cultura não é crime. Queremos que essa prática social, já estabelecida, seja reconhecida com um direito do cidadão. Não somos contra o comércio de conhecimento ou informação. Defendemos a liberação do compartilhamento sem objetivos comerciais. Mas, para isso é inevitável acabar com o monopólio sobre a informação.

Defendemos também o amplo acesso à informação governamental. Todos os atos de governo são atos públicos. O governo não tem direito de esconder dos cidadãos informações sobre a aplicação de recursos públicos. Também não deve impedir o acesso a conteúdos educacionais e culturais produzidos com dinheiro público. Todas as informações governamentais devem estar acessíveis ao cidadão: relatórios técnicos, contratos, atas, documentos legais. Isso já é possível tecnicamente. Um alto nível de transparência minimizaria muito a corrupção, que ocorre sempre onde o controle e fiscalização dos cidadãos e da sociedade são insuficientes ou inexistentes.

Também defendemos a adoção de software livre na administração pública, por ser mais seguro, eficiente permitir a independência tecnológica (código aberto), ser mais barato e não beneficiar uma empresa de forma monopólica, como ocorre hoje. Também defendemos que todas as transações da administração pública sejam feitas usando documentos ("protocolos") abertos. Iso permitiria abrir documentos e fazer transações com qualquer sistema operacional, sem a necessidade de obrigar o cidadão a comprar um software proprietário (como Windows). Por falta de compromisso com o interesse público, tem ignorado isso, o que pode ser considerado uma ilegalidade, pois isso ocorre em benefício de poucas empresas e poderosos lobbies.

O Partido Pirata defende também a privacidade das pessoas. Cada vez mais nossa vida pessoal tem sido exposta através das tecnologias digitais. Com desculpas como combate ao terrorismo, pedofilia, projetos altamente invasivos tem sido levados a parlamentos de muitos países. No Brasil, recentemente, houve um projeto de autoria do senador Eduardo Azeredo que tentava impor o controle e o monitoramento dos cidadãos, além de criminalizar práticas banais dos cidadãos, como desbloqueio de celular, baixar música da internet ou copiar música de um tocador de Mp3. Felizmente, com a ação da sociedade civil e inclusive do Partido Pirata, esse projeto já foi praticamente barrado no Congresso.

Atualmente, manipula-se o medo das pessoas para aprovar projetos que pretendem monitorar a sociedade, através do armazenamento dos dados de conexão e/ou navegação. No fundo, atendem o interesse de grandes empresas, que já possuem grandes quantidades de informações e/ou que têm interesses econômicos de acessá-las. O Partido Pirata, está atento a isso e fará o possível para barrar tais iniciativas que violam direitos humanos fundamentais, além de expor à risco, jornalistas, ativistas políticos e cidadãos em geral.

Por fim, defendemos também a inclusão digital, a ampliação de redes abertas. Vemos na ampliação do acesso à informação uma oportunidade notável para promover a cultura, a preservação da memória, a cidadania e melhor fiscalizar governos. Apesar de ser um fenômeno extraordinário em muitos campos do conhecimento no Brasil, uma parcela muito pequena da população brasileira tem acesso regular à internet, sendo que muitos de forma precária. As transformações seriam muito mais profundas se o acesso fosse mais amplo.


== 3) E qual é a posição do Partido Pirata com respeito a outras pautas? ==


Não temos soluções para tudo. Nossa inovação está no método e na forma de fazer política. Nosso programa é desenvolvido de forma colaborativa. Se houver necessidade, abrimos outras frentes de atuação. Mas isso será decidido coletivamente e sempre construído colaborativamente. Também pretendemos implantar um amplo sistema de consultas aos filiados para balizar nossas decisões políticas especialmente nos temas onde não temos uma posição formada. Participação, colaboração, transparência e prioridade no interesse coletivo são elementos fundamentais em nossa ação política.


== 4) Por que vocês defendem o direito à privacidade na Internet? ==


O controle sobre a Internet coloca em risco direitos como privacidade, liberdade de expressão, direito à comunicação e, inclusive, o acesso à educação. Além disso, pode transformar nossa sociedade em um estado policialesco de vigilância permanente. Os cidadãos não devem ser tratados como suspeitos. O anonimato deve ser encarado como um direito, como uma defesa pessoal contra abusos legais. O registro e o controle inibem a atuação dos cidadãos e a liberdade de expressão. As ameaças à privacidade podem colocar em risco a segurança de jornalistas, militantes de movimentos sociais e ativista dos direitos humanos, além de expô-los a chantagens e outros tipos de pressão.

Experiências de controle e monitoramento sobre as comunicações dos cidadãos são típicas de países autoritários e regimes repressivos. Por trás de argumentos como necessidade de combate ao terrorismo ou defesa de segurança, existem interesses econômicos. Governos e corporações manipulam o medo para convencer a população a viver sob tais controles. Será que os controles são realmente necessários? Quem tem acesso a tais dados?

Atualmente se pode adquirir bancos de dados da receita federal com camelôs das grandes cidades do Brasil. Na Alemanha, ocorreu vazamento semelhante, desta vez com dados de gente famosa e políticos. Por isso, ao invés de obrigar a um longo armazenamento, a lei deve impor restrições ao tempo de armazenamento. Nesse sentido, o Partido Pirata vem desenvolvendo a Carta de Direitos da Internet. Entendemos que os princípios consagrados na Declaração de Direitos Humanos são inegociáveis e devem prevalecer sobre o interesse econômico ou de corporações e governos.


== 5) Por que o Partido Pirata defende a transparência no governo? ==


Defendemos a transparência na administração pública e o acesso a informação sobre o governo para todos os cidadãos. Isso por uma razão simples: todos os atos públicos devem ser, de fato, atos públicos. É um direito do cidadão ter controles sobre a gestão pública em todos os níveis. A corrupção, os desmandos e os vícios de todo tipo surgem onde não há transparência e imperam em meio ao segredo e a desinformação.

No entanto, não nos contentamos em apenas informar os cidadãos sobre os atos públicos. Desejamos desenvolver ferramentas que possibilitem uma participação direta da população nos processos das esferas legislativas e executivas. À transparência governamental deve seguir a democracia participativa, utilizando ferramentas como a Internet para aproximar governo e cidadão. Eis a importância fundamental em equipar todo os sistemas públicos com softwares livres para que todos tenham acesso ao estudo e reprodução dos programas que fornecem a base para o processamento de dados públicos.


== 6) Por que "Partido"? ==


Não é nossa preocupação ser um partido. Nosso objetivo é defender o compartilhamento na sociedade. Se for necessário agir como partido, assim faremos. A ideia é justamente formar um "anti-partido". Buscamos a participação de todos e não apenas de uma "parte" - e de forma não-hierárquica e participativa. Deste modo, a construção de um partido oficial pode servir justamente como um dos instrumentos para a superação da condição de intermediário político e a realização destas práticas de democracia participativa. Para algumas pessoas a palavra "partido" pode incomodar. Trata-se de uma provocação. Como "piratas" podemos incomodar ainda mais, fazer as pessoas pensarem que é possível fazer política de forma diferente, alegre, com seriedade e crítica ao mesmo tempo que esperançosa.


== 7) Por que "Pirata"? ==


Historicamente, o termo "pirata" vem sendo deturpado e associado a criminosos e assassinos, apesar de a maioria dos crimes terem sido cometidos pelas grandes potências marítimas. Hoje em dia, o termo é utilizado para denominar falsificadores e vendedores ilegais, assim como para denominar quem usa a internet para compartilhar um filme ou música. Mas o "pirata" significa ser livre. É curioso é que as crianças gostam tanto de piratas, mas os adultos os comparam com assassinos sanguinários. As crianças também gostam de compartilhar e são alegres, enquanto os adultos vão ficando egoístas e perdem o bom humor.

Na história da pirataria no Caribe, os navios piratas não aceitavam escravos. Em suas tripulações haviam negros livres, índios e pessoas de todas nacionalidades. Os piratas eram amigos dos índios e das comunidades de cimarrons (negros fugidos), assim podiam viver muitos anos no mar, descansando na costa e fazendo trocas de víveres e bens. Já os espanhóis, por exemplo, roubavam e escravizavam os índios. Cheias de produto de roubo, seus pesados galeões eram movidos pela força das remadas dos escravos, que viviam nas galés (a "galera", os mesmos ainda remam hoje nosso desenvolvimento econômico desigual e injusto). Um ataque pirata era a esperança de liberdade na galera.

Os piratas não faziam comércio, pois não havia meios de operar em dinheiro ou moedas de ouro ou prata. Faziam o escambo, que significa troca. De fato, os piratas faziam circular as riquezas saqueadas das Américas quebrando o monopólio colônia-metrópole. "Quando alguém chamar um camelô de pirata, você pode falar: “Pirata o escambau!” E escambau vem de escambo!


== 8) Quem faz parte do Partido Pirata? ==


Jovens de todas as idades. Pode-se dizer que se você é generoso e compartilha alguma coisa, você compartilha dos ideais do Partido Pirata. Esse é o principal fator que nos une. Para garantir nosso direito de compartilhar e ser livres, pretendemos mudar as leis e, em última instância, a própria sociedade.


== 9) A pirataria desencoraja a inovação e a criação? ==


Durante a maior parte da história humana eram quase inexistentes as restrições para cópias, transformações ou novos usos de inventos e criações. Mesmo assim se desenvolveu a Ciência, as Artes, a Cultura e surgiram inúmeras inovações.Não há literatura que aponte exemplos de artistas, músicos ou escritores que deixaram de criar em função das cópias ou obras derivadas. Atualmente, não há qualquer evidência disso no que se refere ao compartilhamento na Internet. Essa afirmação constitui um discurso, aparentemente convicente, mas que não possui base nenhuma. Pelo contrário, estudos recentes, apontam o efeito positivo do compartilhamento, seja para sociedade ou os artistas.

Um exemplo disso, é o recente estudo feito na Estudo da Universidade de Harvard. Ele indicou que o download não representa uma venda perdida e, inclusive remixes e mashups de músicas incentivam a venda de canções originais e aumentam a demanda por shows. O estudo conclui que o compartilhamento illegal de arquivos não desencoraja a produção artística, pois o número de novos álbuns dobrou desde 2000. Conclui também que o maior acesso do público às músicas e uma proteção mais fraca dos direitos autorais, beneficiam a sociedade.

Outro estudo feito na Noruega que indica que as pessoas que baixam discos ilegalmente via redes peer-to-peer (P2P) também estão entre os que têm mais probabilidade de comprar música online
.
A Internet é um ambiente notavelmente criativo com participação de uma multidão de pessoas dispostas a criar com um ímpeto sem precedente na história humana. A "propriedade intelectual", assim como as patentes, são utilizadas em geral como reserva de mercado. Por ser baseada em monopólios - o que pode ser considerado uma anomalia numa economia concorrencial - possibilita que o produtor estipule o preço que quiser. O monopólio é ainda mais inaceitável dada a natureza da informação - intangível e inesgotável.

As leis de propriedade intelectual são eficazes apenas para constituir monopólios que enriquecem aos intermediários (editores musicais, editores de livros, etc.) e não aos criadores. Em geral, estes perdem o controle de suas próprias obras, devido à "transferência de titularidade" dos direitos de sua obra que fazem para poderem divulgar suas obras. Esse é o caso dos grandes nomes da música popular brasileira. Muito autores ficam impedidos de regravar ou lançar coletâneas. O problema é mais complexo quando a editora fechou ou as obras estão fora de catálogo por falta de interesse comercial do "titular de direitos".


== 10) Mas do que vão viver os músicos se as pessoas deixarem de comprar seus CDs? ==


Os músicos em geral não vivem de venda de CDs. O valor que arrecadam com a venda de CDs é irrisório. Basta calcular as tiragens e calcular um valor em torno dos 5% do valor cobrado na loja. O artista ganha principalmente com performances. Além disso ganha em execuções em rádios comerciais, TVs, eventos, vinhetas publicitárias e assim por diante.

A função principal do CD é a divulgação da obra. E isso a Internet faz muito melhor.

O lobby dá indústria tem o discurso de que protegem o artista. Na verdade seus contratos são dacronianos e em geral, tiram do artista o controle de sua obra. A indústria cultural apenas faz o papel de intermediário, cujos interesses são sim ameaçados pela Internet. A rede põe em contato o artista diretamente com seu público. Ele inclusive pode tomar contato com um público maior, divulgar com eficiência, e inclusive vender bens tangíveis, como camisetas, posters, etc.

A indústria cultural também tem um papel muito ruim na difusão da cultura. O jabá, benefício distribuído às rádios e TVs para tocar uma música ou exibir um artista é altamente nefasto. Isso faz com que artistas sejam empacotados e vendidos como mercadorias. Muitas vezes a qualidade musical e artística é sofrível. Não raras vezes o artista é mutilado em sua criatividade, tendo que produzir algo para atender aos interesses de lucro da indústria. Na Internet, predomina a liberdade de escolha. Pessoas podem conhecer uma infinidade de artistas e formar sua própria opinião sobre eles, segundo seus gostos e preferências.


== 11) Vocês não estão vendo demais o lado do cidadão, e como fica o artista que é pirateado? ==


O artista/criador é um aliado do Partido Pirata. Pretendemos ajudar a libertá-los das garras da indústria. Queremos promover a inclusão digital, a expansão das redes abertas. Mais música na rede, mais acesso da população aos artistas e suas obras. Com isso, mais oportunidades de shows, contato sem a necessidade de intermediários com o seu público. Nada de contratos draconianos com gravadoras. Os artistas devem controlar suas próprias obras.

Defendemos também um limite de 5 anos no tempo de transferência legal da titularidade da obra dos artistas às gravadoras. Ou seja, passado esse período de exploração comercial a obra volta ao controle artista, que pode fazer o que quiser com ela.


== 12) Quais seriam as novas leis de direitos autorais? ==


Nós enxergamos o Autor não como "dono" e sim como "criador", e porque não, "co-criador" de "sua" obra. A lei deve se adequar as novas de produção e difusão do conhecimento. Os usos não-comerciais deveriam ser definitivamente liberados. Isso resolveria também o problema das obras esgotadas, órfãs ou de acervos não digitais que correm o risco de desaparecer pela ação do tempo.

Estamos desenvolvendo nossa proposta de forma colaborativa aqui.


== 13) Defender a descriminalização da pirataria é legal? ==


A resposta dessa pergunta vai depender do que você entende por pirataria. Por exemplo, se o ponto não é compartilhamento, mas sim a pirataria dos camelôs, entendemos que tais trabalhadores sustentam suas famílias em condições precárias de trabalho e sob o risco constante de perder sua mercadoria. Como um problema social, não deve ser tratado de forma policial, senão através de políticas que incluam essa população em condições mais dignas e favoráveis na sociedade.

A pirataria de rua é reflexo das condições de pobreza do país, da falta de acesso a bens culturais e de consumo. Boa parte dos produtos de rua são produzidos na China, país que viola sistematicamente os direitos humanos, que censura a Internet, que não respeita direitos trabalhistas e que se utiliza de mão de obra infantil ou semi-escrava. Além disso, a China invadiu o Tibete e lá instalou uma tirania. Por que o governo não toma uma medida contra este país, ao invés de reprimir a ponta fraca (os comerciantes que vão às ruas expôr os produtos) desta corrente? Aliás como essas mercadorias entram no país? Certamemte por aeroportos ou portos. A Receita não vê? A Polícia Rodoviária não pára? A PF não sabe? Curioso. A pirataria de rua é o final de uma longa cadeia de injustiças em nossa sociedade. É muito fácil prender o pobre que está na rua. Amanhã haverá outro em seu lugar. Nosso foco é sobre o interesse coletivo, por isso defendemos a dignidade das pessoas e um tratamento mais cuidadoso dessa questão, sem hipocrisias.


== 14) Então, o Partido Pirata é a favor da falsificação de produtos? ==


A falsificação de produtos prejudica o cidadão, isto é: a tentativa de réplica de um produto usando características do original como a marca e a aparência. Sabemos que isso não oferece uma proteção ao cidadão quanto a origem e a qualidade do produto. Defendemos a liberdade de criação de novos produtos legítimos. Uma forma disso ocorrer é através de uma profunda mudança no sistema de patentes, liberando a inovação e a criação na sociedade. Com muitos produtores, o mercado se desconcentraria, surgiriam muitas novas oportunidades para toda a sociedade, beneficiando igualmente a produtores e consumidores.

O ideal para a sociedade não é a criação de monopólios. Por isso, as inovações deveriam ser produzidas por diferentes produtores que, competiriam entre si para atender as necessidades das pessoas, gerando assim múltiplas cadeias de inovação.

Falsificar é enganar, não é criar nada. Associamos pirataria à compartilhamento, isso inclui reprodução, mas não falsificação.