El propósito de este articulo es redactar una respuesta a este proyecto de ley remarcando los puntos flojos y los peligros de la aprobación de esta ley.

==Borrador==


==Proyecto "Ley Pinedo"==
REGIMEN PARA PROVEDORES DEL SERVICIO DE INTERNET

ARTICULO 1º.- A los efectos de la presente ley, los términos que a continuación se indican tendrán el significado previsto en el presente artículo:

1) Proveedores de Servicios de Internet: Incluye a los Proveedores de Acceso a Internet, Proveedores de Facilidades de Interconexión, Proveedores de Alojamiento, Proveedores de Contenido o de Información y Proveedores de Servicios.

2) Proveedores de Acceso a Internet: Son quienes por medio de diferentes medios técnicos, prestan el servicio de conexión a la red Internet.

3) Proveedores de Facilidades de Interconexión: Son quienes aportan los medios estructurales para la circulación de la información en Internet, independientemente de las características de la conexión o de la circulación de la información en la red.

4) Proveedores de Servicios: Los Motores de Búsqueda y todos aquellos que ofrecen servicios y/o programas que utilizan Internet como medio necesario para prestar dichos servicios o utilizar dichas aplicaciones o programas.

5) Proveedores de Contenido o de Información: Son todos los que suministran contenidos a Internet, independientemente del formato utilizado para suministrar dichos contenidos e independientemente de si los suministraran en sitios webs propios o provistos por terceros.

6) Motores de Búsqueda: Quienes ofrecen en un sitio web el servicio de buscar en Internet enlaces a otros sitios webs en los que se encuentre o se encontraba el o los términos ingresados por el usuario.

7) Proveedores de Alojamiento: Quienes ofrecen los medios técnicos para que los desarrolladores y/o titulares de sitios Web hagan disponible el contenido de los mismos a todos los usuarios de Internet, almacenando o no los contenidos de los sitios en servidores de su propiedad.

8) Contenido específico: todo archivo de cualquier naturaleza o formato al que se pueda acceder a través de Internet.

ARTICULO 2º.- Los Proveedores de Servicios de Internet serán responsables por el almacenamiento automático de contenidos generados por terceros exclusivamente cuando tuvieren conocimiento efectivo, en los términos de esta ley, de que los contenidos almacenados violan normas legales o derechos de terceros.

ARTICULO 3º.- Toda persona, de existencia visible o ideal, podrá promover una medida ante el juez con competencia en su domicilio con el objeto de solicitar judicialmente que se elimine y/o se restrinja y/o se bloquee el acceso a uno o más contenidos específicos -sea en forma de texto, sonido, imagen o cualquier otra información o representación- que lesionen derechos o garantías reconocidos por la Constitución Nacional, un tratado o una ley de la República Argentina.

En el caso, el juez podrá ordenar las medidas requeridas en forma provisional sin haber oído a la otra parte, en particular cuando haya probabilidad de que cualquier retraso cause daño irreparable al titular de los derechos, o cuando haya un riesgo demostrable de destrucción de pruebas. El juez podrá exigir al demandante que presente las pruebas de que razonablemente disponga, con el fin de establecer con un grado suficiente de certidumbre que el demandante es el titular del derecho alegado y que ese derecho es objeto o va a ser objeto inminentemente de violación, y ordenar que preste caución juratoria o aporte una fianza, o garantía equivalente, que sea suficiente para proteger al demandado y evitar abusos.

En todo aquello no previsto especialmente por esta ley, la medida establecida en el presente artículo tramitará de conformidad con las normas del proceso previsto en los artículos 232 y 321 inciso segundo del Código Procesal Civil y Comercial de la Nación y normas análogas de las legislaciones procesales provinciales.

ARTICULO 4º.- Será responsabilidad primaria del Ministerio Público solicitar las medidas indicadas en el artículo tercero de la presente ley ante contenidos específicos en los que se pueda observar la representación de un menor de dieciocho (18) años dedicado a actividades sexuales explícitas o toda representación de sus partes genitales con fines predominantemente sexuales.

ARTICULO 5º.- Los Proveedores de Servicios de Internet serán responsables por la transmisión o retransmisión de contenidos generados por terceros exclusivamente cuando los propios Proveedores de Servicios de Internet sean quienes originen dicha transmisión o retransmisión, o cuando modifiquen o seleccionen los contenidos y/o seleccionen a los destinatarios de la información transmitida o retransmitida.

Se entenderá por modificación a la variación concreta, parcial o total, del contenido, pero no a la variación estrictamente técnica del formato de los contenidos transmitidos o retransmitidos.

ARTÍCULO 6º.- Los Proveedores de Alojamiento, los Proveedores de Contenidos y los Proveedores de Servicios que ofrezcan enlaces a otros sitios webs u ofrezcan información provista por terceros, serán responsables respecto de la información provista por los terceros exclusivamente en los casos en que tengan conocimiento efectivo de que la información almacenada viola normas legales o derechos de terceros.

ARTÍCULO 7º.- A los fines de la aplicación de la presente ley, se entenderá que los Proveedores de Servicios de Internet tienen conocimiento efectivo de que determinados contenidos violan normas legales o derechos de terceros desde el momento en que sean notificados del dictado de alguna de las medidas previstas en el artículo 3º de esta ley o de otra resolución judicial que así lo establezca.

ARTICULO 8º.- Los Motores de Búsqueda que se encuentren en sitios web bajo el código país (ccLTD) AR deberán agregar en sus sitios webs, dentro de los noventa días de la entrada en vigencia de ésta ley, una dirección de correo electrónico para la atención de reclamos de consumidores o usuarios, la cual deberá emitir una respuesta electrónica, incluyendo una copia del mensaje recibido, dirigida a la dirección electrónica del remitente confirmando la recepción del reclamo.

ARTICULO 9º.- Invítese a las Provincias y a la Ciudad Autónoma de Buenos Aires al dictado de normas procesales similares a las previstas en la presente ley.

ARTICULO 10.- Comuníquese, etc.

==Fuentes==

* http://alt1040.com/2011/03/todo-sobre-el-proyecto-de-ley-que-pretende-regular-internet-en-argentina
* http://parlamentario.com/noticia-34666.html
* http://www.ncn.com.ar/08/noticiad.php?n=9928&sec=2&ssec=&s=noticiad
* http://www.bea.org.ar/2011/03/alguien-que-le-avise-a-pinedo/
* http://derechoaleer.org/2011/03/una-mala-copia-de-la-ley-sinde-p.html
* http://www.fabio.com.ar/verpost.php?id_noticia=4396